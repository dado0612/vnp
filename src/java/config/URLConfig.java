package config;

public class URLConfig {

    public static final String LOGIN = "index/login.html";
    public static final String ERROR = "index/error.html";
    public static final String EMPLOYEE = "admin/employee.jsp";
}
