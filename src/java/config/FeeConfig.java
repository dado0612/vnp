package config;

public class FeeConfig {

    public static final float SCALE = 1.5f;
    public static final int FEE_PER_GRAM = 150;
    public static final int FEE_PER_DIM = 250;
}
