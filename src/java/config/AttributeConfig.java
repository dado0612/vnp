package config;

public class AttributeConfig {

    public static final String USERNAME = "username";
    public static final String NUMBER_BILLS = "billNumber";
    public static final String NUMBER_POSTS = "postNumber";
    public static final String NUMBER_EMPLOYEES = "employeeNumber";
    public static final String LIST_TRACKS = "listTrack";
    public static final String DEFAULT_PASSWORD = "c4ca4238a0b923820dcc509a6f75849b";
}
