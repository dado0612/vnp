package config;

public class RoleConfig {

    public static final int RETIRE = -1;
    public static final int ADMIN = 1;
    public static final int MANAGER = 2;
    public static final int SHIPPER = 3;
}
