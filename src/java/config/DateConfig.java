package config;

import java.text.SimpleDateFormat;

public class DateConfig {

    public static final SimpleDateFormat DATE_TIME_FORMAT = new SimpleDateFormat("dd/MM/yyyy HH:mm");
    public static final SimpleDateFormat DATE_FORMAT = new SimpleDateFormat("dd/MM/yyyy");
    public static final SimpleDateFormat DATE_FORMAT_HTML = new SimpleDateFormat("MM/dd/yyyy");
}
