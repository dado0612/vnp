package config;

public class TrackStatus {

    public static final int CONF = 0;
    public static final int INIT = 1;
    public static final int IN = 2;
    public static final int OUT = 3;
    public static final int SHIP = 4;
    public static final int OK = 5;
    public static final int ERR = 6;
}
