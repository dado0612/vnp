package config;

public class RandomConfig {

    public static final long MAX_DATE = 1541523600000L;  // 7/11/2018
    public static final long MIN_DATE = 1541005200000L;  // 1/11/2018
    public static final long MIN_HIRE = 1483203600000L;  // 1/1/2017
    public static final long MAX_DOB = 946659600000L;    // 1/1/2000
    public static final long MIN_DOB = 631126800000L;    // 1/1/1990
    public static final long MAX_INC = 42000000L;        // 12h
    public static final long MIN_INC = 300000L;          // 5m
}
