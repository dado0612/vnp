package controller;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import model.Bill;

public class BillDAO {

    private static final DBContext CONTEXT = new DBContext();

    private static List<Bill> getList(ResultSet rs) throws Exception {
        List<Bill> listBill = new ArrayList<>();
        while (rs.next()) {
            int billID = rs.getInt("billID");
            String postID = rs.getString("postID");
            long packageID = rs.getLong("packageID");
            Date time = rs.getTimestamp("time");
            int fee = rs.getInt("fee");
            listBill.add(new Bill(billID, postID, packageID, time, fee));
        }
        return listBill;
    }

    public static List<Bill> selectByDay(String postID, Calendar cal) {
        List<Bill> listBill = new ArrayList<>();
        String sql = "SELECT * FROM Bill WHERE postID = ? AND YEAR(time) = ? AND MONTH(time) = ? AND DAY(time) = ?";
        try (Connection con = CONTEXT.getConnection();
                PreparedStatement ps = con.prepareStatement(sql)) {
            ps.setString(1, postID);
            ps.setInt(2, cal.get(Calendar.YEAR));
            ps.setInt(3, cal.get(Calendar.MONTH) + 1);
            ps.setInt(4, cal.get(Calendar.DAY_OF_MONTH));
            try (ResultSet rs = ps.executeQuery()) {
                listBill = getList(rs);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return listBill;
    }

    public static List<Bill> selectByMonth(String postID, Calendar cal) {
        List<Bill> listBill = new ArrayList<>();
        String sql = "SELECT * FROM Bill WHERE postID = ? AND YEAR(time) = ? AND MONTH(time) = ?";
        try (Connection con = CONTEXT.getConnection();
                PreparedStatement ps = con.prepareStatement(sql)) {
            ps.setString(1, postID);
            ps.setInt(2, cal.get(Calendar.YEAR));
            ps.setInt(3, cal.get(Calendar.MONTH) + 1);
            try (ResultSet rs = ps.executeQuery()) {
                listBill = getList(rs);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return listBill;
    }

    public static List<Bill> selectByYear(String postID, Calendar cal) {
        List<Bill> listBill = new ArrayList<>();
        String sql = "SELECT * FROM Bill WHERE postID = ? AND YEAR(time) = ?";
        try (Connection con = CONTEXT.getConnection();
                PreparedStatement ps = con.prepareStatement(sql)) {
            ps.setString(1, postID);
            ps.setInt(2, cal.get(Calendar.YEAR));
            try (ResultSet rs = ps.executeQuery()) {
                listBill = getList(rs);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return listBill;
    }

    public static Bill selectByPackageID(long packageID) {
        Bill bill = null;
        String sql = "SELECT * FROM Bill WHERE packageID = ?";
        try (Connection con = CONTEXT.getConnection();
                PreparedStatement ps = con.prepareStatement(sql)) {
            ps.setLong(1, packageID);
            try (ResultSet rs = ps.executeQuery()) {
                if (rs.next()) {
                    int billID = rs.getInt("billID");
                    String postID = rs.getString("postID");
                    Date time = rs.getTimestamp("time");
                    int fee = rs.getInt("fee");
                    bill = new Bill(billID, postID, packageID, time, fee);
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return bill;
    }

    public static boolean addBill(Bill bill) {
        boolean check = true;
        String sql = "INSERT INTO Bill VALUES (?,?,?,?)";
        try (Connection con = CONTEXT.getConnection();
                PreparedStatement ps = con.prepareStatement(sql)) {
            ps.setString(1, bill.getPostID());
            ps.setLong(2, bill.getPackageID());
            ps.setTimestamp(3, new java.sql.Timestamp(bill.getTime().getTime()));
            ps.setInt(4, bill.getFee());
            ps.execute();
        } catch (Exception e) {
            e.printStackTrace();
            check = false;
        }
        return check;
    }

    public static boolean addAllBill(List<Bill> list) {
        boolean check = true;
        String sql = "INSERT INTO Bill VALUES (?,?,?,?)";
        try (Connection con = CONTEXT.getConnection();
                PreparedStatement ps = con.prepareStatement(sql)) {
            for (Bill bill : list) {
                ps.setString(1, bill.getPostID());
                ps.setLong(2, bill.getPackageID());
                ps.setTimestamp(3, new java.sql.Timestamp(bill.getTime().getTime()));
                ps.setInt(4, bill.getFee());
                ps.addBatch();
            }
            ps.executeBatch();
        } catch (Exception e) {
            e.printStackTrace();
            check = false;
        }
        return check;
    }

    public static int countBill() {
        int cnt = -1;
        String sql = "SELECT COUNT(*) FROM Bill";
        try (Connection con = CONTEXT.getConnection();
                PreparedStatement ps = con.prepareStatement(sql)) {
            try (ResultSet rs = ps.executeQuery()) {
                if (rs.next()) {
                    cnt = rs.getInt(1);
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return cnt;
    }

    public static long totalRevenueByCurrentYear(String postID) {
        List<Bill> list = selectByYear(postID, Calendar.getInstance());
        long total = 0;
        for (Bill bill : list) {
            total += bill.getFee();
        }
        return total;
    }

    public static long[] revenueLastNDays(String postID, int n) {
        long[] arr = new long[n];
        for (int i = 0; i < n; i++) {
            Calendar cal = Calendar.getInstance();
            cal.add(Calendar.DAY_OF_MONTH, -i);
            List<Bill> list = selectByDay(postID, cal);
            long total = 0;
            for (Bill bill : list) {
                total += bill.getFee();
            }
            arr[i] = total;
        }
        return arr;
    }

    public static long totalRevenueByCurrentMonth(String postID) {
        List<Bill> list = selectByMonth(postID, Calendar.getInstance());
        long total = 0;
        for (Bill bill : list) {
            total += bill.getFee();
        }
        return total;
    }

    public static long totalRevenueByCurrentDay(String postID) {
        List<Bill> list = selectByDay(postID, Calendar.getInstance());
        long total = 0;
        for (Bill bill : list) {
            total += bill.getFee();
        }
        return total;
    }

    public static long[] revenueLastNMonths(String postID, int n) {
        long[] arr = new long[n];
        for (int i = 0; i < n; i++) {
            Calendar cal = Calendar.getInstance();
            cal.add(Calendar.MONTH, -i);
            List<Bill> list = selectByMonth(postID, cal);
            long total = 0;
            for (Bill bill : list) {
                total += bill.getFee();
            }
            arr[i] = total;
        }
        return arr;
    }

    public static long[] revenueLastNYears(String postID, int n) {
        long[] arr = new long[n];
        for (int i = 0; i < n; i++) {
            Calendar cal = Calendar.getInstance();
            cal.add(Calendar.YEAR, -i);
            List<Bill> list = selectByYear(postID, cal);
            long total = 0;
            for (Bill bill : list) {
                total += bill.getFee();
            }
            arr[i] = total;
        }
        return arr;
    }

    public static List<Bill> selectByDay(Calendar cal) {
        List<Bill> listBill = new ArrayList<>();
        String sql = "SELECT * FROM Bill WHERE YEAR(time) = ? AND MONTH(time) = ? AND DAY(time) = ?";
        try (Connection con = CONTEXT.getConnection();
                PreparedStatement ps = con.prepareStatement(sql)) {
            ps.setInt(1, cal.get(Calendar.YEAR));
            ps.setInt(2, cal.get(Calendar.MONTH) + 1);
            ps.setInt(3, cal.get(Calendar.DAY_OF_MONTH));
            try (ResultSet rs = ps.executeQuery()) {
                listBill = getList(rs);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return listBill;
    }

    public static List<Bill> selectByMonth(Calendar cal) {
        List<Bill> listBill = new ArrayList<>();
        String sql = "SELECT * FROM Bill WHERE YEAR(time) = ? AND MONTH(time) = ?";
        try (Connection con = CONTEXT.getConnection();
                PreparedStatement ps = con.prepareStatement(sql)) {
            ps.setInt(1, cal.get(Calendar.YEAR));
            ps.setInt(2, cal.get(Calendar.MONTH) + 1);
            try (ResultSet rs = ps.executeQuery()) {
                listBill = getList(rs);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return listBill;
    }

    public static List<Bill> selectByYear(Calendar cal) {
        List<Bill> listBill = new ArrayList<>();
        String sql = "SELECT * FROM Bill WHERE YEAR(time) = ?";
        try (Connection con = CONTEXT.getConnection();
                PreparedStatement ps = con.prepareStatement(sql)) {
            ps.setInt(1, cal.get(Calendar.YEAR));
            try (ResultSet rs = ps.executeQuery()) {
                listBill = getList(rs);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return listBill;
    }

    public static long totalRevenueByCurrentYear() {
        List<Bill> list = selectByYear(Calendar.getInstance());
        long total = 0;
        for (Bill bill : list) {
            total += bill.getFee();
        }
        return total;
    }

    public static long[] revenueLastNDays(int n) {
        long[] arr = new long[n];
        for (int i = 0; i < n; i++) {
            Calendar cal = Calendar.getInstance();
            cal.add(Calendar.DAY_OF_MONTH, -i);
            List<Bill> list = selectByDay(cal);
            long total = 0;
            for (Bill bill : list) {
                total += bill.getFee();
            }
            arr[i] = total;
        }
        return arr;
    }

    public static long totalRevenueByCurrentMonth() {
        List<Bill> list = selectByMonth(Calendar.getInstance());
        long total = 0;
        for (Bill bill : list) {
            total += bill.getFee();
        }
        return total;
    }

    public static long totalRevenueByCurrentDay() {
        List<Bill> list = selectByDay(Calendar.getInstance());
        long total = 0;
        for (Bill cur : list) {
            total += cur.getFee();
        }
        return total;
    }

    public static long[] revenueLastNMonths(int n) {
        long[] arr = new long[n];
        for (int i = 0; i < n; i++) {
            Calendar cal = Calendar.getInstance();
            cal.add(Calendar.MONTH, -i);
            List<Bill> list = selectByMonth(cal);
            long total = 0;
            for (Bill bill : list) {
                total += bill.getFee();
            }
            arr[i] = total;
        }
        return arr;
    }

    public static long[] revenueLastNYears(int n) {
        long[] arr = new long[n];
        for (int i = 0; i < n; i++) {
            Calendar cal = Calendar.getInstance();
            cal.add(Calendar.YEAR, -i);
            List<Bill> list = selectByYear(cal);
            long total = 0;
            for (Bill bill : list) {
                total += bill.getFee();
            }
            arr[i] = total;
        }
        return arr;
    }
}
