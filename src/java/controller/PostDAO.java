package controller;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.List;
import model.Post;

public class PostDAO {

    private static final DBContext CONTEXT = new DBContext();

    public static Post selectByPostID(String postID) {
        Post post = null;
        String sql = "SELECT * FROM Post WHERE postID = ?";
        try (Connection con = CONTEXT.getConnection();
                PreparedStatement ps = con.prepareStatement(sql)) {
            ps.setString(1, postID);
            try (ResultSet rs = ps.executeQuery()) {
                if (rs.next()) {
                    String locationID = rs.getString("locationID");
                    String postName = rs.getString("postName");
                    String postAddress = rs.getString("postAddress");
                    post = new Post(postID, locationID, postName, postAddress);
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return post;
    }

    public static List<Post> selectAll() {
        List<Post> listPost = new ArrayList<>();
        String sql = "SELECT * FROM Post";
        try (Connection con = CONTEXT.getConnection();
                PreparedStatement ps = con.prepareStatement(sql)) {
            try (ResultSet rs = ps.executeQuery()) {
                while (rs.next()) {
                    String postID = rs.getString("postID");
                    String locationID = rs.getString("locationID");
                    String postName = rs.getString("postName");
                    String postAddress = rs.getString("postAddress");
                    listPost.add(new Post(postID, locationID, postName, postAddress));
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return listPost;
    }

    public static List<Post> selectPostByLocation(String inLocationID) {
        List<Post> listPost = new ArrayList<>();
        inLocationID += "%";
        String sql = "SELECT * FROM Post WHERE locationID LIKE ?";
        try (Connection con = CONTEXT.getConnection();
                PreparedStatement ps = con.prepareStatement(sql)) {
            ps.setString(1, inLocationID);
            try (ResultSet rs = ps.executeQuery()) {
                while (rs.next()) {
                    String postID = rs.getString("postID");
                    String locationID = rs.getString("locationID");
                    String postName = rs.getString("postName");
                    String postAddress = rs.getString("postAddress");
                    listPost.add(new Post(postID, locationID, postName, postAddress));
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return listPost;
    }

    public static boolean addAllPost(List<Post> list) {
        boolean check = true;
        String sql = "INSERT INTO Post VALUES (?,?,?,?)";
        try (Connection con = CONTEXT.getConnection();
                PreparedStatement ps = con.prepareStatement(sql)) {
            for (Post post : list) {
                ps.setString(1, post.getPostID());
                ps.setString(2, post.getLocationID());
                ps.setString(3, post.getPostName());
                ps.setString(4, post.getPostAddress());
                ps.addBatch();
            }
            ps.executeBatch();
        } catch (Exception e) {
            e.printStackTrace();
            check = false;
        }
        return check;
    }

    public static int countPost() {
        int cnt = -1;
        String sql = "SELECT COUNT(*) FROM Post";
        try (Connection con = CONTEXT.getConnection();
                PreparedStatement ps = con.prepareStatement(sql)) {
            try (ResultSet rs = ps.executeQuery()) {
                if (rs.next()) {
                    cnt = rs.getInt(1);
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return cnt;
    }
}
