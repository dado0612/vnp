package controller;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import model.Employee;

public class EmployeeDAO {

    private static final DBContext CONTEXT = new DBContext();

    private static Employee getEmployee(ResultSet rs) throws Exception {
        int employeeID = rs.getInt("employeeID");
        String postID = rs.getString("postID");
        int roleID = rs.getInt("roleID");
        String username = rs.getString("username");
        String name = rs.getString("name");
        boolean gender = rs.getBoolean("gender");
        Date dob = rs.getDate("dob");
        String phone = rs.getString("phone");
        Date hireDate = rs.getDate("hireDate");
        String identityNum = rs.getString("identityNum");
        return new Employee(employeeID, postID, roleID, username, name, gender, dob, phone, hireDate, identityNum);
    }

    public static List<Employee> selectAll(int roleID) {
        List<Employee> listEmp = new ArrayList<>();
        String sql = "SELECT * FROM Employee WHERE roleID > ?";
        try (Connection con = CONTEXT.getConnection();
                PreparedStatement ps = con.prepareStatement(sql)) {
            ps.setInt(1, roleID);
            try (ResultSet rs = ps.executeQuery()) {
                while (rs.next()) {
                    listEmp.add(getEmployee(rs));
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return listEmp;
    }

    public static List<Employee> selectShipperInPost(String postID) {
        List<Employee> listEmp = new ArrayList<>();
        String sql = "SELECT * FROM Employee WHERE postID = ? AND roleID = 3";
        try (Connection con = CONTEXT.getConnection();
                PreparedStatement ps = con.prepareStatement(sql)) {
            ps.setString(1, postID);
            try (ResultSet rs = ps.executeQuery()) {
                while (rs.next()) {
                    listEmp.add(getEmployee(rs));
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return listEmp;
    }

    public static Employee selectByEmployeeID(int employeeID) {
        Employee emp = null;
        String sql = "SELECT * FROM Employee WHERE employeeID = ?";
        try (Connection con = CONTEXT.getConnection();
                PreparedStatement ps = con.prepareStatement(sql)) {
            ps.setInt(1, employeeID);
            try (ResultSet rs = ps.executeQuery()) {
                if (rs.next()) {
                    emp = getEmployee(rs);
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return emp;
    }

    public static Employee selectByUsername(String username) {
        Employee emp = null;
        String sql = "SELECT * FROM Employee WHERE username = ?";
        try (Connection con = CONTEXT.getConnection();
                PreparedStatement ps = con.prepareStatement(sql)) {
            ps.setString(1, username);
            try (ResultSet rs = ps.executeQuery()) {
                if (rs.next()) {
                    emp = getEmployee(rs);
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return emp;
    }

    public static boolean addEmployee(Employee emp) {
        boolean check = true;
        String sql = "INSERT INTO Employee VALUES (?,?,?,?,?,?,?,?,?)";
        try (Connection con = CONTEXT.getConnection();
                PreparedStatement ps = con.prepareStatement(sql)) {
            ps.setString(1, emp.getPostID());
            ps.setInt(2, emp.getRoleID());
            ps.setString(3, emp.getUsername());
            ps.setString(4, emp.getName());
            ps.setBoolean(5, emp.isGender());
            ps.setDate(6, new java.sql.Date(emp.getDob().getTime()));
            ps.setString(7, emp.getPhone());
            ps.setDate(8, new java.sql.Date(emp.getHireDate().getTime()));
            ps.setString(9, emp.getIdentityNum());
            ps.execute();
        } catch (Exception e) {
            e.printStackTrace();
            check = false;
        }
        return check;
    }

    public static boolean addAllEmployee(List<Employee> list) {
        boolean check = true;
        String sql = "INSERT INTO Employee VALUES (?,?,?,?,?,?,?,?,?)";
        try (Connection con = CONTEXT.getConnection();
                PreparedStatement ps = con.prepareStatement(sql)) {
            for (Employee emp : list) {
                ps.setString(1, emp.getPostID());
                ps.setInt(2, emp.getRoleID());
                ps.setString(3, emp.getUsername());
                ps.setString(4, emp.getName());
                ps.setBoolean(5, emp.isGender());
                ps.setDate(6, new java.sql.Date(emp.getDob().getTime()));
                ps.setString(7, emp.getPhone());
                ps.setDate(8, new java.sql.Date(emp.getHireDate().getTime()));
                ps.setString(9, emp.getIdentityNum());
                ps.addBatch();
            }
            ps.executeBatch();
        } catch (Exception e) {
            e.printStackTrace();
            check = false;
        }
        return check;
    }

    public static boolean updateEmployee(Employee emp) {
        boolean check = true;
        String sql = "UPDATE Employee SET postID = ?,roleID = ?,username = ?,name = ?,\n"
                + "gender = ?,dob = ?,phone = ?,identityNum = ? WHERE employeeID = ?";
        try (Connection con = CONTEXT.getConnection();
                PreparedStatement ps = con.prepareStatement(sql)) {
            ps.setString(1, emp.getPostID());
            ps.setInt(2, emp.getRoleID());
            ps.setString(3, emp.getUsername());
            ps.setString(4, emp.getName());
            ps.setBoolean(5, emp.isGender());
            ps.setDate(6, new java.sql.Date(emp.getDob().getTime()));
            ps.setString(7, emp.getPhone());
            ps.setString(8, emp.getIdentityNum());
            ps.setInt(9, emp.getEmployeeID());
            ps.execute();
        } catch (Exception e) {
            e.printStackTrace();
            check = false;
        }
        return check;
    }

    public static boolean deleteEmployee(int employeeID) {
        boolean check = true;
        String sql = "UPDATE Employee SET roleID = -1 WHERE employeeID = ?";
        try (Connection con = CONTEXT.getConnection();
                PreparedStatement ps = con.prepareStatement(sql)) {
            ps.setInt(1, employeeID);
            ps.execute();
        } catch (Exception e) {
            e.printStackTrace();
            check = false;
        }
        return check;
    }

    public static int countEmployee() {
        int cnt = -1;
        String sql = "SELECT COUNT(*) FROM Employee";
        try (Connection con = CONTEXT.getConnection();
                PreparedStatement ps = con.prepareStatement(sql)) {
            try (ResultSet rs = ps.executeQuery()) {
                if (rs.next()) {
                    cnt = rs.getInt(1);
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return cnt;
    }
}
