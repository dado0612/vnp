package controller;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.List;
import model.Location;

public class LocationDAO {

    private static final DBContext CONTEXT = new DBContext();

    private static List<Location> getList(ResultSet rs) throws Exception {
        List<Location> list = new ArrayList<>();
        while (rs.next()) {
            String locationID = rs.getString("locationID");
            String locationName = rs.getString("locationName");
            list.add(new Location(locationID, locationName));
        }
        return list;
    }

    public static Location selectByLocationID(String locationID) {
        Location loc = null;
        String sql = "SELECT locationName FROM Location WHERE locationID = ?";
        try (Connection con = CONTEXT.getConnection();
                PreparedStatement ps = con.prepareStatement(sql)) {
            ps.setString(1, locationID);
            try (ResultSet rs = ps.executeQuery()) {
                if (rs.next()) {
                    String locationName = rs.getString("locationName");
                    loc = new Location(locationID, locationName);
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return loc;
    }

    public static List<Location> selectAllProvince() {
        List<Location> listProvince = new ArrayList<>();
        String sql = "SELECT * FROM Location WHERE LEN(locationID) = 2";
        try (Connection con = CONTEXT.getConnection();
                PreparedStatement ps = con.prepareStatement(sql)) {
            try (ResultSet rs = ps.executeQuery()) {
                listProvince = getList(rs);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return listProvince;
    }

    public static List<Location> selectAllDistrictByProvince(String locationID) {
        List<Location> listDistrict = new ArrayList<>();
        locationID = locationID + "%";
        String sql = "SELECT * FROM Location WHERE LEN(locationID) = 4 AND locationID LIKE ?";
        try (Connection con = CONTEXT.getConnection();
                PreparedStatement ps = con.prepareStatement(sql)) {
            ps.setString(1, locationID);
            try (ResultSet rs = ps.executeQuery()) {
                listDistrict = getList(rs);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return listDistrict;
    }

    public static List<Location> selectAllCommuneByDistrict(String locationID) {
        List<Location> listCommune = new ArrayList<>();
        locationID = locationID + "%";
        String sql = "SELECT * FROM Location WHERE LEN(locationID) = 6 AND locationID LIKE ?";
        try (Connection con = CONTEXT.getConnection();
                PreparedStatement ps = con.prepareStatement(sql)) {
            ps.setString(1, locationID);
            try (ResultSet rs = ps.executeQuery()) {
                listCommune = getList(rs);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return listCommune;
    }

    public static boolean addAllLocation(List<Location> list) {
        boolean check = true;
        String sql = "INSERT INTO Location VALUES (?,?)";
        try (Connection con = CONTEXT.getConnection();
                PreparedStatement ps = con.prepareStatement(sql)) {
            for (Location loc : list) {
                ps.setString(1, loc.getLocationID());
                ps.setString(2, loc.getLocationName());
                ps.addBatch();
            }
            ps.executeBatch();
        } catch (Exception e) {
            e.printStackTrace();
            check = false;
        }
        return check;
    }

    public static String getFullNameCommune(String locationID) {
        String commune = selectByLocationID(locationID).getLocationName();
        String district = selectByLocationID(locationID.substring(0, 4)).getLocationName();
        String province = selectByLocationID(locationID.substring(0, 2)).getLocationName();
        return commune + " - " + district + " - " + province;
    }
}
