package controller;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.TreeMap;
import model.Track;

public class TrackDAO {

    private static final DBContext CONTEXT = new DBContext();

    private static List<Track> getList(ResultSet rs) throws Exception {
        List<Track> list = new ArrayList<>();
        while (rs.next()) {
            int trackID = rs.getInt("trackID");
            int employeeID = rs.getInt("employeeID");
            long packageID = rs.getLong("packageID");
            String postID = rs.getString("postID");
            Date time = rs.getTimestamp("time");
            int status = rs.getInt("status");
            String description = rs.getString("description");
            list.add(new Track(trackID, employeeID, packageID, postID, time, status, description));
        }
        return list;
    }

    public static List<Track> selectByPackageID(long packageID) {
        List<Track> listTrack = new ArrayList<>();
        String sql = "SELECT * FROM Track WHERE packageID = ? ORDER BY trackID DESC";
        try (Connection con = CONTEXT.getConnection();
                PreparedStatement ps = con.prepareStatement(sql)) {
            ps.setLong(1, packageID);
            try (ResultSet rs = ps.executeQuery()) {
                listTrack = getList(rs);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return listTrack;
    }

    public static List<Track> selectByPostID(String postID) {
        List<Track> listTrack = new ArrayList<>();
        String sql = "SELECT TOP (1000) * FROM Track WHERE postID = ? ORDER BY trackID DESC";
        try (Connection con = CONTEXT.getConnection();
                PreparedStatement ps = con.prepareStatement(sql)) {
            ps.setString(1, postID);
            try (ResultSet rs = ps.executeQuery()) {
                listTrack = getList(rs);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return listTrack;
    }

    public static List<Track> selectAll() {
        List<Track> listTrack = new ArrayList<>();
        String sql = "SELECT TOP (1000) * FROM Track ORDER BY trackID DESC";
        try (Connection con = CONTEXT.getConnection();
                PreparedStatement ps = con.prepareStatement(sql)) {
            try (ResultSet rs = ps.executeQuery()) {
                listTrack = getList(rs);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return listTrack;
    }

    public static List<Track> selectStorageByPostID(String postID) {
        List<Track> listTrack = new ArrayList<>();
        String sql = "SELECT * FROM Track WHERE postID = ? AND trackID in\n"
                + "(SELECT MAX(trackID) FROM Track GROUP BY packageID) AND\n"
                + "(status = 1 OR status = 2 OR status = 6) ORDER BY trackID DESC";
        try (Connection con = CONTEXT.getConnection();
                PreparedStatement ps = con.prepareStatement(sql)) {
            ps.setString(1, postID);
            try (ResultSet rs = ps.executeQuery()) {
                listTrack = getList(rs);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return listTrack;
    }

    public static List<Track> selectStorageAll() {
        List<Track> listTrack = new ArrayList<>();
        String sql = "SELECT * FROM Track WHERE trackID in\n"
                + "(SELECT MAX(trackID) FROM Track GROUP BY packageID) AND\n"
                + "(status = 1 OR status = 2 OR status = 6) ORDER BY trackID DESC";
        try (Connection con = CONTEXT.getConnection();
                PreparedStatement ps = con.prepareStatement(sql)) {
            try (ResultSet rs = ps.executeQuery()) {
                listTrack = getList(rs);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return listTrack;
    }

    public static boolean addTrack(Track track) {
        boolean check = true;
        String sql = "INSERT INTO Track VALUES (?,?,?,?,?,?)";
        try (Connection con = CONTEXT.getConnection();
                PreparedStatement ps = con.prepareStatement(sql)) {
            ps.setInt(1, track.getEmployeeID());
            ps.setLong(2, track.getPackageID());
            ps.setString(3, track.getPostID());
            ps.setTimestamp(4, new java.sql.Timestamp(track.getTime().getTime()));
            ps.setInt(5, track.getStatus());
            ps.setString(6, track.getDescription());
            ps.execute();
        } catch (Exception e) {
            e.printStackTrace();
            check = false;
        }
        return check;
    }

    public static boolean addAllTrack(TreeMap<Long, Track> map) {
        boolean check = true;
        String sql = "INSERT INTO Track VALUES (?,?,?,?,?,?)";
        try (Connection con = CONTEXT.getConnection();
                PreparedStatement ps = con.prepareStatement(sql)) {
            for (long x : map.keySet()) {
                Track track = map.get(x);
                ps.setInt(1, track.getEmployeeID());
                ps.setLong(2, track.getPackageID());
                ps.setString(3, track.getPostID());
                ps.setTimestamp(4, new java.sql.Timestamp(track.getTime().getTime()));
                ps.setInt(5, track.getStatus());
                ps.setString(6, track.getDescription());
                ps.addBatch();
            }
            ps.executeBatch();
        } catch (Exception e) {
            e.printStackTrace();
            check = false;
        }
        return check;
    }

    public static Track getLastTrackByPackageID(long packageID) {
        Track track = null;
        String sql = "SELECT TOP (1) * FROM Track WHERE packageID = ? ORDER BY trackID DESC";
        try (Connection con = CONTEXT.getConnection();
                PreparedStatement ps = con.prepareStatement(sql)) {
            ps.setLong(1, packageID);
            try (ResultSet rs = ps.executeQuery()) {
                if (rs.next()) {
                    int trackID = rs.getInt("trackID");
                    int employeeID = rs.getInt("employeeID");
                    String postID = rs.getString("postID");
                    Date time = rs.getTimestamp("time");
                    int status = rs.getInt("status");
                    String description = rs.getString("description");
                    track = new Track(trackID, employeeID, packageID, postID, time, status, description);
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return track;
    }

    public static List<Track> selectInitPackByPostID(String postID) {
        List<Track> listTrack = new ArrayList<>();
        String sql = "SELECT * FROM Track WHERE postID = ? AND status = 1";
        try (Connection con = CONTEXT.getConnection();
                PreparedStatement ps = con.prepareStatement(sql)) {
            ps.setString(1, postID);
            try (ResultSet rs = ps.executeQuery()) {
                listTrack = getList(rs);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return listTrack;
    }

    public static List<Track> selectIncomePackByPostID(String postID) {
        List<Track> listTrack = new ArrayList<>();
        String sql = "SELECT * FROM Track WHERE postID = ? AND status = 2";
        try (Connection con = CONTEXT.getConnection();
                PreparedStatement ps = con.prepareStatement(sql)) {
            ps.setString(1, postID);
            try (ResultSet rs = ps.executeQuery()) {
                listTrack = getList(rs);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return listTrack;
    }

    public static List<Track> selectInitTrackByDay(String postID, Calendar cal) {
        List<Track> listTrack = new ArrayList<>();
        String sql = "SELECT * FROM Track WHERE postID = ? AND status = 1\n"
                + "AND YEAR(time) = ? AND MONTH(time) = ? AND DAY(time) = ?";
        try (Connection con = CONTEXT.getConnection();
                PreparedStatement ps = con.prepareStatement(sql)) {
            ps.setString(1, postID);
            ps.setInt(2, cal.get(Calendar.YEAR));
            ps.setInt(3, cal.get(Calendar.MONTH) + 1);
            ps.setInt(4, cal.get(Calendar.DAY_OF_MONTH));
            try (ResultSet rs = ps.executeQuery()) {
                listTrack = getList(rs);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return listTrack;
    }

    public static int[] numberInitPackLastNDay(String postID, int n) {
        int[] arr = new int[n];
        for (int i = 0; i < n; i++) {
            Calendar cal = Calendar.getInstance();
            cal.add(Calendar.DAY_OF_MONTH, -i);
            List<Track> list = selectInitTrackByDay(postID, cal);
            arr[i] = list.size();
        }
        return arr;
    }

    public static List<Track> selectIncomeTrackByDay(String postID, Calendar cal) {
        List<Track> listTrack = new ArrayList<>();
        String sql = "SELECT * FROM Track WHERE postID = ? AND status = 2\n"
                + "AND YEAR(time) = ? AND MONTH(time) = ? AND DAY(time) = ?";
        try (Connection con = CONTEXT.getConnection();
                PreparedStatement ps = con.prepareStatement(sql)) {
            ps.setString(1, postID);
            ps.setInt(2, cal.get(Calendar.YEAR));
            ps.setInt(3, cal.get(Calendar.MONTH) + 1);
            ps.setInt(4, cal.get(Calendar.DAY_OF_MONTH));
            try (ResultSet rs = ps.executeQuery()) {
                listTrack = getList(rs);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return listTrack;
    }

    public static int[] numberIncomePackLastNDay(String postID, int n) {
        int[] arr = new int[n];
        for (int i = 0; i < n; i++) {
            Calendar cal = Calendar.getInstance();
            cal.add(Calendar.DAY_OF_MONTH, -i);
            List<Track> list = selectIncomeTrackByDay(postID, cal);
            arr[i] = list.size();
        }
        return arr;
    }

    public static List<Track> selectInitTrackByMonth(String postID, Calendar cal) {
        List<Track> listTrack = new ArrayList<>();
        String sql = "SELECT * FROM Track WHERE postID = ? AND status = 1\n"
                + "AND YEAR(time) = ? AND MONTH(time) = ?";
        try (Connection con = CONTEXT.getConnection();
                PreparedStatement ps = con.prepareStatement(sql)) {
            ps.setString(1, postID);
            ps.setInt(2, cal.get(Calendar.YEAR));
            ps.setInt(3, cal.get(Calendar.MONTH) + 1);
            try (ResultSet rs = ps.executeQuery()) {
                listTrack = getList(rs);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return listTrack;
    }

    public static List<Track> selectInitTrackByYear(String postID, Calendar cal) {
        List<Track> listTrack = new ArrayList<>();
        String sql = "SELECT * FROM Track WHERE postID = ? AND status = 1 AND YEAR(time) = ?";
        try (Connection con = CONTEXT.getConnection();
                PreparedStatement ps = con.prepareStatement(sql)) {
            ps.setString(1, postID);
            ps.setInt(2, cal.get(Calendar.YEAR));
            try (ResultSet rs = ps.executeQuery()) {
                listTrack = getList(rs);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return listTrack;
    }

    public static List<Track> selectIncomeTrackByMonth(String postID, Calendar cal) {
        List<Track> listTrack = new ArrayList<>();
        String sql = "SELECT * FROM Track WHERE postID = ? AND status = 2\n"
                + "AND YEAR(time) = ? AND MONTH(time) = ?";
        try (Connection con = CONTEXT.getConnection();
                PreparedStatement ps = con.prepareStatement(sql)) {
            ps.setString(1, postID);
            ps.setInt(2, cal.get(Calendar.YEAR));
            ps.setInt(3, cal.get(Calendar.MONTH) + 1);
            try (ResultSet rs = ps.executeQuery()) {
                listTrack = getList(rs);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return listTrack;
    }

    public static List<Track> selectIncomeTrackByYear(String postID, Calendar cal) {
        List<Track> listTrack = new ArrayList<>();
        String sql = "SELECT * FROM Track WHERE postID = ? AND status = 2 AND YEAR(time) = ?";
        try (Connection con = CONTEXT.getConnection();
                PreparedStatement ps = con.prepareStatement(sql)) {
            ps.setString(1, postID);
            ps.setInt(2, cal.get(Calendar.YEAR));
            try (ResultSet rs = ps.executeQuery()) {
                listTrack = getList(rs);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return listTrack;
    }

    public static int[] numberInitPackLastNMonth(String postID, int n) {
        int[] arr = new int[n];
        for (int i = 0; i < n; i++) {
            Calendar cal = Calendar.getInstance();
            cal.add(Calendar.MONTH, -i);
            List<Track> list = selectInitTrackByMonth(postID, cal);
            arr[i] = list.size();
        }
        return arr;
    }

    public static int[] numberInitPackLastNYear(String postID, int n) {
        int[] arr = new int[n];
        for (int i = 0; i < n; i++) {
            Calendar cal = Calendar.getInstance();
            cal.add(Calendar.YEAR, -i);
            List<Track> list = selectInitTrackByYear(postID, cal);
            arr[i] = list.size();
        }
        return arr;
    }

    public static int[] numberIncomePackLastNMonth(String postID, int n) {
        int[] arr = new int[n];
        for (int i = 0; i < n; i++) {
            Calendar cal = Calendar.getInstance();
            cal.add(Calendar.MONTH, -i);
            List<Track> list = selectIncomeTrackByMonth(postID, cal);
            arr[i] = list.size();
        }
        return arr;
    }

    public static int[] numberIncomePackLastNYear(String postID, int n) {
        int[] arr = new int[n];
        for (int i = 0; i < n; i++) {
            Calendar cal = Calendar.getInstance();
            cal.add(Calendar.YEAR, -i);
            List<Track> list = selectIncomeTrackByYear(postID, cal);
            arr[i] = list.size();
        }
        return arr;
    }

    public static List<Track> selectInitTrackByDay(Calendar cal) {
        List<Track> listTrack = new ArrayList<>();
        String sql = "SELECT * FROM Track WHERE status = 1 AND YEAR(time) = ?\n"
                + "AND MONTH(time) = ? AND DAY(time) = ?";
        try (Connection con = CONTEXT.getConnection();
                PreparedStatement ps = con.prepareStatement(sql)) {
            ps.setInt(1, cal.get(Calendar.YEAR));
            ps.setInt(2, cal.get(Calendar.MONTH) + 1);
            ps.setInt(3, cal.get(Calendar.DAY_OF_MONTH));
            try (ResultSet rs = ps.executeQuery()) {
                listTrack = getList(rs);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return listTrack;
    }

    public static int[] numberInitPackLastNDay(int n) {
        int[] arr = new int[n];
        for (int i = 0; i < n; i++) {
            Calendar cal = Calendar.getInstance();
            cal.add(Calendar.DAY_OF_MONTH, -i);
            List<Track> list = selectInitTrackByDay(cal);
            arr[i] = list.size();
        }
        return arr;
    }

    public static List<Track> selectIncomeTrackByDay(Calendar cal) {
        List<Track> listTrack = new ArrayList<>();
        String sql = "SELECT * FROM Track WHERE status = 2 AND YEAR(time) = ?\n"
                + "AND MONTH(time) = ? AND DAY(time) = ?";
        try (Connection con = CONTEXT.getConnection();
                PreparedStatement ps = con.prepareStatement(sql)) {
            ps.setInt(1, cal.get(Calendar.YEAR));
            ps.setInt(2, cal.get(Calendar.MONTH) + 1);
            ps.setInt(3, cal.get(Calendar.DAY_OF_MONTH));
            try (ResultSet rs = ps.executeQuery()) {
                listTrack = getList(rs);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return listTrack;
    }

    public static int[] numberIncomePackLastNDay(int n) {
        int[] arr = new int[n];
        for (int i = 0; i < n; i++) {
            Calendar cal = Calendar.getInstance();
            cal.add(Calendar.DAY_OF_MONTH, -i);
            List<Track> list = selectIncomeTrackByDay(cal);
            arr[i] = list.size();
        }
        return arr;
    }

    public static List<Track> selectInitTrackByMonth(Calendar cal) {
        List<Track> listTrack = new ArrayList<>();
        String sql = "SELECT * FROM Track WHERE status = 1 AND YEAR(time) = ? AND MONTH(time) = ?";
        try (Connection con = CONTEXT.getConnection();
                PreparedStatement ps = con.prepareStatement(sql)) {
            ps.setInt(1, cal.get(Calendar.YEAR));
            ps.setInt(2, cal.get(Calendar.MONTH) + 1);
            try (ResultSet rs = ps.executeQuery()) {
                listTrack = getList(rs);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return listTrack;
    }

    public static List<Track> selectInitTrackByYear(Calendar cal) {
        List<Track> listTrack = new ArrayList<>();
        String sql = "SELECT * FROM Track WHERE status = 1 AND YEAR(time) = ?";
        try (Connection con = CONTEXT.getConnection();
                PreparedStatement ps = con.prepareStatement(sql)) {
            ps.setInt(1, cal.get(Calendar.YEAR));
            try (ResultSet rs = ps.executeQuery()) {
                listTrack = getList(rs);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return listTrack;
    }

    public static List<Track> selectIncomeTrackByMonth(Calendar cal) {
        List<Track> listTrack = new ArrayList<>();
        String sql = "SELECT * FROM Track WHERE status = 2 AND YEAR(time) = ? AND MONTH(time) = ?";
        try (Connection con = CONTEXT.getConnection();
                PreparedStatement ps = con.prepareStatement(sql)) {
            ps.setInt(1, cal.get(Calendar.YEAR));
            ps.setInt(2, cal.get(Calendar.MONTH) + 1);
            try (ResultSet rs = ps.executeQuery()) {
                listTrack = getList(rs);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return listTrack;
    }

    public static List<Track> selectIncomeTrackByYear(Calendar cal) {
        List<Track> listTrack = new ArrayList<>();
        String sql = "SELECT * FROM Track WHERE status = 2 AND YEAR(time) = ?";
        try (Connection con = CONTEXT.getConnection();
                PreparedStatement ps = con.prepareStatement(sql)) {
            ps.setInt(1, cal.get(Calendar.YEAR));
            try (ResultSet rs = ps.executeQuery()) {
                listTrack = getList(rs);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return listTrack;
    }

    public static int[] numberInitPackLastNMonth(int n) {
        int[] arr = new int[n];
        for (int i = 0; i < n; i++) {
            Calendar cal = Calendar.getInstance();
            cal.add(Calendar.MONTH, -i);
            List<Track> list = selectInitTrackByMonth(cal);
            arr[i] = list.size();
        }
        return arr;
    }

    public static int[] numberInitPackLastNYear(int n) {
        int[] arr = new int[n];
        for (int i = 0; i < n; i++) {
            Calendar cal = Calendar.getInstance();
            cal.add(Calendar.YEAR, -i);
            List<Track> list = selectInitTrackByYear(cal);
            arr[i] = list.size();
        }
        return arr;
    }

    public static int[] numberIncomePackLastNMonth(int n) {
        int[] arr = new int[n];
        for (int i = 0; i < n; i++) {
            Calendar cal = Calendar.getInstance();
            cal.add(Calendar.MONTH, -i);
            List<Track> list = selectIncomeTrackByMonth(cal);
            arr[i] = list.size();
        }
        return arr;
    }

    public static int[] numberIncomePackLastNYear(int n) {
        int[] arr = new int[n];
        for (int i = 0; i < n; i++) {
            Calendar cal = Calendar.getInstance();
            cal.add(Calendar.YEAR, -i);
            List<Track> list = selectIncomeTrackByYear(cal);
            arr[i] = list.size();
        }
        return arr;
    }
}
