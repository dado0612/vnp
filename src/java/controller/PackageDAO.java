package controller;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.List;
import model.Package;

public class PackageDAO {

    private static final DBContext CONTEXT = new DBContext();

    public static Package selectByPackageID(long packageID) {
        Package pac = null;
        String sql = "SELECT * FROM Package WHERE packageID = ?";
        try (Connection con = CONTEXT.getConnection();
                PreparedStatement ps = con.prepareStatement(sql)) {
            ps.setLong(1, packageID);
            try (ResultSet rs = ps.executeQuery()) {
                if (rs.next()) {
                    String postID = rs.getString("postID");
                    String receiverLocation = rs.getString("receiverLocation");
                    String senderName = rs.getString("senderName");
                    String senderPhone = rs.getString("senderPhone");
                    String receiverName = rs.getString("receiverName");
                    String receiverPhone = rs.getString("receiverPhone");
                    String receiverMail = rs.getString("receiverMail");
                    String receiverAddress = rs.getString("receiverAddress");
                    long price = rs.getLong("price");
                    String description = rs.getString("description");
                    pac = new Package(packageID, postID, receiverLocation, senderName, senderPhone,
                            receiverName, receiverPhone, receiverMail, receiverAddress, price, description);
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return pac;
    }

    public static boolean addPackage(Package pac) {
        boolean check = true;
        String sql = "INSERT INTO Package VALUES (?,?,?,?,?,?,?,?,?,?,?)";
        try (Connection con = CONTEXT.getConnection();
                PreparedStatement ps = con.prepareStatement(sql)) {
            ps.setLong(1, pac.getPackageID());
            ps.setString(2, pac.getPostID());
            ps.setString(3, pac.getReceiverLocation());
            ps.setString(4, pac.getSenderName());
            ps.setString(5, pac.getSenderPhone());
            ps.setString(6, pac.getReceiverName());
            ps.setString(7, pac.getReceiverPhone());
            ps.setString(8, pac.getReceiverMail());
            ps.setString(9, pac.getReceiverAddress());
            ps.setLong(10, pac.getPrice());
            ps.setString(11, pac.getDescription());
            ps.execute();
        } catch (Exception e) {
            e.printStackTrace();
            check = false;
        }
        return check;
    }

    public static boolean addAllPackage(List<Package> list) {
        boolean check = true;
        String sql = "INSERT INTO Package VALUES (?,?,?,?,?,?,?,?,?,?,?)";
        try (Connection con = CONTEXT.getConnection();
                PreparedStatement ps = con.prepareStatement(sql)) {
            for (Package pac : list) {
                ps.setLong(1, pac.getPackageID());
                ps.setString(2, pac.getPostID());
                ps.setString(3, pac.getReceiverLocation());
                ps.setString(4, pac.getSenderName());
                ps.setString(5, pac.getSenderPhone());
                ps.setString(6, pac.getReceiverName());
                ps.setString(7, pac.getReceiverPhone());
                ps.setString(8, pac.getReceiverMail());
                ps.setString(9, pac.getReceiverAddress());
                ps.setLong(10, pac.getPrice());
                ps.setString(11, pac.getDescription());
                ps.addBatch();
            }
            ps.executeBatch();
        } catch (Exception e) {
            e.printStackTrace();
            check = false;
        }
        return check;
    }

    public static int countPackage() {
        int cnt = -1;
        String sql = "SELECT COUNT(*) FROM Package";
        try (Connection con = CONTEXT.getConnection();
                PreparedStatement ps = con.prepareStatement(sql)) {
            try (ResultSet rs = ps.executeQuery()) {
                if (rs.next()) {
                    cnt = rs.getInt(1);
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return cnt;
    }
}
