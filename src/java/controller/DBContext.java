package controller;

import config.DBConfig;
import java.sql.Connection;
import java.sql.DriverManager;

public class DBContext {

    public Connection getConnection() throws Exception {
        String url = "jdbc:sqlserver://" + DBConfig.SERVERNAME + ":" + DBConfig.PORTNUMBER + "\\" + DBConfig.INSTANCE + ";databaseName=" + DBConfig.DBNAME;
        if (DBConfig.INSTANCE == null || DBConfig.INSTANCE.trim().isEmpty()) {
            url = "jdbc:sqlserver://" + DBConfig.SERVERNAME + ":" + DBConfig.PORTNUMBER + ";databaseName=" + DBConfig.DBNAME;
        }
        Class.forName("com.microsoft.sqlserver.jdbc.SQLServerDriver");
        return DriverManager.getConnection(url, DBConfig.USERID, DBConfig.PASSWORD);
    }
}
