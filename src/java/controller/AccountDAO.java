package controller;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.List;
import model.Account;

public class AccountDAO {

    private static final DBContext CONTEXT = new DBContext();

    public static boolean checkLogin(Account acc) {
        boolean check = false;
        String sql = "SELECT * FROM Account WHERE username = ? AND password = ?";
        try (Connection con = CONTEXT.getConnection();
                PreparedStatement ps = con.prepareStatement(sql)) {
            ps.setString(1, acc.getUsername());
            ps.setString(2, acc.getPassword());
            try (ResultSet rs = ps.executeQuery()) {
                if (rs.next()) {
                    check = true;
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return check;
    }

    public static boolean checkDuplicate(String username) {
        boolean check = false;
        String sql = "SELECT * FROM Account WHERE username = ?";
        try (Connection con = CONTEXT.getConnection();
                PreparedStatement ps = con.prepareStatement(sql)) {
            ps.setString(1, username);
            try (ResultSet rs = ps.executeQuery()) {
                if (rs.next()) {
                    check = true;
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
            check = true;
        }
        return check;
    }

    public static boolean addAccount(Account acc) {
        boolean check = true;
        if (checkDuplicate(acc.getUsername())) {
            check = false;
        } else {
            String sql = "INSERT INTO Account VALUES (?,?)";
            try (Connection con = CONTEXT.getConnection();
                    PreparedStatement ps = con.prepareStatement(sql)) {
                ps.setString(1, acc.getUsername());
                ps.setString(2, acc.getPassword());
                ps.execute();
            } catch (Exception e) {
                e.printStackTrace();
                check = false;
            }
        }
        return check;
    }

    public static boolean addAllAccount(List<Account> list) {
        boolean check = true;
        String sql = "INSERT INTO Account VALUES (?,?)";
        try (Connection con = CONTEXT.getConnection();
                PreparedStatement ps = con.prepareStatement(sql)) {
            for (Account acc : list) {
                ps.setString(1, acc.getUsername());
                ps.setString(2, acc.getPassword());
                ps.addBatch();
            }
            ps.executeBatch();
        } catch (Exception e) {
            e.printStackTrace();
            check = false;
        }
        return check;
    }

    public static boolean changePassword(Account acc, String newPassword) {
        boolean check = true;
        if (!checkLogin(acc)) {
            check = false;
        } else {
            String sql = "UPDATE Account SET password = ? WHERE username = ?";
            try (Connection con = CONTEXT.getConnection();
                    PreparedStatement ps = con.prepareStatement(sql)) {
                ps.setString(1, newPassword);
                ps.setString(2, acc.getUsername());
                ps.execute();
            } catch (Exception e) {
                e.printStackTrace();
                check = false;
            }
        }
        return check;
    }
}
