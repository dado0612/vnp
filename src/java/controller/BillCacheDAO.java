package controller;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.List;
import model.BillCache;

public class BillCacheDAO {

    private static final DBContext CONTEXT = new DBContext();

    public static BillCache selectByPackageID(long packageID) {
        BillCache cache = null;
        String sql = "SELECT * FROM BillCache WHERE packageID = ?";
        try (Connection con = CONTEXT.getConnection();
                PreparedStatement ps = con.prepareStatement(sql)) {
            ps.setLong(1, packageID);
            try (ResultSet rs = ps.executeQuery()) {
                if (rs.next()) {
                    int billCacheID = rs.getInt("billCacheID");
                    int employeeID = rs.getInt("employeeID");
                    int fee = rs.getInt("fee");
                    boolean status = rs.getBoolean("status");
                    cache = new BillCache(billCacheID, employeeID, packageID, fee, status);
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return cache;
    }

    public static boolean addBillCache(BillCache billCache) {
        boolean check = true;
        String sql = "INSERT INTO BillCache VALUES (?,?,?,?)";
        try (Connection con = CONTEXT.getConnection();
                PreparedStatement ps = con.prepareStatement(sql)) {
            ps.setInt(1, billCache.getEmployeeID());
            ps.setLong(2, billCache.getPackageID());
            ps.setInt(3, billCache.getFee());
            ps.setBoolean(4, billCache.isStatus());
            ps.execute();
        } catch (Exception e) {
            e.printStackTrace();
            check = false;
        }
        return check;
    }

    public static boolean addAllBillCache(List<BillCache> list) {
        boolean check = true;
        String sql = "INSERT INTO BillCache VALUES (?,?,?,?)";
        try (Connection con = CONTEXT.getConnection();
                PreparedStatement ps = con.prepareStatement(sql)) {
            for (BillCache cache : list) {
                ps.setInt(1, cache.getEmployeeID());
                ps.setLong(2, cache.getPackageID());
                ps.setInt(3, cache.getFee());
                ps.setBoolean(4, cache.isStatus());
                ps.addBatch();
            }
            ps.executeBatch();
        } catch (Exception e) {
            e.printStackTrace();
            check = false;
        }
        return check;
    }

    public static boolean deleteBillCache(long packageID) {
        boolean check = true;
        String sql = "DELETE FROM BillCache WHERE packageID = ?";
        try (Connection con = CONTEXT.getConnection();
                PreparedStatement ps = con.prepareStatement(sql)) {
            ps.setLong(1, packageID);
            ps.execute();
        } catch (Exception e) {
            e.printStackTrace();
            check = false;
        }
        return check;
    }
}
