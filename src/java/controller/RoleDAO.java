package controller;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.List;
import model.Role;

public class RoleDAO {

    private static final DBContext CONTEXT = new DBContext();

    public static Role selectByUsername(String username) {
        Role role = null;
        String sql = "SELECT Role.* FROM Employee INNER JOIN Role ON Employee.roleID = Role.roleID WHERE username = ?";
        try (Connection con = CONTEXT.getConnection();
                PreparedStatement ps = con.prepareStatement(sql)) {
            ps.setString(1, username);
            try (ResultSet rs = ps.executeQuery()) {
                if (rs.next()) {
                    int roleID = rs.getInt("roleID");
                    String roleName = rs.getString("roleName");
                    String url = rs.getString("url");
                    String description = rs.getString("description");
                    role = new Role(roleID, roleName, url, description);
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return role;
    }

    public static List<Role> selectAllRole() {
        List<Role> listRole = new ArrayList<>();
        String sql = "SELECT * FROM Role WHERE roleID > 1";
        try (Connection con = CONTEXT.getConnection();
                PreparedStatement ps = con.prepareStatement(sql)) {
            try (ResultSet rs = ps.executeQuery()) {
                while (rs.next()) {
                    int roleID = rs.getInt("roleID");
                    String roleName = rs.getString("roleName");
                    String url = rs.getString("url");
                    String description = rs.getString("description");
                    listRole.add(new Role(roleID, roleName, url, description));
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return listRole;
    }

    public static boolean addAllRole(List<Role> list) {
        boolean check = true;
        String sql = "INSERT INTO Role VALUES (?,?,?,?)";
        try (Connection con = CONTEXT.getConnection();
                PreparedStatement ps = con.prepareStatement(sql)) {
            for (Role role : list) {
                ps.setInt(1, role.getRoleID());
                ps.setString(2, role.getRoleName());
                ps.setString(3, role.getUrl());
                ps.setString(4, role.getDescription());
                ps.addBatch();
            }
            ps.executeBatch();
        } catch (Exception e) {
            e.printStackTrace();
            check = false;
        }
        return check;
    }
}
