package model;

import java.util.Date;

public class Employee {

    private int employeeID;
    private String postID;
    private int roleID;
    private String username;
    private String name;
    private boolean gender;
    private Date dob;
    private String phone;
    private Date hireDate;
    private String identityNum;

    public Employee() {
    }

    public Employee(String postID, int roleID, String username, String name, boolean gender, Date dob, String phone, Date hireDate, String identityNum) {
        this.postID = postID;
        this.roleID = roleID;
        this.username = username;
        this.name = name;
        this.gender = gender;
        this.dob = dob;
        this.phone = phone;
        this.hireDate = hireDate;
        this.identityNum = identityNum;
    }

    public Employee(int employeeID, String postID, int roleID, String username, String name, boolean gender, Date dob, String phone, Date hireDate, String identityNum) {
        this.employeeID = employeeID;
        this.postID = postID;
        this.roleID = roleID;
        this.username = username;
        this.name = name;
        this.gender = gender;
        this.dob = dob;
        this.phone = phone;
        this.hireDate = hireDate;
        this.identityNum = identityNum;
    }

    public int getEmployeeID() {
        return employeeID;
    }

    public void setEmployeeID(int employeeID) {
        this.employeeID = employeeID;
    }

    public String getPostID() {
        return postID;
    }

    public void setPostID(String postID) {
        this.postID = postID;
    }

    public int getRoleID() {
        return roleID;
    }

    public void setRoleID(int roleID) {
        this.roleID = roleID;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public boolean isGender() {
        return gender;
    }

    public void setGender(boolean gender) {
        this.gender = gender;
    }

    public Date getDob() {
        return dob;
    }

    public void setDob(Date dob) {
        this.dob = dob;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public Date getHireDate() {
        return hireDate;
    }

    public void setHireDate(Date hireDate) {
        this.hireDate = hireDate;
    }

    public String getIdentityNum() {
        return identityNum;
    }

    public void setIdentityNum(String identityNum) {
        this.identityNum = identityNum;
    }

    @Override
    public String toString() {
        return "Employee{" + "employeeID=" + employeeID + ", postID=" + postID + ", roleID=" + roleID + ", username=" + username + ", name=" + name + ", gender=" + gender + ", dob=" + dob + ", phone=" + phone + ", hireDate=" + hireDate + ", identityNum=" + identityNum + '}';
    }
}
