package model;

import java.util.Date;

public class Track {

    private int trackID;
    private int employeeID;
    private long packageID;
    private String postID;
    private Date time;
    private int status;
    private String description;

    public Track() {
    }

    public Track(int employeeID, long packageID, String postID, Date time, int status, String description) {
        this.employeeID = employeeID;
        this.packageID = packageID;
        this.postID = postID;
        this.time = time;
        this.status = status;
        this.description = description;
    }

    public Track(int trackID, int employeeID, long packageID, String postID, Date time, int status, String description) {
        this.trackID = trackID;
        this.employeeID = employeeID;
        this.packageID = packageID;
        this.postID = postID;
        this.time = time;
        this.status = status;
        this.description = description;
    }

    public int getTrackID() {
        return trackID;
    }

    public void setTrackID(int trackID) {
        this.trackID = trackID;
    }

    public int getEmployeeID() {
        return employeeID;
    }

    public void setEmployeeID(int employeeID) {
        this.employeeID = employeeID;
    }

    public long getPackageID() {
        return packageID;
    }

    public void setPackageID(long packageID) {
        this.packageID = packageID;
    }

    public String getPostID() {
        return postID;
    }

    public void setPostID(String postID) {
        this.postID = postID;
    }

    public Date getTime() {
        return time;
    }

    public void setTime(Date time) {
        this.time = time;
    }

    public int getStatus() {
        return status;
    }

    public void setStatus(int status) {
        this.status = status;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    @Override
    public String toString() {
        return "Track{" + "trackID=" + trackID + ", employeeID=" + employeeID + ", packageID=" + packageID + ", postID=" + postID + ", time=" + time + ", status=" + status + ", description=" + description + '}';
    }
}
