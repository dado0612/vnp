package model;

public class Post {

    private String postID;
    private String locationID;
    private String postName;
    private String postAddress;

    public Post() {
    }

    public Post(String postID, String locationID, String postName, String postAddress) {
        this.postID = postID;
        this.locationID = locationID;
        this.postName = postName;
        this.postAddress = postAddress;
    }

    public String getPostID() {
        return postID;
    }

    public void setPostID(String postID) {
        this.postID = postID;
    }

    public String getLocationID() {
        return locationID;
    }

    public void setLocationID(String locationID) {
        this.locationID = locationID;
    }

    public String getPostName() {
        return postName;
    }

    public void setPostName(String postName) {
        this.postName = postName;
    }

    public String getPostAddress() {
        return postAddress;
    }

    public void setPostAddress(String postAddress) {
        this.postAddress = postAddress;
    }

    @Override
    public String toString() {
        return "Post{" + "postID=" + postID + ", locationID=" + locationID + ", postName=" + postName + ", postAddress=" + postAddress + '}';
    }
}
