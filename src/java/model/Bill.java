package model;

import java.util.Date;

public class Bill {

    private int billID;
    private String postID;
    private long packageID;
    private Date time;
    private int fee;

    public Bill() {
    }

    public Bill(String postID, long packageID, Date time, int fee) {
        this.postID = postID;
        this.packageID = packageID;
        this.time = time;
        this.fee = fee;
    }

    public Bill(int billID, String postID, long packageID, Date time, int fee) {
        this.billID = billID;
        this.postID = postID;
        this.packageID = packageID;
        this.time = time;
        this.fee = fee;
    }

    public int getBillID() {
        return billID;
    }

    public void setBillID(int billID) {
        this.billID = billID;
    }

    public String getPostID() {
        return postID;
    }

    public void setPostID(String postID) {
        this.postID = postID;
    }

    public long getPackageID() {
        return packageID;
    }

    public void setPackageID(long packageID) {
        this.packageID = packageID;
    }

    public Date getTime() {
        return time;
    }

    public void setTime(Date time) {
        this.time = time;
    }

    public int getFee() {
        return fee;
    }

    public void setFee(int fee) {
        this.fee = fee;
    }

    @Override
    public String toString() {
        return "Bill{" + "billID=" + billID + ", postID=" + postID + ", packageID=" + packageID + ", time=" + time + ", fee=" + fee + '}';
    }
}
