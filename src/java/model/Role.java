package model;

public class Role {

    private int roleID;
    private String roleName;
    private String url;
    private String description;

    public Role() {
    }

    public Role(int roleID, String roleName, String url, String description) {
        this.roleID = roleID;
        this.roleName = roleName;
        this.url = url;
        this.description = description;
    }

    public int getRoleID() {
        return roleID;
    }

    public void setRoleID(int roleID) {
        this.roleID = roleID;
    }

    public String getRoleName() {
        return roleName;
    }

    public void setRoleName(String roleName) {
        this.roleName = roleName;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    @Override
    public String toString() {
        return "Role{" + "roleID=" + roleID + ", roleName=" + roleName + ", url=" + url + ", description=" + description + '}';
    }
}
