package model;

public class Package {

    private long packageID;
    private String postID;
    private String receiverLocation;
    private String senderName;
    private String senderPhone;
    private String receiverName;
    private String receiverPhone;
    private String receiverMail;
    private String receiverAddress;
    private long price;
    private String description;

    public Package() {
    }

    public Package(long packageID, String postID, String receiverLocation, String senderName, String senderPhone, String receiverName, String receiverPhone, String receiverMail, String receiverAddress, long price, String description) {
        this.packageID = packageID;
        this.postID = postID;
        this.receiverLocation = receiverLocation;
        this.senderName = senderName;
        this.senderPhone = senderPhone;
        this.receiverName = receiverName;
        this.receiverPhone = receiverPhone;
        this.receiverMail = receiverMail;
        this.receiverAddress = receiverAddress;
        this.price = price;
        this.description = description;
    }

    public long getPackageID() {
        return packageID;
    }

    public void setPackageID(long packageID) {
        this.packageID = packageID;
    }

    public String getPostID() {
        return postID;
    }

    public void setPostID(String postID) {
        this.postID = postID;
    }

    public String getReceiverLocation() {
        return receiverLocation;
    }

    public void setReceiverLocation(String receiverLocation) {
        this.receiverLocation = receiverLocation;
    }

    public String getSenderName() {
        return senderName;
    }

    public void setSenderName(String senderName) {
        this.senderName = senderName;
    }

    public String getSenderPhone() {
        return senderPhone;
    }

    public void setSenderPhone(String senderPhone) {
        this.senderPhone = senderPhone;
    }

    public String getReceiverName() {
        return receiverName;
    }

    public void setReceiverName(String receiverName) {
        this.receiverName = receiverName;
    }

    public String getReceiverPhone() {
        return receiverPhone;
    }

    public void setReceiverPhone(String receiverPhone) {
        this.receiverPhone = receiverPhone;
    }

    public String getReceiverMail() {
        return receiverMail;
    }

    public void setReceiverMail(String receiverMail) {
        this.receiverMail = receiverMail;
    }

    public String getReceiverAddress() {
        return receiverAddress;
    }

    public void setReceiverAddress(String receiverAddress) {
        this.receiverAddress = receiverAddress;
    }

    public long getPrice() {
        return price;
    }

    public void setPrice(long price) {
        this.price = price;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    @Override
    public String toString() {
        return "Package{" + "packageID=" + packageID + ", postID=" + postID + ", receiverLocation=" + receiverLocation + ", senderName=" + senderName + ", senderPhone=" + senderPhone + ", receiverName=" + receiverName + ", receiverPhone=" + receiverPhone + ", receiverMail=" + receiverMail + ", receiverAddress=" + receiverAddress + ", price=" + price + ", description=" + description + '}';
    }
}
