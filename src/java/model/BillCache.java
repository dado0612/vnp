package model;

public class BillCache {

    private int billCacheID;
    private int employeeID;
    private long packageID;
    private int fee;
    private boolean status;

    public BillCache() {
    }

    public BillCache(int employeeID, long packageID, int fee, boolean status) {
        this.employeeID = employeeID;
        this.packageID = packageID;
        this.fee = fee;
        this.status = status;
    }

    public BillCache(int billCacheID, int employeeID, long packageID, int fee, boolean status) {
        this.billCacheID = billCacheID;
        this.employeeID = employeeID;
        this.packageID = packageID;
        this.fee = fee;
        this.status = status;
    }

    public int getBillCacheID() {
        return billCacheID;
    }

    public void setBillCacheID(int billCacheID) {
        this.billCacheID = billCacheID;
    }

    public int getEmployeeID() {
        return employeeID;
    }

    public void setEmployeeID(int employeeID) {
        this.employeeID = employeeID;
    }

    public long getPackageID() {
        return packageID;
    }

    public void setPackageID(long packageID) {
        this.packageID = packageID;
    }

    public int getFee() {
        return fee;
    }

    public void setFee(int fee) {
        this.fee = fee;
    }

    public boolean isStatus() {
        return status;
    }

    public void setStatus(boolean status) {
        this.status = status;
    }

    @Override
    public String toString() {
        return "BillCache{" + "billCacheID=" + billCacheID + ", employeeID=" + employeeID + ", packageID=" + packageID + ", fee=" + fee + ", status=" + status + '}';
    }
}
