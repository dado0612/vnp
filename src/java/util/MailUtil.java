package util;

import com.google.zxing.BarcodeFormat;
import com.google.zxing.WriterException;
import com.google.zxing.client.j2se.MatrixToImageWriter;
import com.google.zxing.common.BitMatrix;
import com.google.zxing.qrcode.QRCodeWriter;
import config.MailConfig;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import javax.imageio.ImageIO;
import org.apache.commons.mail.DefaultAuthenticator;
import org.apache.commons.mail.EmailAttachment;
import org.apache.commons.mail.EmailException;
import org.apache.commons.mail.MultiPartEmail;

public class MailUtil {

    private static boolean generateQRCodeImage(long packageID, String path) {
        try {
            QRCodeWriter qrCodeWriter = new QRCodeWriter();
            BitMatrix bitMatrix = qrCodeWriter.encode(String.valueOf(packageID), BarcodeFormat.QR_CODE, 350, 350);
            BufferedImage bufferedImage = MatrixToImageWriter.toBufferedImage(bitMatrix);
            ImageIO.write(bufferedImage, "PNG", new File(path));
            return true;
        } catch (WriterException | IOException e) {
            e.printStackTrace();
        }
        return false;
    }

    public static boolean sendEmailQRCode(long packageID, String receiverMail, String path) {
        if (generateQRCodeImage(packageID, path)) {
            try {
                EmailAttachment attachment = new EmailAttachment();
                attachment.setPath(path);
                attachment.setDisposition(EmailAttachment.ATTACHMENT);
                attachment.setDescription("QR Code");
                attachment.setName("QR_CODE.png");

                // Create the email message
                MultiPartEmail email = new MultiPartEmail();
                email.setHostName("smtp.googlemail.com");
                email.setSmtpPort(465);
                email.setAuthenticator(new DefaultAuthenticator(MailConfig.EMAIL, MailConfig.PASSWORD));

                // Force for email
                email.setSSLOnConnect(true);
                email.addTo(receiverMail);
                email.setFrom(MailConfig.EMAIL);
                email.setSubject("Fly Post");
                email.setMsg("You have new package. Here is your package QR code.");

                // Attach and send mail
                email.attach(attachment);
                email.send();
                return true;
            } catch (EmailException e) {
                e.printStackTrace();
            }
        }
        return false;
    }
}
