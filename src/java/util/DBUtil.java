package util;

import config.AttributeConfig;
import config.RandomConfig;
import config.RoleConfig;
import config.TrackStatus;
import controller.AccountDAO;
import controller.BillCacheDAO;
import controller.BillDAO;
import controller.EmployeeDAO;
import controller.LocationDAO;
import controller.PackageDAO;
import controller.PostDAO;
import controller.RoleDAO;
import controller.TrackDAO;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;
import java.util.TreeMap;
import model.Account;
import model.Bill;
import model.BillCache;
import model.Employee;
import model.Location;
import model.Package;
import model.Post;
import model.Role;
import model.Track;

public class DBUtil {

    private static ArrayList<Account> LIST_ACCOUNT;
    private static ArrayList<Bill> LIST_BILL;
    private static ArrayList<BillCache> LIST_CACHE;
    private static ArrayList<Employee> LIST_EMPLOYEE;
    private static ArrayList<Location> LIST_LOCATION;
    private static ArrayList<Package> LIST_PACKAGE;
    private static ArrayList<Post> LIST_POST;
    private static ArrayList<Role> LIST_ROLE;
    private static TreeMap<Long, Track> MAP_TRACK;
    private static TreeMap<String, Integer> MAP_USERNAME;

    public static boolean dateDiff(long date) {
        return RandomConfig.MAX_DATE - date > RandomConfig.MAX_INC;
    }

    public static long ranDate(long max, long min) {
        double d = Math.random() * (max - min);
        return Math.round(d) + min;
    }

    public static String ranStr(int len) {
        String s = "";
        for (int i = 0; i < len; i++) {
            s += Math.round(Math.random() * 9);
        }
        return s;
    }

    public static String ranLoc(boolean status) {
        String s = "";
        if (status) {
            s += "0" + Math.round(Math.random() * 4 + 1);
            s += "0" + Math.round(Math.random() * 6 + 1);
            s += "0" + Math.round(Math.random() * 6 + 1);
        } else {
            s += "0" + Math.round(Math.random() + 1);
            s += "0" + Math.round(Math.random() * 2 + 1);
            s += "0" + Math.round(Math.random() * 2 + 1);
        }
        return s;
    }

    public static ArrayList<String> readRoad() {
        ArrayList<String> list = new ArrayList<>();
        try {
            File file = new File("db/road.txt");
            BufferedReader br = new BufferedReader(new FileReader(file));
            String s;
            while ((s = br.readLine()) != null) {
                list.add(s);
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
        return list;
    }

    public static ArrayList<String> readName() {
        ArrayList<String> list = new ArrayList<>();
        try {
            File file = new File("db/name.txt");
            BufferedReader br = new BufferedReader(new FileReader(file));
            String s;
            while ((s = br.readLine()) != null) {
                list.add(s);
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
        return list;
    }

    public static ArrayList<String> readLoc() {
        ArrayList<String> list = new ArrayList<>();
        try {
            File file = new File("db/location.txt");
            BufferedReader br = new BufferedReader(new FileReader(file));
            String s;
            while ((s = br.readLine()) != null) {
                list.add(s);
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
        return list;
    }

    public static ArrayList<String> readEmp() {
        ArrayList<String> list = new ArrayList<>();
        try {
            File file = new File("db/employee.txt");
            BufferedReader br = new BufferedReader(new FileReader(file));
            String s = br.readLine();
            while ((s = br.readLine()) != null) {
                list.add(s);
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
        Collections.shuffle(list);
        list.add(0, "Nguyen Duc Quynh");
        return list;
    }

    public static void createRole() {
        LIST_ROLE = new ArrayList<>();
        LIST_ROLE.add(new Role(RoleConfig.RETIRE, "RETIRE", "index/error.html", "Employee retired"));
        LIST_ROLE.add(new Role(RoleConfig.ADMIN, "ADMIN", "admin/dashboard.jsp", "Manage all employee"));
        LIST_ROLE.add(new Role(RoleConfig.MANAGER, "MANAGER", "dashboard/dashboard.jsp", "Check IN/OUT"));
        LIST_ROLE.add(new Role(RoleConfig.SHIPPER, "SHIPPER", "", "Delivery package"));
    }

    public static void createLoc() {
        LIST_LOCATION = new ArrayList<>();
        LIST_POST = new ArrayList<>();
        ArrayList<String> list = readLoc();

        for (String s : list) {
            String[] arr = s.split("[|]");
            String locationID = arr[0];
            String postName = "";

            switch (locationID.length()) {
                case 2: {
                    locationID += "0101";
                    postName = "Buu cuc Thanh pho " + arr[1];
                }
                break;
                case 4: {
                    locationID += "01";
                    postName = "Buu cuc Quan " + arr[1];
                }
                break;
                case 6: {
                    postName = "Buu cuc Phuong " + arr[1];
                }
            }

            LIST_LOCATION.add(new Location(arr[0], arr[1]));
            String postAddress = "So " + Math.round(Math.random() * 100) + " Duong " + arr[2];
            LIST_POST.add(new Post(arr[0], locationID, postName, postAddress));
        }
    }

    public static void addEmp(String postID, int roleID, String name) {
        String[] arr = name.toLowerCase().split("\\s");
        String username = arr[2] + arr[0].charAt(0) + arr[1].charAt(0);

        if (MAP_USERNAME.containsKey(username)) {
            int x = MAP_USERNAME.get(username) + 1;
            MAP_USERNAME.put(username, x);
            username += x;
        } else {
            MAP_USERNAME.put(username, 1);
        }

        boolean gender = (Math.random() > 0.5);
        long date = ranDate(RandomConfig.MAX_DOB, RandomConfig.MIN_DOB);
        Date dob = new Date(date);
        String phone = "09" + ranStr(8);

        date = ranDate(RandomConfig.MIN_DATE, RandomConfig.MIN_HIRE);
        Date hireDate = new Date(date);
        String identityNum = ranStr(12);

        LIST_EMPLOYEE.add(new Employee(postID, roleID, username, name, gender, dob, phone, hireDate, identityNum));
        LIST_ACCOUNT.add(new Account(username, AttributeConfig.DEFAULT_PASSWORD));
    }

    public static void createEmp() {
        LIST_EMPLOYEE = new ArrayList<>();
        LIST_ACCOUNT = new ArrayList<>();
        MAP_USERNAME = new TreeMap<>();
        ArrayList<String> list = readEmp();

        int id = 1;
        for (Post post : LIST_POST) {
            String postID = post.getPostID();
            if (postID.equals("02")) {
                addEmp(postID, RoleConfig.ADMIN, list.get(0));
            } else {
                addEmp(postID, RoleConfig.MANAGER, list.get(id));
                id++;
            }

            for (int i = 0; i < 2; i++) {
                addEmp(postID, RoleConfig.MANAGER, list.get(id));
                id++;
            }
            for (int i = 0; i < 3; i++) {
                addEmp(postID, RoleConfig.SHIPPER, list.get(id));
                id++;
            }
        }
    }

    public static int getEmployeeID(String postID) {
        int id = 0;
        while (id < LIST_EMPLOYEE.size()) {
            Employee emp = LIST_EMPLOYEE.get(id);
            if (emp.getPostID().equals(postID) && emp.getRoleID() == RoleConfig.MANAGER) {
                break;
            } else {
                id++;
            }
        }

        if (postID.equals("02")) {
            return id + (int) Math.round(Math.random()) + 1;
        } else {
            return id + (int) Math.round(Math.random() * 2) + 1;
        }
    }

    public static long addTrack(long packageID, String postID, long date, int status) {
        int employeeID = getEmployeeID(postID);
        date += ranDate(RandomConfig.MAX_INC + RandomConfig.MIN_INC, RandomConfig.MAX_INC);
        while (MAP_TRACK.containsKey(date)) {
            date += ranDate(RandomConfig.MIN_INC, 0);
        }

        if (status > TrackStatus.OUT || dateDiff(date)) {
            Date time = new Date(date);
            MAP_TRACK.put(date, new Track(employeeID, packageID, postID, time, status, "NT"));
        }
        return date;
    }

    public static void addPackage(int num, long max, long min, boolean status) {
        ArrayList<String> listName = readName();
        ArrayList<String> listRoad = readRoad();

        for (int i = 0; i < num; i++) {
            long packageID = ranDate(max, min);
            while (MAP_TRACK.containsKey(packageID)) {
                packageID = ranDate(max, min);
            }
            String postID = ranLoc(status);
            String receiverLocation = ranLoc(status);
            String senderName = listName.get((int) Math.round(Math.random() * (listName.size() - 1)));
            String senderPhone = "09" + ranStr(8);
            String receiverName = listName.get((int) Math.round(Math.random() * (listName.size() - 1)));;
            String receiverPhone = "09" + ranStr(8);
            String[] arr = receiverName.toLowerCase().split("\\s");
            String receiverMail = arr[2] + arr[0].charAt(0) + arr[1].charAt(0) + "@gmail.com";
            String receiverAddress = "So " + Math.round(Math.random() * 100) + " Duong ";
            receiverAddress += listRoad.get((int) Math.round(Math.random() * (listRoad.size() - 1)));
            long price = Math.round(Math.random() * 10000) * 1000;
            String description;
            if (Math.random() > 0.5) {
                description = "Height = " + Math.round(Math.random() * 1000);
                description += " | Width = " + Math.round(Math.random() * 1000);
                description += " | Depth = " + Math.round(Math.random() * 1000);
            } else {
                description = "Weight = " + Math.round(Math.random() * 10000);
            }

            LIST_PACKAGE.add(new Package(packageID, postID, receiverLocation, senderName, senderPhone,
                    receiverName, receiverPhone, receiverMail, receiverAddress, price, description));
            ArrayList<String> list = (ArrayList) RouteUtil.findPath(postID, receiverLocation);
            long date = addTrack(packageID, postID, packageID, TrackStatus.INIT);

            boolean stop = true;
            if (!list.isEmpty()) {
                for (int j = 0; j < list.size() - 1; j++) {
                    if (Math.random() > 0.96) {
                        stop = false;
                        break;
                    }
                    date = addTrack(packageID, list.get(j), date, TrackStatus.OUT);

                    if (Math.random() > 0.96) {
                        stop = false;
                        break;
                    }
                    date = addTrack(packageID, list.get(j + 1), date, TrackStatus.IN);
                }
            }

            boolean check = Math.random() > 0.5;
            int employeeID = getEmployeeID(postID);
            int fee = (int) Math.round(Math.random() * 1000) * 1000 + 6000;
            if (stop && dateDiff(date)) {
                date = addTrack(packageID, receiverLocation, date, TrackStatus.SHIP);
                int statusTrack = (Math.random() > 0.02) ? TrackStatus.OK : TrackStatus.ERR;
                date = addTrack(packageID, receiverLocation, date, statusTrack);

                if (statusTrack == TrackStatus.OK) {
                    if (check) {
                        LIST_BILL.add(new Bill(postID, packageID, new Date(date), fee));
                    } else {
                        LIST_BILL.add(new Bill(receiverLocation, packageID, new Date(date), fee));
                    }
                } else {
                    LIST_CACHE.add(new BillCache(employeeID, packageID, fee, check));
                }
            } else {
                LIST_CACHE.add(new BillCache(employeeID, packageID, fee, check));
            }
        }
    }

    public static void createPackage() {
        LIST_PACKAGE = new ArrayList<>();
        MAP_TRACK = new TreeMap<>();
        LIST_BILL = new ArrayList<>();
        LIST_CACHE = new ArrayList<>();

        addPackage(50000, RandomConfig.MIN_DATE, RandomConfig.MIN_HIRE, true);
        addPackage(2753, RandomConfig.MAX_DATE, RandomConfig.MIN_DATE, false);
    }

    public static void process() {
        RoleDAO.addAllRole(LIST_ROLE);
        LocationDAO.addAllLocation(LIST_LOCATION);
        PostDAO.addAllPost(LIST_POST);
        System.out.println("First");

        AccountDAO.addAllAccount(LIST_ACCOUNT);
        EmployeeDAO.addAllEmployee(LIST_EMPLOYEE);
        System.out.println("Second");

        PackageDAO.addAllPackage(LIST_PACKAGE);
        System.out.println("Package");
        TrackDAO.addAllTrack(MAP_TRACK);
        System.out.println("Track");

        BillDAO.addAllBill(LIST_BILL);
        BillCacheDAO.addAllBillCache(LIST_CACHE);
        System.out.println("Done!!!");
    }

    public static void main(String[] args) {
        createRole();
        createLoc();
        createEmp();
        createPackage();
        process();
    }
}
