package util;

import config.DateConfig;
import java.text.ParseException;
import java.util.Date;

public class DateUtil {

    public static String formatDateTime(Date time) {
        return DateConfig.DATE_TIME_FORMAT.format(time);
    }

    public static String formatDate(Date time) {
        return DateConfig.DATE_FORMAT.format(time);
    }

    public static String formatDateHTML(Date time) {
        return DateConfig.DATE_FORMAT_HTML.format(time);
    }

    public static Date parseDate(String date) throws ParseException {
        return DateConfig.DATE_FORMAT.parse(date);
    }
}
