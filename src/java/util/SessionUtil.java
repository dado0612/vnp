package util;

import config.AttributeConfig;
import controller.EmployeeDAO;
import javafx.util.Pair;
import javax.servlet.http.HttpSession;
import model.Employee;

public class SessionUtil {

    public static Pair<Boolean, Employee> checkSession(HttpSession session, int... roleID) {
        String username = String.valueOf(session.getAttribute(AttributeConfig.USERNAME));
        Employee emp = EmployeeDAO.selectByUsername(username);

        boolean check = true;
        if (emp != null) {
            for (int id : roleID) {
                if (emp.getRoleID() == id) {
                    check = false;
                }
            }
        }
        return new Pair(check, emp);
    }
}
