package util;

import controller.PostDAO;
import java.util.ArrayList;
import java.util.List;
import model.Post;

public class RouteUtil {

    private static int findFirstDiff(String src, String dst) {
        String[] arrs = src.split("(?<=\\G.{2})");
        String[] arrd = dst.split("(?<=\\G.{2})");
        int id = 0;
        while (id < Math.min(arrs.length, arrd.length) && arrs[id].equals(arrd[id])) {
            id++;
        }
        return id;
    }

    public static List<String> findPath(String src, String dst) {
        List<String> list = new ArrayList<>();
        int id = findFirstDiff(src, dst);
        if (id * 2 >= src.length()) {
            list.add(src);
        } else {
            for (int i = src.length(); i > id * 2; i -= 2) {
                list.add(src.substring(0, i));
            }
        }
        for (int i = id * 2; i < dst.length(); i += 2) {
            list.add(dst.substring(0, i + 2));
        }
        return list;
    }

    public static List<Post> getPath(String src, String dst) {
        List<String> path = findPath(src, dst);
        List<Post> list = new ArrayList<>();
        path.forEach((s) -> {
            list.add(PostDAO.selectByPostID(s));
        });
        return list;
    }

    public static Post getNextPost(String src, String dst) {
        int id = findFirstDiff(src, dst) + 1;
        String postID = "";
        if (src.length() > id * 2) {
            postID = src.substring(0, src.length() - 2);
        } else {
            postID = dst.substring(0, Math.min(dst.length(), id * 2));
        }
        return PostDAO.selectByPostID(postID);
    }
}
