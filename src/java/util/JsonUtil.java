package util;

import config.RandomConfig;
import config.TrackStatus;
import controller.BillCacheDAO;
import controller.EmployeeDAO;
import controller.LocationDAO;
import controller.PackageDAO;
import controller.PostDAO;
import java.util.Date;
import java.util.List;
import java.util.concurrent.TimeUnit;
import model.BillCache;
import model.Location;
import model.Package;
import model.Post;
import model.Track;
import org.json.simple.JSONArray;
import org.json.simple.JSONObject;

public class JsonUtil {

    public static String encodeJsonArrayLocation(List<Location> list) {
        JSONObject responseDetailsJson = new JSONObject();
        JSONArray jsonArr = new JSONArray();
        for (Location loc : list) {
            JSONObject jsonObj = new JSONObject();
            jsonObj.put("locationID", loc.getLocationID());
            jsonObj.put("locationName", loc.getLocationName());
            jsonArr.add(jsonObj);
        }
        responseDetailsJson.put("location", jsonArr);
        return responseDetailsJson.toString();
    }

    public static String encodeJsonObjectPackage(Package pac) {
        JSONObject jsonObj = new JSONObject();
        jsonObj.put("packageID", pac.getPackageID());
        jsonObj.put("postID", pac.getPostID());
        jsonObj.put("receiverLocation", pac.getReceiverLocation());
        jsonObj.put("senderName", pac.getReceiverName());
        jsonObj.put("senderPhone", pac.getSenderPhone());
        jsonObj.put("receiverName", pac.getReceiverName());
        jsonObj.put("receiverPhone", pac.getReceiverPhone());
        jsonObj.put("receiverMail", pac.getReceiverMail());
        jsonObj.put("receiverAddress", pac.getReceiverAddress());
        jsonObj.put("price", pac.getPrice());
        jsonObj.put("description", pac.getDescription());
        return jsonObj.toString();
    }

    public static String encodeJsonCheckInOut(Package pac, int status, Post nextPost) {
        BillCache bill = BillCacheDAO.selectByPackageID(pac.getPackageID());
        JSONObject jsonObj = new JSONObject();
        jsonObj.put("status", status);
        jsonObj.put("senderName", pac.getSenderName());
        jsonObj.put("senderPhone", pac.getSenderPhone());
        jsonObj.put("receiverName", pac.getReceiverName());
        jsonObj.put("receiverPhone", pac.getReceiverPhone());
        String s = pac.getReceiverAddress() + ", " + LocationDAO.getFullNameCommune(pac.getReceiverLocation());
        jsonObj.put("receiverAddress", s);
        jsonObj.put("receiverMail", pac.getReceiverMail());
        jsonObj.put("isPaid", bill.isStatus() ? "Yes" : "No");
        jsonObj.put("price", pac.getPrice());
        jsonObj.put("fee", bill.getFee());
        if (status == TrackStatus.OUT) {
            jsonObj.put("nextPostID", nextPost.getPostID());
            jsonObj.put("nextPostName", nextPost.getPostName());
        }
        return jsonObj.toString();
    }

    public static String encodeJsonForTrackJSP(Package pac, List<Track> tracks) {
        JSONObject jsonObjPac = new JSONObject();
        jsonObjPac.put("packageID", pac.getPackageID());
        jsonObjPac.put("description", pac.getDescription());
        jsonObjPac.put("status", BillCacheDAO.selectByPackageID(pac.getPackageID()) == null ? "Finished" : "Not Finished");
        jsonObjPac.put("postIDSrc", pac.getPostID() + " - " + PostDAO.selectByPostID(pac.getPostID()).getPostName());
        jsonObjPac.put("postIDDest", pac.getReceiverLocation() + " - " + PostDAO.selectByPostID(pac.getReceiverLocation()).getPostName());

        JSONObject responseDetailsJson = new JSONObject();
        JSONArray jsonArr = new JSONArray();
        List<Post> listPostNext = RouteUtil.getPath(tracks.get(0).getPostID(), pac.getReceiverLocation());
        for (int i = listPostNext.size() - 1; i > 0; i--) {
            Post post = listPostNext.get(i);
            JSONObject jsonObj = new JSONObject();
            jsonObj.put("postID", post.getPostID());
            jsonObj.put("postName", post.getPostName());
            jsonObj.put("date", "");
            jsonObj.put("time", "");
            jsonObj.put("status", "Future");
            jsonObj.put("location", post.getPostAddress() + " - " + LocationDAO.getFullNameCommune(post.getLocationID()));
            jsonObj.put("active", "false");
            jsonArr.add(jsonObj);
        }

        for (Track track : tracks) {
            JSONObject jsonObj = new JSONObject();
            jsonObj.put("postID", track.getPostID());
            jsonObj.put("postName", PostDAO.selectByPostID(track.getPostID()).getPostName());
            String datetime = DateUtil.formatDateTime(track.getTime());
            jsonObj.put("date", datetime.substring(0, 10));
            jsonObj.put("time", datetime.substring(10));
            String status = "";
            switch (track.getStatus()) {
                case TrackStatus.INIT:
                    status = "Initial";
                    break;
                case TrackStatus.IN:
                    status = "Check IN";
                    break;
                case TrackStatus.OUT:
                    status = "Check OUT";
                    break;
                case TrackStatus.SHIP:
                    status = "Shipper";
                    break;
                case TrackStatus.OK:
                    status = "Finished";
                    break;
                case TrackStatus.ERR:
                    status = "Ship Fail";
                    break;
            }
            jsonObj.put("status", status);
            jsonObj.put("location", PostDAO.selectByPostID(track.getPostID()).getPostAddress() + " - "
                    + LocationDAO.getFullNameCommune(PostDAO.selectByPostID(track.getPostID()).getLocationID()));
            jsonObj.put("active", "true");
            jsonArr.add(jsonObj);
        }

        responseDetailsJson.put("package", jsonObjPac);
        responseDetailsJson.put("tracks", jsonArr);
        return responseDetailsJson.toString();
    }

    public static String encodeJsonArrayPost(List<Post> list) {
        JSONObject responseDetailsJson = new JSONObject();
        JSONArray jsonArr = new JSONArray();
        for (Post p : list) {
            JSONObject jsonObj = new JSONObject();
            jsonObj.put("postID", p.getPostID());
            jsonObj.put("postName", p.getPostName());
            jsonObj.put("address", p.getPostAddress() + ", " + LocationDAO.getFullNameCommune(p.getLocationID()));
            jsonArr.add(jsonObj);
        }
        responseDetailsJson.put("posts", jsonArr);
        return responseDetailsJson.toString();
    }

    public static String encodeJsonIndex() {
        JSONObject jsonObj = new JSONObject();
        jsonObj.put("totalPost", PostDAO.countPost());
        jsonObj.put("totalEmployee", EmployeeDAO.countEmployee());
        jsonObj.put("totalPackage", PackageDAO.countPackage());
        Date now = new Date();
        Date workFrom = new Date(RandomConfig.MIN_HIRE);
        long totalDayWork = TimeUnit.DAYS.convert(now.getTime() - workFrom.getTime(), TimeUnit.MILLISECONDS);
        jsonObj.put("totalDay", totalDayWork);
        return jsonObj.toString();
    }
}
