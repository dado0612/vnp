package servlet;

import config.RoleConfig;
import config.URLConfig;
import controller.EmployeeDAO;
import java.io.IOException;
import javafx.util.Pair;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import model.Employee;
import util.SessionUtil;

@WebServlet(name = "FireEmployeeServlet", urlPatterns = {"/FireEmployeeServlet"})
public class FireEmployeeServlet extends HttpServlet {

    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        Pair<Boolean, Employee> pair = SessionUtil.checkSession(request.getSession(), RoleConfig.ADMIN);
        if (pair.getKey()) {
            response.sendRedirect(URLConfig.LOGIN);
        } else {
            try {
                int employeeID = Integer.parseInt(request.getParameter("employeeID"));
                Employee emp = EmployeeDAO.selectByEmployeeID(employeeID);
                if (emp != null && EmployeeDAO.deleteEmployee(employeeID)) {
                    response.sendRedirect(URLConfig.EMPLOYEE);
                    return;
                }
            } catch (NumberFormatException e) {
                e.printStackTrace();
            }
            response.sendRedirect(URLConfig.ERROR);
        }
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>
}
