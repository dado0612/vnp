package servlet;

import config.RoleConfig;
import config.TrackStatus;
import config.URLConfig;
import controller.BillCacheDAO;
import controller.PackageDAO;
import controller.TrackDAO;
import java.io.File;
import java.io.IOException;
import java.util.Date;
import javafx.util.Pair;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import model.BillCache;
import model.Employee;
import model.Package;
import model.Track;
import util.MailUtil;
import util.SessionUtil;

@WebServlet(name = "NewOrderServlet", urlPatterns = {"/NewOrderServlet"})
public class NewOrderServlet extends HttpServlet {

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        Pair<Boolean, Employee> pair = SessionUtil.checkSession(request.getSession(), RoleConfig.MANAGER);
        if (pair.getKey()) {
            response.sendRedirect(URLConfig.LOGIN);
        } else {
            try {
                int employeeID = pair.getValue().getEmployeeID();
                long packageID = new Date().getTime();
                String postID = request.getParameter("postID");
                String receiverLocation = request.getParameter("receiverLocation");
                String senderName = request.getParameter("senderName");
                String senderPhone = request.getParameter("senderPhone");
                String receiverName = request.getParameter("receiverName");
                String receiverPhone = request.getParameter("receiverPhone");
                String receiverMail = request.getParameter("receiverMail");
                String receiverAddress = request.getParameter("receiverAddress");
                long price = Long.parseLong(request.getParameter("price"));
                String description = request.getParameter("description");
                boolean isPaid = request.getParameter("isPaid").equals("true");
                int fee = Integer.parseInt(request.getParameter("fee"));

                String weight, height, width, depth;
                weight = request.getParameter("weight");
                String descriptionPackage = "";
                if (weight == null) {
                    weight = "x";
                    height = request.getParameter("height");
                    width = request.getParameter("width");
                    depth = request.getParameter("depth");
                    descriptionPackage = "Height = " + height + " | Width = " + width + " | Depth = " + depth;
                } else {
                    height = "x";
                    width = "x";
                    depth = "x";
                    descriptionPackage = "Weight = " + weight;
                }

                boolean check = true;
                check &= PackageDAO.addPackage(new Package(packageID, postID, receiverLocation, senderName,
                        senderPhone, receiverName, receiverPhone, receiverMail, receiverAddress, price, descriptionPackage));
                check &= TrackDAO.addTrack(new Track(employeeID, packageID, postID, new Date(), TrackStatus.INIT, description));
                check &= BillCacheDAO.addBillCache(new BillCache(employeeID, packageID, fee, isPaid));
                if (check) {
                    response.sendRedirect("dashboard/bill.jsp?packageID=" + packageID + "&weight=" + weight
                            + "&height=" + height + "&width=" + width + "&depth=" + depth + "&description=" + description);
                    String path = getServletContext().getRealPath("/") + packageID + ".png";
                    MailUtil.sendEmailQRCode(packageID, receiverMail, path);
                    new File(path).delete();
                    return;
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
            response.sendRedirect(URLConfig.ERROR);
        }
    }
}
