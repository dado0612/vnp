package servlet;

import config.RoleConfig;
import config.URLConfig;
import controller.EmployeeDAO;
import java.io.IOException;
import java.text.ParseException;
import java.util.Date;
import javafx.util.Pair;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import model.Employee;
import util.DateUtil;
import util.SessionUtil;

@WebServlet(name = "UpdateEmployeeServlet", urlPatterns = {"/UpdateEmployeeServlet"})
public class UpdateEmployeeServlet extends HttpServlet {

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        Pair<Boolean, Employee> pair = SessionUtil.checkSession(request.getSession(), RoleConfig.ADMIN);
        if (pair.getKey()) {
            response.sendRedirect(URLConfig.LOGIN);
        } else {
            try {
                int employeeID = Integer.parseInt(request.getParameter("employeeID"));
                String postID = request.getParameter("postID");
                int roleID = Integer.parseInt(request.getParameter("roleID"));
                String username = request.getParameter("username");
                String name = request.getParameter("name");
                boolean gender = request.getParameter("gender").equals("1");
                Date dob = DateUtil.parseDate(request.getParameter("dob"));
                String phone = request.getParameter("phone");
                Date hireDate = DateUtil.parseDate(request.getParameter("hireDate"));
                String identityNum = request.getParameter("identityNum");

                Employee emp = new Employee(employeeID, postID, roleID, username, name, gender, dob, phone, hireDate, identityNum);
                if (EmployeeDAO.updateEmployee(emp)) {
                    response.sendRedirect(URLConfig.EMPLOYEE);
                    return;
                }
            } catch (NumberFormatException | ParseException e) {
                e.printStackTrace();
            }
            response.sendRedirect(URLConfig.ERROR);
        }
    }
}
