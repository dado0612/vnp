package servlet;

import config.FeeConfig;
import java.io.IOException;
import java.io.PrintWriter;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

@WebServlet(name = "FeeServlet", urlPatterns = {"/FeeServlet"})
public class FeeServlet extends HttpServlet {

    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        try (PrintWriter out = response.getWriter()) {
            String senderLocationID = request.getParameter("senderLocationID").substring(0, 2);
            String receiverLocationID = request.getParameter("receiverLocationID").substring(0, 2);

            int fee = 0;
            if (request.getParameter("weight") != null) {
                int weight = Integer.parseInt(request.getParameter("weight"));
                fee = weight * FeeConfig.FEE_PER_GRAM;
            } else {
                int height = Integer.parseInt(request.getParameter("height"));
                int depth = Integer.parseInt(request.getParameter("depth"));
                int width = Integer.parseInt(request.getParameter("width"));
                fee = (height + width + depth) * FeeConfig.FEE_PER_DIM;
            }

            fee = Math.max(fee, 4000);
            if (!senderLocationID.equals(receiverLocationID)) {
                fee = Math.round(fee * FeeConfig.SCALE);
            }
            out.print(fee);
        }
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>
}
