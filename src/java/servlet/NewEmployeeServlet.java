package servlet;

import config.AttributeConfig;
import config.RoleConfig;
import config.URLConfig;
import controller.AccountDAO;
import controller.EmployeeDAO;
import java.io.IOException;
import java.text.ParseException;
import java.util.Date;
import javafx.util.Pair;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import model.Account;
import model.Employee;
import util.DateUtil;
import util.SessionUtil;

@WebServlet(name = "NewEmployeeServlet", urlPatterns = {"/NewEmployeeServlet"})
public class NewEmployeeServlet extends HttpServlet {

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        Pair<Boolean, Employee> pair = SessionUtil.checkSession(request.getSession(), RoleConfig.ADMIN);
        if (pair.getKey()) {
            response.sendRedirect(URLConfig.LOGIN);
        } else {
            try {
                String postID = request.getParameter("postID");
                int roleID = Integer.parseInt(request.getParameter("roleID"));
                String username = request.getParameter("username");
                String name = request.getParameter("name");
                boolean gender = request.getParameter("gender").equals("1");
                Date dob = DateUtil.parseDate(request.getParameter("dob"));
                String phone = request.getParameter("phone");
                Date hireDate = new Date();
                String identityNum = request.getParameter("identityNum");

                Account acc = new Account(username, AttributeConfig.DEFAULT_PASSWORD);
                Employee emp = new Employee(postID, roleID, username, name, gender, dob, phone, hireDate, identityNum);
                if (AccountDAO.addAccount(acc) && EmployeeDAO.addEmployee(emp)) {
                    response.sendRedirect(URLConfig.EMPLOYEE);
                    return;
                }
            } catch (NumberFormatException | ParseException e) {
                e.printStackTrace();
            }
            response.sendRedirect(URLConfig.ERROR);
        }
    }
}
