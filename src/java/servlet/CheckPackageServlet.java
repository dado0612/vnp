package servlet;

import config.RoleConfig;
import config.TrackStatus;
import controller.PackageDAO;
import controller.TrackDAO;
import java.io.IOException;
import java.io.PrintWriter;
import javafx.util.Pair;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import model.Employee;
import model.Package;
import model.Post;
import model.Track;
import util.JsonUtil;
import util.RouteUtil;
import util.SessionUtil;

@WebServlet(name = "CheckPackageServlet", urlPatterns = {"/CheckPackageServlet"})
public class CheckPackageServlet extends HttpServlet {

    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        try (PrintWriter out = response.getWriter()) {
            Pair<Boolean, Employee> pair = SessionUtil.checkSession(request.getSession(), RoleConfig.MANAGER);
            if (pair.getKey()) {
                out.print("error");
            } else {
                long packageID = Long.parseLong(request.getParameter("packageID"));
                Package pac = PackageDAO.selectByPackageID(packageID);
                Track track = TrackDAO.getLastTrackByPackageID(packageID);
                String json = "";
                switch (track.getStatus()) {
                    case TrackStatus.INIT:
                    case TrackStatus.IN: {
                        if (track.getPostID().equals(pac.getReceiverLocation())) {
                            json = JsonUtil.encodeJsonCheckInOut(pac, TrackStatus.SHIP, null);
                        } else {
                            Post nextPost = RouteUtil.getNextPost(track.getPostID(), pac.getReceiverLocation());
                            json = JsonUtil.encodeJsonCheckInOut(pac, TrackStatus.OUT, nextPost);
                        }
                    }
                    break;
                    case TrackStatus.OUT: {
                        json = JsonUtil.encodeJsonCheckInOut(pac, TrackStatus.IN, null);
                    }
                    break;
                    case TrackStatus.SHIP:
                    case TrackStatus.ERR: {
                        json = JsonUtil.encodeJsonCheckInOut(pac, TrackStatus.CONF, null);
                    }
                }
                out.print(json);
            }
        }
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>
}
