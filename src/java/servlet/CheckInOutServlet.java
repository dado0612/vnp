package servlet;

import config.RoleConfig;
import config.TrackStatus;
import config.URLConfig;
import controller.BillCacheDAO;
import controller.BillDAO;
import controller.PackageDAO;
import controller.TrackDAO;
import java.io.IOException;
import java.util.Date;
import javafx.util.Pair;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import model.Bill;
import model.BillCache;
import model.Employee;
import model.Track;
import util.SessionUtil;

@WebServlet(name = "CheckInOutServlet", urlPatterns = {"/CheckInOutServlet"})
public class CheckInOutServlet extends HttpServlet {

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        Pair<Boolean, Employee> pair = SessionUtil.checkSession(request.getSession(), RoleConfig.MANAGER);
        if (pair.getKey()) {
            response.sendRedirect(URLConfig.LOGIN);
        } else {
            try {
                int employeeID = pair.getValue().getEmployeeID();
                long packageID = Long.parseLong(request.getParameter("packageID"));
                String postID = request.getParameter("postID");
                String description = request.getParameter("description");
                int trackStatus = 0;
                String status = request.getParameter("status");
                switch (status) {
                    case "Confirm": {
                        String deliveryStatus = request.getParameter("deliveryStatus");
                        if (deliveryStatus.equals("success")) {
                            trackStatus = TrackStatus.OK;
                        } else {
                            trackStatus = TrackStatus.ERR;
                        }
                    }
                    break;
                    case "Check In": {
                        trackStatus = TrackStatus.IN;
                    }
                    break;
                    case "Check Out": {
                        trackStatus = TrackStatus.OUT;
                    }
                    break;
                    case "Shipper": {
                        trackStatus = TrackStatus.SHIP;
                        description = "Shipper: " + request.getParameter("shipper") + "; " + description;
                    }
                }

                boolean check = TrackDAO.addTrack(new Track(employeeID, packageID, postID, new Date(), trackStatus, description));
                if (trackStatus == TrackStatus.OK) {
                    BillCache cache = BillCacheDAO.selectByPackageID(packageID);
                    int fee = cache.getFee();
                    boolean statusBill = cache.isStatus();
                    String postBill = "";
                    if (statusBill) {
                        postBill = PackageDAO.selectByPackageID(packageID).getPostID();
                    } else {
                        postBill = postID;
                    }

                    check &= BillDAO.addBill(new Bill(postBill, packageID, new Date(), fee));
                    check &= BillCacheDAO.deleteBillCache(packageID);
                }
                if (check) {
                    response.sendRedirect("dashboard/checkInOut.jsp");
                    return;
                }
            } catch (IOException | NumberFormatException e) {
                e.printStackTrace();
            }
            response.sendRedirect(URLConfig.ERROR);
        }
    }
}
