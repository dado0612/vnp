<%@page import="controller.PostDAO"%>
<%@page import="config.TrackStatus"%>
<%@page import="util.DateUtil"%>
<%@page import="controller.EmployeeDAO"%>
<%@page import="controller.TrackDAO"%>
<%@page import="java.util.List"%>
<%@page import="model.Track"%>
<%@page import="javafx.util.Pair"%>
<%@page import="javafx.util.Pair"%>
<%@page import="util.SessionUtil"%>
<%@page import="config.RoleConfig"%>
<%@page import="model.Employee"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>

<!DOCTYPE html>
<html lang="en">
    <%
        Pair<Boolean, Employee> checkSession = SessionUtil.checkSession(session, RoleConfig.ADMIN);
        if (checkSession.getKey()) {
            response.sendRedirect("../index/login.html");
            return;
        }
        List<Track> listTrack = TrackDAO.selectStorageAll();
        listTrack = listTrack.subList(0, 1000);
    %>
    <head>
        <meta charset="utf-8" />
        <link rel="apple-touch-icon" sizes="76x76" href="../../img/apple-icon.png">
        <link rel="icon" type="image/png" href="../../img/favicon.png">
        <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />
        <title>
            Fly Post | Storage
        </title>
        <meta content='width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0, shrink-to-fit=no' name='viewport' />
        <!--     Fonts and icons     -->
        <link rel="stylesheet" type="text/css" href="https://fonts.googleapis.com/css?family=Roboto:300,400,500,700|Roboto+Slab:400,700|Material+Icons" />
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/latest/css/font-awesome.min.css">
        <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.4.1/css/all.css" integrity="sha384-5sAR7xN1Nv6T6+dT2mhtzEpVJvfS3NScPQTrOxhwjIuvcA67KV2R5Jz6kr4abQsz"
              crossorigin="anonymous">
        <!--datatable style-->
        <link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/v/bs4/jszip-2.5.0/dt-1.10.18/b-1.5.4/b-html5-1.5.4/datatables.min.css"/>  
        <!-- CSS Files -->
        <link href="../../css/dashboard/material-dashboard.css?v=2.1.0" rel="stylesheet" />
        <link rel="stylesheet" href="../../css/dashboard/custom.css">
    </head>

    <body class="">
        <div class="wrapper ">
            <div class="sidebar" data-color="azure" data-background-color="white" data-image="../../img/sidebar-1.jpg">
                <div class="logo">
                    <a href="./dashboard.jsp" class="simple-text logo-normal">
                        FLY POST
                    </a>
                </div>
                <div class="sidebar-wrapper">
                    <ul class="nav">
                        <li class="nav-item">
                            <a class="nav-link" href="./dashboard.jsp">
                                <i class="material-icons">dashboard</i>
                                <p>Dashboard</p>
                            </a>
                        </li>
                        <li class="nav-item ">
                            <a class="nav-link" href="./employee.jsp">
                                <i class="material-icons">person_add</i>
                                <p>Employee</p>
                            </a>
                        </li>
                        <li class="nav-item active">
                            <a class="nav-link" href="./storage.jsp">
                                <i class="material-icons">list</i>
                                <p>Storage</p>
                            </a>
                        </li>
                        <li class="nav-item ">
                            <a class="nav-link" href="./history.jsp">
                                <i class="material-icons">history</i>
                                <p>History</p>
                            </a>
                        </li>
                        <li class="nav-item ">
                            <a class="nav-link" href="./userProfile.jsp">
                                <i class="material-icons">person</i>
                                <p>User Profile</p>
                            </a>
                        </li>
                        <li class="nav-item ">
                            <a class="nav-link" href="./logout.jsp">
                                <i class="material-icons">power_settings_new</i>
                                <p>Logout</p>
                            </a>
                        </li>
                    </ul>
                </div>
            </div>
            <div class="main-panel">
                <!-- Navbar -->
                <nav class="navbar navbar-expand-lg navbar-transparent navbar-absolute fixed-top ">
                    <div class="container-fluid">
                        <div class="navbar-wrapper">
                            <a class="navbar-brand" href="./dashboard.jsp">Dashboard</a>
                        </div>
                        <button class="navbar-toggler" type="button" data-toggle="collapse" aria-controls="navigation-index"
                                aria-expanded="false" aria-label="Toggle navigation">
                            <span class="sr-only">Toggle navigation</span>
                            <span class="navbar-toggler-icon icon-bar"></span>
                            <span class="navbar-toggler-icon icon-bar"></span>
                            <span class="navbar-toggler-icon icon-bar"></span>
                        </button>
                        <div class="collapse navbar-collapse justify-content-end">
                            <ul class="navbar-nav">
                                <li class="nav-item">
                                    <a class="nav-link" href="./dashboard.jsp">
                                        <i class="material-icons">dashboard</i>
                                        <p class="d-lg-none d-md-block">
                                            Stats
                                        </p>
                                    </a>
                                </li>
                                <li class="nav-item dropdown">
                                    <a class="nav-link" href="" id="navbarDropdownMenuLink" data-toggle="dropdown" aria-haspopup="true"
                                       aria-expanded="false">
                                        <i class="material-icons">person</i>
                                        <p class="d-lg-none d-md-block">
                                            Account
                                        </p>
                                    </a>
                                    <div class="dropdown-menu dropdown-menu-right dropdown-with-icons" aria-labelledby="navbarDropdownMenuLink">
                                        <a class="dropdown-item" href="./userProfile.jsp">
                                            <span><i class="fas fa-sliders-h"></i></span>
                                            Setting
                                        </a>
                                        <a class="dropdown-item" href="./logout.jsp">
                                            <span><i class="fas fa-power-off"></i></span>
                                            Logout
                                        </a>
                                    </div>
                                </li>
                            </ul>
                        </div>
                    </div>
                </nav>
                <!-- End Navbar -->
                <div class="content">
                    <div class="container-fluid">
                        <!--content go here-->
                        <div class="d-flex justify-content-center"><h3>All Packages Storage</h3></div>
                        <div class="card">
                            <div class="card-header card-header-success">
                                <table id="employeeTable" class="table table-borderless table-hover" style="width:100%">
                                    <thead>
                                        <tr>
                                            <th>No</th>
                                            <th>Package ID</th>
                                            <th>Post ID</th>
                                            <th>Post Name</th>
                                            <th>Employee ID</th>
                                            <th>Employee Name</th>
                                            <th>Time</th>
                                            <th>Status</th>
                                            <th>Description</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <%                                    int num = 0;
                                            for (Track i : listTrack) {
                                                out.print("<tr>");
                                                out.print("<td style = 'text-align:center'>" + (++num) + "</td>");
                                                out.print("<td><a href='../dashboard/detailPackage.jsp?packageID=" + i.getPackageID() + "' target='_blank'>"
                                                        + i.getPackageID() + "</a></td>");
                                                out.print("<td>" + i.getPostID() + "</td>");
                                                out.print("<td>"
                                                        + PostDAO.selectByPostID(i.getPostID()).getPostName() + "</td>");
                                                out.print("<td>" + i.getEmployeeID() + "</td>");
                                                out.print("<td>"
                                                        + EmployeeDAO.selectByEmployeeID(i.getEmployeeID()).getName() + "</td>");
                                                out.print("<td>" + DateUtil.formatDateTime(i.getTime()) + "</td>");
                                                String status = "";
                                                switch (i.getStatus()) {
                                                    case TrackStatus.INIT:
                                                        status = "Initial";
                                                        break;
                                                    case TrackStatus.IN:
                                                        status = "Check In";
                                                        break;
                                                    case TrackStatus.ERR:
                                                        status = "Ship Fail";
                                                        break;
                                                }
                                                out.print("<td>" + status + "</td>");
                                                out.print("<td>" + i.getDescription() + "</td>");
                                                out.print("</tr>");
                                            }
                                        %>
                                    </tbody>
                                    <tfoot>
                                        <tr>
                                            <th>No</th>
                                            <th>Package ID</th>
                                            <th>Post ID</th>
                                            <th>Post Name</th>
                                            <th>Employee ID</th>
                                            <th>Employee Name</th>
                                            <th>Time</th>
                                            <th>Status</th>
                                            <th>Description</th>
                                        </tr>
                                    </tfoot>
                                </table>
                            </div>
                            <div class="card-body">
                            </div>
                        </div>
                    </div>
                </div>
                <footer class="footer">
                    <div class="container-fluid">
                        <nav class="float-left">
                            <ul>
                                <li>
                                    <a href="../">
                                        FLY POST
                                    </a>
                                </li>
                                <li>
                                    <a href="../index/about.html">
                                        About Us
                                    </a>
                                </li>
                                <li>
                                    <a href="../index/about.html">
                                        Blog
                                    </a>
                                </li>
                            </ul>
                        </nav>
                        <div class="copyright float-right">
                            &copy;
                            <script>
                                document.write(new Date().getFullYear())
                            </script>, made with <i class="material-icons">favorite</i> by
                            <a href="https://www.creative-tim.com" target="_blank">Creative Tim</a> for a better web.
                        </div>
                    </div>
                </footer>
            </div>
        </div>
        <!--   Core JS Files   -->
        <script src="../../js/dashboard/core/jquery.min.js" type="text/javascript"></script>
        <script src="../../js/dashboard/core/popper.min.js" type="text/javascript"></script>
        <script src="../../js/dashboard/core/bootstrap-material-design.min.js" type="text/javascript"></script>
        <script src="../../js/dashboard/plugins/perfect-scrollbar.jquery.min.js"></script>
        <!-- Control Center for Material Dashboard: parallax effects, scripts for the example pages etc -->
        <script src="../../js/dashboard/material-dashboard.min.js?v=2.1.0" type="text/javascript"></script>
        <!--datatable-->
        <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.36/pdfmake.min.js"></script>
        <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.36/vfs_fonts.js"></script>
        <script type="text/javascript" src="https://cdn.datatables.net/v/bs4/jszip-2.5.0/dt-1.10.18/b-1.5.4/b-html5-1.5.4/datatables.min.js"></script>

        <script>
                                $(document).ready(function () {
                                    md.initDashboardPageCharts();
                                    var table = $('#employeeTable').DataTable({
                                        lengthChange: false,
                                        buttons: ['copy', 'excel', 'pdf']
                                    });

                                    table.buttons().container().appendTo('#employeeTable_wrapper .col-md-6:eq(0)');
                                });
        </script>
    </body>
</html>