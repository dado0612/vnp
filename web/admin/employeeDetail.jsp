<%@page import="util.DateUtil"%>
<%@page import="controller.EmployeeDAO"%>
<%@page import="controller.RoleDAO"%>
<%@page import="model.Role"%>
<%@page import="model.Post"%>
<%@page import="controller.PostDAO"%>
<%@page import="java.util.List"%>
<%@page import="config.RoleConfig"%>
<%@page import="javafx.util.Pair"%>
<%@page import="util.SessionUtil"%>
<%@page import="model.Employee"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>

<!DOCTYPE html>
<html>
    <%
        Pair<Boolean, Employee> checkSession = SessionUtil.checkSession(session, RoleConfig.ADMIN);
        if (checkSession.getKey()) {
            response.sendRedirect("../index/login.html");
            return;
        }
        int employeeID;
        try {
            employeeID = Integer.parseInt(request.getParameter("employeeID"));
        } catch (NumberFormatException e) {
            response.sendRedirect("../index/error.html");
            return;
        }
        Employee emp = EmployeeDAO.selectByEmployeeID(employeeID);
        if (emp == null || emp.getRoleID() <= RoleConfig.ADMIN) {
            response.sendRedirect("../index/error.html");
            return;
        }

        List<Post> listPost = PostDAO.selectAll();
        List<Role> listRole = RoleDAO.selectAllRole();
    %>
    <head>
        <meta charset="utf-8" />
        <link rel="apple-touch-icon" sizes="76x76" href="../../img/apple-icon.png">
        <link rel="icon" type="image/png" href="../../img/favicon.png">
        <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />
        <title>
            Fly Post | Employee Information
        </title>
        <meta content='width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0, shrink-to-fit=no' name='viewport' />
        <!--     Fonts and icons     -->
        <link rel="stylesheet" type="text/css" href="https://fonts.googleapis.com/css?family=Roboto:300,400,500,700|Roboto+Slab:400,700|Material+Icons" />
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/latest/css/font-awesome.min.css">
        <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.4.1/css/all.css" integrity="sha384-5sAR7xN1Nv6T6+dT2mhtzEpVJvfS3NScPQTrOxhwjIuvcA67KV2R5Jz6kr4abQsz"
              crossorigin="anonymous">
        <!-- CSS Files -->
        <link href="../../css/dashboard/material-dashboard.css?v=2.1.0" rel="stylesheet" />
        <link rel="stylesheet" href="../../css/dashboard/custom.css">
    </head>

    <body class="">
        <div class="wrapper ">
            <div class="sidebar" data-color="azure" data-background-color="white" data-image="../../img/sidebar-1.jpg">
                <div class="logo">
                    <a href="./dashboard.jsp" class="simple-text logo-normal">
                        FLY POST
                    </a>
                </div>
                <div class="sidebar-wrapper">
                    <ul class="nav">
                        <li class="nav-item">
                            <a class="nav-link" href="./dashboard.jsp">
                                <i class="material-icons">dashboard</i>
                                <p>Dashboard</p>
                            </a>
                        </li>
                        <li class="nav-item active">
                            <a class="nav-link" href="./employee.jsp">
                                <i class="material-icons">person_add</i>
                                <p>Employee</p>
                            </a>
                        </li>
                        <li class="nav-item ">
                            <a class="nav-link" href="./storage.jsp">
                                <i class="material-icons">list</i>
                                <p>Storage</p>
                            </a>
                        </li>
                        <li class="nav-item ">
                            <a class="nav-link" href="./history.jsp">
                                <i class="material-icons">history</i>
                                <p>History</p>
                            </a>
                        </li>
                        <li class="nav-item ">
                            <a class="nav-link" href="./userProfile.jsp">
                                <i class="material-icons">person</i>
                                <p>User Profile</p>
                            </a>
                        </li>
                        <li class="nav-item ">
                            <a class="nav-link" href="./logout.jsp">
                                <i class="material-icons">power_settings_new</i>
                                <p>Logout</p>
                            </a>
                        </li>
                    </ul>
                </div>
            </div>
            <div class="main-panel">
                <!-- Navbar -->
                <nav class="navbar navbar-expand-lg navbar-transparent navbar-absolute fixed-top ">
                    <div class="container-fluid">
                        <div class="navbar-wrapper">
                            <a class="navbar-brand" href="./dashboard.jsp">Dashboard</a>
                        </div>
                        <button class="navbar-toggler" type="button" data-toggle="collapse" aria-controls="navigation-index"
                                aria-expanded="false" aria-label="Toggle navigation">
                            <span class="sr-only">Toggle navigation</span>
                            <span class="navbar-toggler-icon icon-bar"></span>
                            <span class="navbar-toggler-icon icon-bar"></span>
                            <span class="navbar-toggler-icon icon-bar"></span>
                        </button>
                        <div class="collapse navbar-collapse justify-content-end">
                            <ul class="navbar-nav">
                                <li class="nav-item">
                                    <a class="nav-link" href="./dashboard.jsp">
                                        <i class="material-icons">dashboard</i>
                                        <p class="d-lg-none d-md-block">
                                            Stats
                                        </p>
                                    </a>
                                </li>
                                <li class="nav-item dropdown">
                                    <a class="nav-link" href="" id="navbarDropdownMenuLink" data-toggle="dropdown" aria-haspopup="true"
                                       aria-expanded="false">
                                        <i class="material-icons">person</i>
                                        <p class="d-lg-none d-md-block">
                                            Account
                                        </p>
                                    </a>
                                    <div class="dropdown-menu dropdown-menu-right dropdown-with-icons" aria-labelledby="navbarDropdownMenuLink">
                                        <a class="dropdown-item" href="./userProfile.jsp">
                                            <span><i class="fas fa-sliders-h"></i></span>
                                            Setting
                                        </a>
                                        <a class="dropdown-item" href="./logout.jsp">
                                            <span><i class="fas fa-power-off"></i></span>
                                            Logout
                                        </a>
                                    </div>
                                </li>
                            </ul>
                        </div>
                    </div>
                </nav>
                <!-- End Navbar -->
                <div class="content">
                    <div class="container-fluid">
                        <!-- your content here -->
                        <div class="container">
                            <div>
                                <div class="card">
                                    <div class="card-header card-header-success">
                                        <h4 class="card-title">Employee Information</h4>
                                        <p class="card-category">Detail information about employee</p>
                                    </div>
                                    <div class="card-body">
                                        <form action="../UpdateEmployeeServlet" method="POST">
                                            <!--change action letter-->
                                            <div class="row">
                                                <div class="col-md-3">
                                                    <div class="form-group">
                                                        <label>Employee ID</label>
                                                        <input type="text" id="employeeID" name="employeeID" class="form-control" value="<%= employeeID%>" readonly>
                                                    </div>
                                                </div>
                                                <div class="col-md-3">
                                                    <div class="form-group">
                                                        <label>UserName</label>
                                                        <input type="text" id="username" name="username" class="form-control" value="<%= emp.getUsername()%>" readonly>
                                                    </div>
                                                </div>
                                                <div class="col-md-3">
                                                    <div class="form-group">
                                                        <label class="label-control">Work From</label>
                                                        <input type="text" id="hireDate" name="hireDate" class="form-control" value="<%= DateUtil.formatDate(emp.getHireDate())%>" readonly>
                                                    </div>
                                                </div>
                                                <div class="col-md-3">
                                                    <div class="form-group">
                                                        <label>Identity Number</label>
                                                        <input type="text" id="identityNum" name="identityNum" class="form-control" value="<%= emp.getIdentityNum()%>"  required>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="row">
                                                <div class="col-md-6 col-lg-3">
                                                    <div class="form-group">
                                                        <label>Employee Name</label>
                                                        <input type="text" id="name" name="name" class="form-control" value="<%= emp.getName()%>" required>
                                                    </div>
                                                </div>
                                                <div class="col-md-6 col-lg-3">
                                                    <div class="form-group">
                                                        <label class="label-control">Date Of Birth</label>
                                                        <input type="text" id="dob" name="dob" class="form-control" value="<%= DateUtil.formatDateHTML(emp.getDob())%>" required>
                                                    </div>
                                                </div>
                                                <div class="col-md-6 col-lg-3">
                                                    <div class="form-group">
                                                        <label>Employee Phone</label>
                                                        <input type="text" id="phone" name="phone" class="form-control" value="<%= emp.getPhone()%>" required>
                                                    </div>
                                                </div>
                                                <div class="col-sm-4 col-md-4 col-lg-3">
                                                    <label class="label-control">Gender</label>
                                                    <div class="row">
                                                        <div class="col form-check">
                                                            <label class="form-check-label">
                                                                <input class="form-check-input" type="radio" name="gender" value="1" <%= emp.isGender() ? "checked" : ""%>>                 
                                                                Male
                                                                <span class="circle">
                                                                    <span class="check"></span>
                                                                </span>
                                                            </label>
                                                        </div>
                                                        <div class="col form-check">
                                                            <label class="form-check-label">
                                                                <input class="form-check-input" type="radio" name="gender" value="0" <%= emp.isGender() ? "" : "checked"%>>
                                                                Female
                                                                <span class="circle">
                                                                    <span class="check"></span>
                                                                </span>
                                                            </label>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="row">
                                                <div class="col-md-4">
                                                    <div class="form-group">
                                                        <label for="postID">Post</label>
                                                        <select name="postID" id="postID" class="form-control" required>
                                                            <%
                                                                for (Post i : listPost) {
                                                                    String selected = "";
                                                                    if (i.getPostID().equals(emp.getPostID())) {
                                                                        selected = "selected";
                                                                    }
                                                                    out.print("<option value='" + i.getPostID() + "'" + selected + ">" + i.getPostID() + " - " + i.getPostName() + "</option>");
                                                                }
                                                            %>
                                                        </select>
                                                    </div>
                                                </div>
                                                <div class="col-md-4">
                                                    <div class="form-group">
                                                        <label for="roleID">Role</label>
                                                        <select name="roleID" id="roleID" class="form-control" required>
                                                            <%
                                                                for (Role i : listRole) {
                                                                    String selected = "";
                                                                    if (i.getRoleID() == emp.getRoleID()) {
                                                                        selected = "selected";
                                                                    }
                                                                    out.print("<option value='" + i.getRoleID() + "'" + selected + ">" + i.getRoleName() + "</option>");
                                                                }
                                                            %>
                                                        </select>
                                                    </div>
                                                </div>
                                            </div>
                                            <button type="button" class="btn btn-danger pull-right mr-sm-3" onclick="location.href = '../FireEmployeeServlet?employeeID=<%= employeeID%>'">Fire</button>
                                            <button type="submit" class="btn btn-primary pull-right mr-sm-3">Update</button>
                                            <div class="clearfix"></div>
                                        </form>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <footer class="footer">
                    <div class="container-fluid">
                        <nav class="float-left">
                            <ul>
                                <li>
                                    <a href="https://www.creative-tim.com">
                                        Creative Tim
                                    </a>
                                </li>
                            </ul>
                        </nav>
                        <div class="copyright float-right">
                            &copy;
                            <script>
                                document.write(new Date().getFullYear())
                            </script>, made with <i class="material-icons">favorite</i> by
                            <a href="https://www.creative-tim.com" target="_blank">Creative Tim</a> for a better web.
                        </div>
                        <!-- your footer here -->
                    </div>
                </footer>
            </div>
        </div>
        <!--   Core JS Files   -->
        <script src="../../js/dashboard/core/jquery.min.js" type="text/javascript"></script>
        <script src="../../js/dashboard/core/popper.min.js" type="text/javascript"></script>
        <script src="../../js/dashboard/core/bootstrap-material-design.min.js" type="text/javascript"></script>
        <script src="../../js/dashboard/plugins/perfect-scrollbar.jquery.min.js"></script>
        <!-- Control Center for Material Dashboard: parallax effects, scripts for the example pages etc -->
        <script src="../../js/dashboard/material-dashboard.min.js?v=2.1.0" type="text/javascript"></script>
        <link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">
        <script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>
        <script>
                                $(function () {
                                    $("#dob").datepicker({
                                        changeMonth: true,
                                        changeYear: true,
                                        yearRange: "1930:" + (new Date().getFullYear()),
                                    });
                                    $("#dob").datepicker("option", "dateFormat", "dd/mm/yy");
                                });
        </script>
    </body>
</html>
