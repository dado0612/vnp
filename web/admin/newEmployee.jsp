<%@page import="model.Role"%>
<%@page import="controller.RoleDAO"%>
<%@page import="controller.PostDAO"%>
<%@page import="model.Post"%>
<%@page import="java.util.List"%>
<%@page import="config.RoleConfig"%>
<%@page import="javafx.util.Pair"%>
<%@page import="util.SessionUtil"%>
<%@page import="util.SessionUtil"%>
<%@page import="model.Employee"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>

<!DOCTYPE html>
<html>
    <%
        Pair<Boolean, Employee> checkSession = SessionUtil.checkSession(session, RoleConfig.ADMIN);
        if (checkSession.getKey()) {
            response.sendRedirect("../index/login.html");
            return;
        }
        List<Post> listPost = PostDAO.selectAll();
        List<Role> listRole = RoleDAO.selectAllRole();
    %>
    <head>
        <meta charset="utf-8" />
        <link rel="apple-touch-icon" sizes="76x76" href="../../img/apple-icon.png">
        <link rel="icon" type="image/png" href="../../img/favicon.png">
        <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />
        <title>
            Fly Post | New Employee
        </title>
        <meta content='width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0, shrink-to-fit=no' name='viewport' />
        <!--     Fonts and icons     -->
        <link rel="stylesheet" type="text/css" href="https://fonts.googleapis.com/css?family=Roboto:300,400,500,700|Roboto+Slab:400,700|Material+Icons" />
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/latest/css/font-awesome.min.css">
        <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.4.1/css/all.css" integrity="sha384-5sAR7xN1Nv6T6+dT2mhtzEpVJvfS3NScPQTrOxhwjIuvcA67KV2R5Jz6kr4abQsz"
              crossorigin="anonymous">
        <!-- CSS Files -->
        <link href="../../css/dashboard/material-dashboard.css?v=2.1.0" rel="stylesheet" />
        <link rel="stylesheet" href="../../css/dashboard/custom.css">
    </head>

    <body class="">
        <div class="wrapper ">
            <div class="sidebar" data-color="azure" data-background-color="white" data-image="../../img/sidebar-1.jpg">
                <div class="logo">
                    <a href="./dashboard.jsp" class="simple-text logo-normal">
                        FLY POST
                    </a>
                </div>
                <div class="sidebar-wrapper">
                    <ul class="nav">
                        <li class="nav-item">
                            <a class="nav-link" href="./dashboard.jsp">
                                <i class="material-icons">dashboard</i>
                                <p>Dashboard</p>
                            </a>
                        </li>
                        <li class="nav-item active">
                            <a class="nav-link" href="./employee.jsp">
                                <i class="material-icons">person_add</i>
                                <p>Employee</p>
                            </a>
                        </li>
                        <li class="nav-item ">
                            <a class="nav-link" href="./storage.jsp">
                                <i class="material-icons">list</i>
                                <p>Storage</p>
                            </a>
                        </li>
                        <li class="nav-item ">
                            <a class="nav-link" href="./history.jsp">
                                <i class="material-icons">history</i>
                                <p>History</p>
                            </a>
                        </li>
                        <li class="nav-item ">
                            <a class="nav-link" href="./userProfile.jsp">
                                <i class="material-icons">person</i>
                                <p>User Profile</p>
                            </a>
                        </li>
                        <li class="nav-item ">
                            <a class="nav-link" href="./logout.jsp">
                                <i class="material-icons">power_settings_new</i>
                                <p>Logout</p>
                            </a>
                        </li>
                    </ul>
                </div>
            </div>
            <div class="main-panel">
                <!-- Navbar -->
                <nav class="navbar navbar-expand-lg navbar-transparent navbar-absolute fixed-top ">
                    <div class="container-fluid">
                        <div class="navbar-wrapper">
                            <a class="navbar-brand" href="./dashboard.jsp">Dashboard</a>
                        </div>
                        <button class="navbar-toggler" type="button" data-toggle="collapse" aria-controls="navigation-index"
                                aria-expanded="false" aria-label="Toggle navigation">
                            <span class="sr-only">Toggle navigation</span>
                            <span class="navbar-toggler-icon icon-bar"></span>
                            <span class="navbar-toggler-icon icon-bar"></span>
                            <span class="navbar-toggler-icon icon-bar"></span>
                        </button>
                        <div class="collapse navbar-collapse justify-content-end">
                            <ul class="navbar-nav">
                                <li class="nav-item">
                                    <a class="nav-link" href="./dashboard.jsp">
                                        <i class="material-icons">dashboard</i>
                                        <p class="d-lg-none d-md-block">
                                            Stats
                                        </p>
                                    </a>
                                </li>
                                <li class="nav-item dropdown">
                                    <a class="nav-link" href="" id="navbarDropdownMenuLink" data-toggle="dropdown" aria-haspopup="true"
                                       aria-expanded="false">
                                        <i class="material-icons">person</i>
                                        <p class="d-lg-none d-md-block">
                                            Account
                                        </p>
                                    </a>
                                    <div class="dropdown-menu dropdown-menu-right dropdown-with-icons" aria-labelledby="navbarDropdownMenuLink">
                                        <a class="dropdown-item" href="./userProfile.jsp">
                                            <span><i class="fas fa-sliders-h"></i></span>
                                            Setting
                                        </a>
                                        <a class="dropdown-item" href="./logout.jsp">
                                            <span><i class="fas fa-power-off"></i></span>
                                            Logout
                                        </a>
                                    </div>
                                </li>
                            </ul>
                        </div>
                    </div>
                </nav>
                <!-- End Navbar -->
                <div class="content">
                    <div class="container-fluid">
                        <!-- your content here -->
                        <div class="container">
                            <div>
                                <div class="card">
                                    <div class="card-header card-header-success">
                                        <h4 class="card-title">New Employee</h4>
                                        <p class="card-category">Add new employee to the system</p>
                                    </div>
                                    <div class="card-body">
                                        <form action="../NewEmployeeServlet" method="POST">
                                            <!--change action letter-->
                                            <div class="row">
                                                <div class="col-md-3">
                                                    <div class="form-group">
                                                        <label class="bmd-label-floating">UserName</label>
                                                        <input type="text" id="username" name="username" class="form-control" onblur="checkUsername()" required>
                                                    </div>
                                                </div>
                                                <div class="col-md-5">
                                                    <div class="form-group">
                                                        <label class="bmd-label-floating">Employee Name</label>
                                                        <input type="text" id="name" name="name" class="form-control" required>
                                                    </div>
                                                </div>
                                                <div class="col-md-4">
                                                    <div class="form-group">
                                                        <label class="bmd-label-floating">Employee Phone</label>
                                                        <input type="text" id="phone" name="phone" class="form-control" required>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="row">
                                                <div class="col-md-4">
                                                    <div class="form-group">
                                                        <label class="bmd-label-floating">Identity Number</label>
                                                        <input type="text" id="identityNum" name="identityNum" class="form-control" required>
                                                    </div>
                                                </div>
                                                <div class="col-md-4">
                                                    <div class="form-group">
                                                        <label class="bmd-label-floating">Date Of Birth</label>
                                                        <input type="text" id="dob" name="dob" class="form-control" required>
                                                    </div>
                                                </div>
                                                <div class="col-sm-4 col-md-4 col-lg-3">
                                                    <label class="label-control">Gender</label>
                                                    <div class="row">
                                                        <div class="col form-check">
                                                            <label class="form-check-label">
                                                                <input class="form-check-input" type="radio" name="gender" value="1" checked>                 
                                                                Male
                                                                <span class="circle">
                                                                    <span class="check"></span>
                                                                </span>
                                                            </label>
                                                        </div>
                                                        <div class="col form-check">
                                                            <label class="form-check-label">
                                                                <input class="form-check-input" type="radio" name="gender" value="0">
                                                                Female
                                                                <span class="circle">
                                                                    <span class="check"></span>
                                                                </span>
                                                            </label>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="row">
                                                <div class="col-md-4">
                                                    <div class="form-group">
                                                        <label for="postID">Post</label>
                                                        <select name="postID" id="postID" class="form-control" required>
                                                            <option value="">Select One</option>
                                                            <%
                                                                for (Post i : listPost) {
                                                                    out.print("<option value='" + i.getPostID() + "'>" + i.getPostID() + " - " + i.getPostName() + "</option>");
                                                                }
                                                            %>
                                                        </select>
                                                    </div>
                                                </div>
                                                <div class="col-md-4">
                                                    <div class="form-group">
                                                        <label for="roleID">Role</label>
                                                        <select name="roleID" id="roleID" class="form-control" required>
                                                            <option value="">Select One</option>
                                                            <%
                                                                for (Role i : listRole) {
                                                                    out.print("<option value='" + i.getRoleID() + "'>" + i.getRoleName() + "</option>");
                                                                }
                                                            %>
                                                        </select>
                                                    </div>
                                                </div>
                                            </div>
                                            <button type="button" class="btn btn-primary pull-right mr-sm-3" onclick="location.href = 'employee.jsp'">Cancel</button>           
                                            <button type="submit" class="btn btn-primary pull-right mr-sm-3">Add</button>
                                            <div class="clearfix"></div>
                                        </form>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <footer class="footer">
                    <div class="container-fluid">
                        <nav class="float-left">
                            <ul>
                                <li>
                                    <a href="https://www.creative-tim.com">
                                        Creative Tim
                                    </a>
                                </li>
                            </ul>
                        </nav>
                        <div class="copyright float-right">
                            &copy;
                            <script>
                                document.write(new Date().getFullYear())
                            </script>, made with <i class="material-icons">favorite</i> by
                            <a href="https://www.creative-tim.com" target="_blank">Creative Tim</a> for a better web.
                        </div>
                        <!-- your footer here -->
                    </div>
                </footer>
            </div>
        </div>
        <!--   Core JS Files   -->
        <script src="../../js/dashboard/core/jquery.min.js" type="text/javascript"></script>
        <script src="../../js/dashboard/core/popper.min.js" type="text/javascript"></script>
        <script src="../../js/dashboard/core/bootstrap-material-design.min.js" type="text/javascript"></script>
        <script src="../../js/dashboard/plugins/perfect-scrollbar.jquery.min.js"></script>
        <script src="../../js/index/plugins/moment.min.js"></script>
        <!--  Notifications Plugin    -->
        <script src="../../js/dashboard/plugins/bootstrap-notify.js"></script>
        <!-- Control Center for Material Dashboard: parallax effects, scripts for the example pages etc -->
        <script src="../../js/dashboard/material-dashboard.min.js?v=2.1.0" type="text/javascript"></script>
        <!-- Control Center for Material Kit: parallax effects, scripts for the example pages etc -->
        <script src="../../js/index/material-kit.js?v=2.0.4" type="text/javascript"></script>
        <link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">
        <script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>
        <script>
                                $(function () {
                                    $("#dob").datepicker({
                                        changeMonth: true,
                                        changeYear: true,
                                        yearRange: "1930:" + (new Date().getFullYear()),
                                    });
                                    $("#dob").datepicker("option", "dateFormat", "dd/mm/yy");
                                });
        </script>

        <script>
            // INIT AJAX
            var request;
            if (window.XMLHttpRequest) {
                request = new XMLHttpRequest();
            } else if (window.ActiveXObject) {
                request = new ActiveXObject("Microsoft.XMLHTTP");
            }
            function checkUsername() {
                var user = document.getElementById('username').value.trim();
                document.getElementById('username').value = user;
                if (user != '') {
                    var url = "../CheckUserServlet?username=" + user;
                    try {
                        request.onreadystatechange = getInfo; // getInfor is below function
                        request.open("GET", url, true);
                        request.send();
                    } catch (e) {
                        alert("Unable to connect to server");
                    }
                }
            }

            function getInfo() {
                if (request.readyState == 4) {
                    var val = request.responseText; // request.responseText = out.print in servlet
                    if (val == 'error') {
                        showNotification('top', 'center', 'danger', 'Username is duplicate!');
                        document.getElementById('username').value = '';
                        document.getElementById('username').focus();
                    } else {
                        document.getElementById('username').value = val;
                    }
                }
            }
            function showNotification(from, align, color, msg) {
                type = ['', 'info', 'danger', 'success', 'warning', 'rose', 'primary'];

                $.notify({
                    icon: "add_alert",
                    message: msg

                }, {
                    type: color,
                    timer: 500,
                    placement: {
                        from: from,
                        align: align
                    }
                }
                );
            }
        </script>
    </body>
</html>
