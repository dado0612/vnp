<%@page import="model.Location"%>
<%@page import="java.util.List"%>
<%@page import="controller.LocationDAO"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>

<!DOCTYPE html>
<html>
    <%
        List<Location> listProvince = LocationDAO.selectAllProvince();
    %>
    <head>
        <meta charset="utf-8" />
        <link rel="apple-touch-icon" sizes="76x76" href="../../img//apple-icon.png">
        <link rel="icon" type="image/png" href="../../img//favicon.png">
        <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />
        <title>
            Fly Post | Postal Network
        </title>
        <meta content='width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0, shrink-to-fit=no' name='viewport' />
        <!--     Fonts and icons     -->
        <link rel="stylesheet" type="text/css" href="https://fonts.googleapis.com/css?family=Roboto:300,400,500,700|Roboto+Slab:400,700|Material+Icons" />
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/latest/css/font-awesome.min.css">
        <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.4.1/css/all.css" integrity="sha384-5sAR7xN1Nv6T6+dT2mhtzEpVJvfS3NScPQTrOxhwjIuvcA67KV2R5Jz6kr4abQsz"
              crossorigin="anonymous">
        <!-- CSS Files -->
        <link href="../../css/index/material-kit.css?v=2.0.4" rel="stylesheet" />
        <link rel="stylesheet" href="../../css/index/custom.css">

        <!-- Scrollbar Custom CSS -->
        <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/malihu-custom-scrollbar-plugin/3.1.5/jquery.mCustomScrollbar.min.css">
    </head>

    <body class="index-page sidebar-collapse">
        <!--navbar-->
        <nav class="navbar navbar-transparent navbar-color-on-scroll fixed-top navbar-expand-lg" color-on-scroll="100" id="sectionsNav">
            <div class="container">
                <div class="navbar-translate">
                    <a class="navbar-brand" href="../" style="font-size: 30px">
                        Fly Post</a>
                    <button class="navbar-toggler" type="button" data-toggle="collapse" aria-expanded="false" aria-label="Toggle navigation">
                        <span class="sr-only">Toggle navigation</span>
                        <span class="navbar-toggler-icon"></span>
                        <span class="navbar-toggler-icon"></span>
                        <span class="navbar-toggler-icon"></span>
                    </button>
                </div>
                <div class="collapse navbar-collapse">
                    <ul class="navbar-nav ml-auto">
                        <li class="dropdown nav-item">
                            <a href="" class="dropdown-toggle nav-link" data-toggle="dropdown">
                                <i class="fa fa-bars"></i> Service
                            </a>
                            <div class="dropdown-menu dropdown-with-icons">
                                <a href="feeCalculator.html" class="dropdown-item">
                                    <span><i class="fas fa-truck"></i></span> Fee Calculator
                                </a>
                                <a href="track.html" class="dropdown-item">
                                    <span><i class="fas fa-map-marker-alt"></i></span> Items Track & Trace
                                </a>
                                <a href="postalNetwork.jsp" class="dropdown-item">
                                    <span><i class="fas fa-map"></i></span> Postal network
                                </a>
                                <a href="prohibitedItems.html" class="dropdown-item">
                                    <span><i class="fas fa-ban"></i></span> Prohibited items checking
                                </a>
                            </div>
                        </li>
                        <li class="dropdown nav-item">
                            <a href="" class="dropdown-toggle nav-link" data-toggle="dropdown">
                                <i class="fas fa-phone"></i> Contact
                            </a>
                            <div class="dropdown-menu dropdown-with-icons">
                                <a href="" class="dropdown-item">
                                    <span><i class="fas fa-phone"></i></span> 0123456789
                                </a>
                                <a href="" class="dropdown-item">
                                    <span><i class="far fa-envelope"></i></span> flypostse1210@gmail.com
                                </a>
                                <a href="" class="dropdown-item">
                                    <span><i class="far fa-question-circle"></i></span> FAQ
                                </a>
                            </div>
                        </li>
                        <li class="nav-item">
                            <a href="about.html" class="nav-link">
                                <span><i class="far fa-newspaper fa-lg"></i></span> News
                            </a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link" rel="tooltip" title="" data-placement="bottom" href="feeCalculator.html" data-original-title="Price">
                                <i class="fas fa-search-dollar fa-lg"></i>
                            </a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link" rel="tooltip" title="" data-placement="bottom" href="" data-original-title="Review - enquiry">
                                <i class="far fa-comments fa-lg"></i>
                            </a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link" rel="tooltip" title="" data-placement="bottom" href="" data-original-title="Follow us on facebook">
                                <i class="fab fa-facebook-square fa-lg"></i>
                            </a>
                        </li>
                        <li class="nav-item">
                            <!--search bar-->
                            <form class="form-inline ml-auto form-row" style="padding: 0.5rem 1rem;" onsubmit="return false">
                                <div class="form-group no-border">
                                    <input type="text" class="form-control" placeholder="Search">
                                </div>
                                <button type="submit" class="btn btn-white btn-just-icon btn-round">
                                    <i class="material-icons">search</i>
                                </button>
                            </form>
                        </li>
                    </ul>
                </div>
            </div>
        </nav>
        <!--page-header-->
        <div class="page-header header-filter clear-filter" data-parallax="true" style="background-image: url('../../img//postalNetwork.jpg');">
            <div class="container">
                <div class="row">
                    <div class="col-md-8 ml-auto mr-auto">
                        <div class="brand">
                            <h1>Fly Post</h1>
                            <h3>Fast - Save - Simple</h3>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!--main container-->
        <div class="main main-raised">
            <div class="container">
                <div class="section">
                    <h3>Postal Network</h3>
                    <form id ="myForm" method="GET" action="../PostServlet">
                        <div class="row">
                            <div class="col-md-4">
                                <div class="form-group">
                                    <label for="province">Province</label>
                                    <select name="provinceID" id="provinceID" class="form-control" onchange="connectLocationServlet('province')">
                                        <option value="">Select One</option>
                                        <%
                                            for (Location l : listProvince) {
                                                out.print("<option value='" + l.getLocationID() + "'>" + l.getLocationName() + "</option>");
                                            }
                                        %>
                                    </select>
                                </div>
                            </div>
                            <div class="col-md-4">
                                <div class="form-group">
                                    <label for="district">District</label>
                                    <select name="districtID" id="districtID" class="form-control" onchange="connectLocationServlet('district')">
                                        <option value="">Select One</option>
                                    </select>
                                </div>
                            </div>
                            <div class="col-md-4">
                                <div class="form-group">
                                    <label for="commune">Commune</label>
                                    <select name="communeID" id="communeID" class="form-control">
                                        <option value="">Select One</option>
                                    </select>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-4">
                                <button type="submit" class="btn btn-primary mr-sm-3">Search</button>
                            </div>
                        </div>
                    </form>
                    <table class="table table-borderless table-hover" id="tablePost">
                        <tr>
                            <th>Post ID</th>
                            <th>Post Name</th>
                            <th>Address</th>
                        </tr>
                    </table>
                </div>
            </div>
        </div>
        <!--footer-->
        <footer class="footer" data-background-color="black">
            <div class="container">
                <nav class="float-left">
                    <ul>
                        <li>
                            <a href="../">
                                FLY POST
                            </a>
                        </li>
                        <li>
                            <a href="about.html">
                                About Us
                            </a>
                        </li>
                        <li>
                            <a href="about.html">
                                Blog
                            </a>
                        </li>
                    </ul>
                </nav>
                <div class="copyright float-right">
                    &copy;
                    <script>
                        document.write(new Date().getFullYear())
                    </script>, made with <i class="material-icons">favorite</i> by
                    <a href="https://www.creative-tim.com" target="_blank">Creative Tim</a> for a better web.
                </div>
            </div>
        </footer>
        <!--   Core JS Files   -->
        <script src="../../js/index/core/jquery.min.js" type="text/javascript"></script>
        <script src="../../js/index/core/popper.min.js" type="text/javascript"></script>
        <script src="../../js/index/core/bootstrap-material-design.min.js" type="text/javascript"></script>
        <script src="../../js/index/plugins/moment.min.js"></script>
        <!-- Control Center for Material Kit: parallax effects, scripts for the example pages etc -->
        <script src="../../js/index/material-kit.js?v=2.0.4" type="text/javascript"></script>

        <script>
                        // INIT AJAX
                        var request;
                        if (window.XMLHttpRequest) {
                            request = new XMLHttpRequest();
                        } else if (window.ActiveXObject) {
                            request = new ActiveXObject("Microsoft.XMLHTTP");
                        }

                        function connectLocationServlet(x) {
                            var url;
                            if (x == 'province') {
                                var provinceID = document.getElementById("provinceID").value;
                                document.getElementById('districtID').innerText = null;
                                var selectDistrict = document.getElementById("districtID");
                                var optionDistrict = document.createElement("option");
                                optionDistrict.value = "";
                                optionDistrict.text = "Select One";
                                selectDistrict.add(optionDistrict);

                                document.getElementById('communeID').innerText = null;
                                var selectCommune = document.getElementById("communeID");
                                var optionCommune = document.createElement("option");
                                optionCommune.value = "";
                                optionCommune.text = "Select One";
                                selectCommune.add(optionCommune);
                                if (provinceID == "") {
                                    return;
                                } else {
                                    url = "../LocationServlet?provinceID=" + provinceID;
                                }
                            } else {
                                document.getElementById('communeID').innerText = null;
                                var selectCommune = document.getElementById("communeID");
                                var optionCommune = document.createElement("option");
                                optionCommune.value = "";
                                optionCommune.text = "Select One";
                                selectCommune.add(optionCommune);
                                var districtID = document.getElementById("districtID").value;
                                if (districtID == "") {
                                    return;
                                } else {
                                    url = "../LocationServlet?provinceID=" + provinceID + "&districtID=" + districtID;
                                }
                            }
                            try {
                                request.onreadystatechange = getInfoLocation; // getInfor is below function
                                request.open("GET", url, true);
                                request.send();
                            } catch (e) {
                                alert("Unable to connect to server");
                            }
                        }

                        function getInfoLocation() {
                            if (request.readyState == 4) {
                                var val = request.responseText; // request.responseText = out.print in servlet
                                var locations = JSON.parse(val);
                                var selectLocation;
                                if (document.getElementById("districtID").value == "") {
                                    selectLocation = document.getElementById("districtID");
                                } else {
                                    selectLocation = document.getElementById("communeID");
                                }
                                var option = document.createElement("option");
                                for (var i = 0; i < locations.location.length; i++) {
                                    var locationID = locations.location[i].locationID;
                                    var locationName = locations.location[i].locationName;
                                    var option = document.createElement("option");
                                    option.text = locationName;
                                    option.value = locationID;
                                    selectLocation.add(option);
                                }
                            }
                        }
        </script>
        <script>
            $("#myForm").submit(function (event) {
                event.preventDefault(); //prevent default action 
                var post_url = $(this).attr("action"); //get form action url
                var request_method = $(this).attr("method"); //get form GET/POST method
                var form_data = $(this).serialize(); //Encode form elements for submission

                $.ajax({
                    url: post_url,
                    type: request_method,
                    data: form_data
                }).done(function (response) { //
                    //alert(response);

                    //document.getElementById('tablePost').innerHTML = null;
                    var tablePost = document.getElementById('tablePost');
                    var rows = tablePost.rows;
                    var i = rows.length;
                    while (--i > 0) {
                        tablePost.deleteRow(i);
                    }
                    if (!response == "") {
                        var json = JSON.parse(response);
                        for (var i = 0; i < json.posts.length; i++) {
                            var tr = tablePost.insertRow();
                            var td1 = tr.insertCell(0);
                            var td2 = tr.insertCell(1);
                            var td3 = tr.insertCell(2);
                            td1.innerHTML = json.posts[i].postID;
                            td2.innerHTML = json.posts[i].postName;
                            td3.innerHTML = json.posts[i].address;
                        }
                    }

                });
            });
        </script>

        <!-- Subiz -->
        <script>
            (function (s, u, b, i, z) {
                u[i] = u[i] || function () {
                    u[i].t = +new Date();
                    (u[i].q = u[i].q || []).push(arguments);
                };
                z = s.createElement('script');
                var zz = s.getElementsByTagName('script')[0];
                z.async = 1;
                z.src = b;
                z.id = 'subiz-script';
                zz.parentNode.insertBefore(z, zz);
            })(document, window, 'https://widgetv4.subiz.com/static/js/app.js', 'subiz');
            subiz('setAccount', 'acqdyzswvoagxtwmqmij');
        </script>
        <!-- End Subiz -->
    </body>
</html>
