<%@page import="util.DateUtil"%>
<%@page import="controller.RoleDAO"%>
<%@page import="controller.PostDAO"%>
<%@page import="javafx.util.Pair"%>
<%@page import="util.SessionUtil"%>
<%@page import="config.RoleConfig"%>
<%@page import="config.RoleConfig"%>
<%@page import="model.Employee"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>

<!doctype html>
<html lang="en">
    <%
        Pair<Boolean, Employee> checkSession = SessionUtil.checkSession(session, RoleConfig.MANAGER);
        if (checkSession.getKey()) {
            response.sendRedirect("../index/login.html");
            return;
        }
        Employee emp = checkSession.getValue();
    %>
    <head>
        <meta charset="utf-8" />
        <link rel="apple-touch-icon" sizes="76x76" href="../../img/apple-icon.png">
        <link rel="icon" type="image/png" href="../../img/favicon.png">
        <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />
        <title>
            Fly Post | User Profile
        </title>
        <meta content='width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0, shrink-to-fit=no' name='viewport' />
        <!--     Fonts and icons     -->
        <link rel="stylesheet" type="text/css" href="https://fonts.googleapis.com/css?family=Roboto:300,400,500,700|Roboto+Slab:400,700|Material+Icons" />
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/latest/css/font-awesome.min.css">
        <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.4.1/css/all.css" integrity="sha384-5sAR7xN1Nv6T6+dT2mhtzEpVJvfS3NScPQTrOxhwjIuvcA67KV2R5Jz6kr4abQsz"
              crossorigin="anonymous">
        <!-- CSS Files -->
        <link href="../../css/dashboard/material-dashboard.css?v=2.1.0" rel="stylesheet" />
        <link rel="stylesheet" href="../../css/dashboard/custom.css">
    </head>

    <body class="">
        <div class="wrapper ">
            <div class="sidebar" data-color="azure" data-background-color="white" data-image="../../img/sidebar-1.jpg">
                <div class="logo">
                    <a href="./dashboard.jsp" class="simple-text logo-normal">
                        FLY POST
                    </a>
                </div>
                <div class="sidebar-wrapper">
                    <ul class="nav">
                        <li class="nav-item">
                            <a class="nav-link" href="./dashboard.jsp">
                                <i class="material-icons">dashboard</i>
                                <p>Dashboard</p>
                            </a>
                        </li>
                        <li class="nav-item ">
                            <a class="nav-link" href="./newOrder.jsp">
                                <i class="material-icons">library_add</i>
                                <p>New order</p>
                            </a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link" href="./checkInOut.jsp">
                                <i class="material-icons">call_received</i>
                                <p>Check In/Out</p>
                            </a>
                        </li>
                        <li class="nav-item ">
                            <a class="nav-link" href="./storage.jsp">
                                <i class="material-icons">list</i>
                                <p>Storage</p>
                            </a>
                        </li>
                        <li class="nav-item ">
                            <a class="nav-link" href="./history.jsp">
                                <i class="material-icons">history</i>
                                <p>History</p>
                            </a>
                        </li>
                        <li class="nav-item active">
                            <a class="nav-link" href="./userProfile.jsp">
                                <i class="material-icons">person</i>
                                <p>User Profile</p>
                            </a>
                        </li>
                        <li class="nav-item ">
                            <a class="nav-link" href="./logout.jsp">
                                <i class="material-icons">power_settings_new</i>
                                <p>Logout</p>
                            </a>
                        </li>
                    </ul>
                </div>
            </div>
            <div class="main-panel">
                <!-- Navbar -->
                <nav class="navbar navbar-expand-lg navbar-transparent navbar-absolute fixed-top ">
                    <div class="container-fluid">
                        <div class="navbar-wrapper">
                            <a class="navbar-brand" href="./dashboard.jsp">Dashboard</a>
                        </div>
                        <button class="navbar-toggler" type="button" data-toggle="collapse" aria-controls="navigation-index"
                                aria-expanded="false" aria-label="Toggle navigation">
                            <span class="sr-only">Toggle navigation</span>
                            <span class="navbar-toggler-icon icon-bar"></span>
                            <span class="navbar-toggler-icon icon-bar"></span>
                            <span class="navbar-toggler-icon icon-bar"></span>
                        </button>
                        <div class="collapse navbar-collapse justify-content-end">
                            <ul class="navbar-nav">
                                <li class="nav-item">
                                    <a class="nav-link" href="./dashboard.jsp">
                                        <i class="material-icons">dashboard</i>
                                        <p class="d-lg-none d-md-block">
                                            Stats
                                        </p>
                                    </a>
                                </li>
                                <li class="nav-item dropdown">
                                    <a class="nav-link" href="" id="navbarDropdownMenuLink" data-toggle="dropdown" aria-haspopup="true"
                                       aria-expanded="false">
                                        <i class="material-icons">person</i>
                                        <p class="d-lg-none d-md-block">
                                            Account
                                        </p>
                                    </a>
                                    <div class="dropdown-menu dropdown-menu-right dropdown-with-icons" aria-labelledby="navbarDropdownMenuLink">
                                        <a class="dropdown-item" href="./userProfile.jsp">
                                            <span><i class="fas fa-sliders-h"></i></span>
                                            Setting
                                        </a>
                                        <a class="dropdown-item" href="./logout.jsp">
                                            <span><i class="fas fa-power-off"></i></span>
                                            Logout
                                        </a>
                                    </div>
                                </li>
                            </ul>
                        </div>
                    </div>
                </nav>
                <!-- End Navbar -->
                <div class="content">
                    <div class="container-fluid">
                        <!-- your content here -->
                        <div class="row">
                            <div class="col-md-8">
                                <div class="card">
                                    <div class="card-header card-header-tabs card-header-info">
                                        <div class="nav-tabs-navigation">
                                            <div class="nav-tabs-wrapper">
                                                <span class="nav-tabs-title">Account Info:</span>
                                                <ul class="nav nav-tabs" data-tabs="tabs">
                                                    <li class="nav-item">
                                                        <a class="nav-link active" href="#profile" data-toggle="tab">
                                                            <i class="material-icons">account_circle</i> personal
                                                            <div class="ripple-container"></div>
                                                        </a>
                                                    </li>
                                                    <li class="nav-item">
                                                        <a class="nav-link" href="#messages" data-toggle="tab">
                                                            <i class="material-icons">security</i> Password
                                                            <div class="ripple-container"></div>
                                                        </a>
                                                    </li>
                                                </ul>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="card-body">
                                        <div class="tab-content">
                                            <div class="tab-pane active" id="profile">
                                                <form onsubmit="return false">
                                                    <div class="row">
                                                        <div class="col-md-4">
                                                            <div class="form-group">
                                                                <label class="bmd-label-floating">Employee ID</label>
                                                                <input type="text" class="form-control" value="<%= emp.getEmployeeID()%>" readonly>
                                                            </div>
                                                        </div>
                                                        <div class="col-md-8">
                                                            <div class="form-group">
                                                                <label class="bmd-label-floating">Name</label>
                                                                <input type="text" class="form-control" value="<%= emp.getName()%>" readonly>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="row">
                                                        <div class="col-md-3">
                                                            <div class="form-group">
                                                                <label class="bmd-label-floating">Post ID</label>
                                                                <!--change post name here-->
                                                                <input type="text" class="form-control" value="<%= emp.getPostID()%>" readonly>
                                                            </div>
                                                        </div>
                                                        <div class="col-md-5">
                                                            <div class="form-group">
                                                                <label class="bmd-label-floating">Post name</label>
                                                                <!--change post name here-->
                                                                <input type="text" class="form-control" value="<%= PostDAO.selectByPostID(emp.getPostID()).getPostName()%>" readonly>
                                                            </div>
                                                        </div>
                                                        <div class="col-md-4">
                                                            <div class="form-group">
                                                                <label class="bmd-label-floating">Role</label>
                                                                <input type="text" class="form-control" value="<%= RoleDAO.selectByUsername(emp.getUsername()).getRoleName()%>" readonly>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="row">
                                                        <div class="col-md-6">
                                                            <div class="form-group">
                                                                <label class="bmd-label-floating">Gender</label>
                                                                <input type="text" class="form-control" value="<%= emp.isGender() ? "Male" : "Female"%>" readonly>
                                                            </div>
                                                        </div>
                                                        <div class="col-md-6">
                                                            <div class="form-group">
                                                                <label class="bmd-label-floating">Date of birth</label>
                                                                <input type="text" class="form-control" readonly value="<%= DateUtil.formatDate(emp.getDob())%>">
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="row">
                                                        <div class="col-md-4">
                                                            <div class="form-group">
                                                                <label class="bmd-label-floating">Phone</label>
                                                                <input type="text" id="phone" class="form-control" value="<%= emp.getPhone()%>">
                                                            </div>
                                                        </div>
                                                        <div class="col-md-4">
                                                            <div class="form-group">
                                                                <label class="bmd-label-floating">Work from</label>
                                                                <input type="text" class="form-control" value="<%= DateUtil.formatDate(emp.getHireDate())%>" readonly>
                                                            </div>
                                                        </div>
                                                        <div class="col-md-4">
                                                            <div class="form-group">
                                                                <label class="bmd-label-floating">Identity number</label>
                                                                <input type="text" class="form-control" value="<%= emp.getIdentityNum()%>" readonly>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <button type="button" class="btn btn-primary pull-right" onclick="changePhone()">Update</button>
                                                    <div class="clearfix"></div>
                                                </form>
                                            </div>
                                            <div class="tab-pane" id="messages">
                                                <form onsubmit="return false">
                                                    <div class="row">
                                                        <div class="col-md-6">
                                                            <div class="form-group">
                                                                <label class="bmd-label-floating">Old password</label>
                                                                <input type="password" id="oldPassword" class="form-control">
                                                            </div>
                                                        </div>
                                                        <div class="col-md-6">
                                                            <div class="form-group">
                                                                <label class="bmd-label-floating">New password</label>
                                                                <input type="password" id="newPassword" class="form-control">
                                                            </div>
                                                            <div class="form-group">
                                                                <label class="bmd-label-floating">Repeat new password</label>
                                                                <input type="password" id="repeatPassword" class="form-control">
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <button type="button" class="btn btn-primary pull-right" onclick="changePassword()">Update</button>
                                                    <div class="clearfix"></div>
                                                </form>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-4">
                                <div class="card card-profile">
                                    <div class="card-avatar">
                                        <img class="img" src="../../img/avatar.jpg" />
                                    </div>
                                    <div class="card-body">
                                        <h6 class="card-category text-gray"><%= RoleDAO.selectByUsername(emp.getUsername()).getRoleName()%></h6>
                                        <h4 class="card-title"><%= emp.getName()%></h4>
                                        <p class="card-description">
                                            Don't be scared of the truth because we need to restart the human foundation in truth
                                            And I love you like Kanye loves Kanye I love Rick Owens’ bed design but the back is...
                                        </p>
                                        <a href="" class="btn btn-primary btn-round">Follow</a>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <footer class="footer">
                    <div class="container-fluid">
                        <nav class="float-left">
                            <ul>
                                <li>
                                    <a href="../">
                                        FLY POST
                                    </a>
                                </li>
                                <li>
                                    <a href="../index/about.html">
                                        About Us
                                    </a>
                                </li>
                                <li>
                                    <a href="../index/about.html">
                                        Blog
                                    </a>
                                </li>
                            </ul>
                        </nav>
                        <div class="copyright float-right">
                            &copy;
                            <script>
                                document.write(new Date().getFullYear())
                            </script>, made with <i class="material-icons">favorite</i> by
                            <a href="https://www.creative-tim.com" target="_blank">Creative Tim</a> for a better web.
                        </div>
                        <!-- your footer here -->
                    </div>
                </footer>
            </div>
        </div>
        <!--   Core JS Files   -->
        <script src="../../js/dashboard/core/jquery.min.js" type="text/javascript"></script>
        <script src="../../js/dashboard/core/popper.min.js" type="text/javascript"></script>
        <script src="../../js/dashboard/core/bootstrap-material-design.min.js" type="text/javascript"></script>
        <script src="../../js/dashboard/plugins/perfect-scrollbar.jquery.min.js"></script>
        <!--  Notifications Plugin    -->
        <script src="../../js/dashboard/plugins/bootstrap-notify.js"></script>
        <!-- Control Center for Material Dashboard: parallax effects, scripts for the example pages etc -->
        <script src="../../js/dashboard/material-dashboard.min.js?v=2.1.0" type="text/javascript"></script>
        <script src="../../js/dashboard/plugins/md5.js"></script>
        <script>
                                // INIT AJAX
                                var request;
                                if (window.XMLHttpRequest) {
                                    request = new XMLHttpRequest();
                                } else if (window.ActiveXObject) {
                                    request = new ActiveXObject("Microsoft.XMLHTTP");
                                }

                                function changePhone() {
                                    var newPhone = document.getElementById("phone").value.trim();
                                    document.getElementById("phone").value = newPhone;
                                    var url = "../ChangePhoneServlet?phone=" + newPhone;
                                    try {
                                        request.onreadystatechange = getInfoPhone; // getInfor is below function
                                        request.open("GET", url, true);
                                        request.send();
                                    } catch (e) {
                                        alert("Unable to connect to server");
                                    }
                                }

                                function changePassword() {
                                    var oldPass = document.getElementById('oldPassword').value.trim();
                                    var newPass = document.getElementById('newPassword').value.trim();
                                    var repeatPass = document.getElementById('repeatPassword').value.trim();
                                    document.getElementById('oldPassword').value = '';
                                    document.getElementById('newPassword').value = '';
                                    document.getElementById('repeatPassword').value = '';

                                    if (oldPass == '') {
                                        showNotification('top', 'center', 'danger', 'Old password is not valid!');
                                        return;
                                    }
                                    if (newPass == '') {
                                        showNotification('top', 'center', 'danger', 'New password is not valid!');
                                        return;
                                    }

                                    if (newPass != repeatPass) {
                                        showNotification('top', 'center', 'danger', 'Repeat password incorrect!');
                                    } else {
                                        var url = "../ChangePasswordServlet?oldPassword=" + md5(oldPass) + "&newPassword=" + md5(newPass);
                                        try {
                                            request.onreadystatechange = getInfoPass; // getInfor is below function
                                            request.open("GET", url, true);
                                            request.send();
                                        } catch (e) {
                                            alert("Unable to connect to server");
                                        }
                                    }
                                }

                                function getInfoPhone() {
                                    if (request.readyState == 4) {
                                        var val = request.responseText; // request.responseText = out.print in servlet
                                        if (val == 'ok') {
                                            showNotification('top', 'center', 'success', 'Update new phone successful!');
                                        } else if (val == 'error') {
                                            showNotification('top', 'center', 'danger', 'Update fail!');
                                        }
                                    }
                                }

                                function getInfoPass() {
                                    if (request.readyState == 4) {
                                        var val = request.responseText; // request.responseText = out.print in servlet
                                        if (val == 'ok') {
                                            showNotification('top', 'center', 'success', 'Update new password successful!');
                                        } else if (val == 'error') {
                                            showNotification('top', 'center', 'danger', 'Update fail or incorrect password!');
                                        }
                                    }
                                }

                                function showNotification(from, align, color, msg) {
                                    type = ['', 'info', 'danger', 'success', 'warning', 'rose', 'primary'];

                                    $.notify({
                                        icon: "add_alert",
                                        message: msg

                                    }, {
                                        type: color,
                                        timer: 500,
                                        placement: {
                                            from: from,
                                            align: align
                                        }
                                    }
                                    );
                                }
        </script>
    </body>
</html>
