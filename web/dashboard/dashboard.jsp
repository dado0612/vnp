<%@page import="javafx.util.Pair"%>
<%@page import="util.SessionUtil"%>
<%@page import="config.RoleConfig"%>
<%@page import="model.Post"%>
<%@page import="controller.PostDAO"%>
<%@page import="controller.BillDAO"%>
<%@page import="controller.TrackDAO"%>
<%@page import="controller.EmployeeDAO"%>
<%@page import="model.Bill"%>
<%@page import="model.Employee"%>
<%@page import="java.text.SimpleDateFormat"%>
<%@page import="java.util.Calendar"%>
<%@page import="java.util.Date"%>
<%@page import="java.util.ArrayList"%>
<%@page import="java.util.List"%>
<%@page import="model.Track"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>

<!DOCTYPE html>
<html lang="en">
    <%
        Pair<Boolean, Employee> checkSession = SessionUtil.checkSession(session, RoleConfig.MANAGER);
        if (checkSession.getKey()) {
            response.sendRedirect("../index/login.html");
            return;
        }
        Employee emp = checkSession.getValue();
    %>
    <head>
        <meta charset="utf-8" />
        <link rel="apple-touch-icon" sizes="76x76" href="../../img/apple-icon.png">
        <link rel="icon" type="image/png" href="../../img/favicon.png">
        <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />
        <title>
            Fly Post | Dashboard
        </title>
        <meta content='width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0, shrink-to-fit=no' name='viewport' />
        <!--     Fonts and icons     -->
        <link rel="stylesheet" type="text/css" href="https://fonts.googleapis.com/css?family=Roboto:300,400,500,700|Roboto+Slab:400,700|Material+Icons" />
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/latest/css/font-awesome.min.css">
        <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.4.1/css/all.css" integrity="sha384-5sAR7xN1Nv6T6+dT2mhtzEpVJvfS3NScPQTrOxhwjIuvcA67KV2R5Jz6kr4abQsz"
              crossorigin="anonymous">
        <!-- CSS Files -->
        <link href="../../css/dashboard/material-dashboard.css?v=2.1.0" rel="stylesheet" />
        <link rel="stylesheet" href="../../css/dashboard/custom.css">
        <!-- CSS Just for demo purpose, don't include it in your project -->
        <link href="../../css/dashboard/demo.css" rel="stylesheet" />
    </head>

    <body class="">
        <%!
            String toLabelString(String[] label) {
                String result = "";
                for (int i = 0; i < label.length; i++) {
                    result += "'" + label[label.length - i - 1] + "', ";
                }
                return result;
            }

            String toDataString(int[] data) {
                String result = "";
                for (int i = 0; i < data.length; i++) {
                    result += "" + data[data.length - i - 1] + ", ";
                }
                return result;
            }

            String toDataString(long[] data) {
                String result = "";
                for (int i = 0; i < data.length; i++) {
                    result += "" + data[data.length - i - 1] + ", ";
                }
                return result;
            }

            int max(int[] num) {
                int max = 0;
                for (int i = 0; i < num.length; i++) {
                    if (num[i] > max) {
                        max = num[i];
                    }
                }
                return max;

            }

            long max(long[] num) {
                long max = 0;
                for (int i = 0; i < num.length; i++) {
                    if (num[i] > max) {
                        max = num[i];
                    }
                }
                return max;
            }

            String numShort(long num) {
                if (num > 999999) {
                    double result = (double) num / 1000000;
                    return "" + ((double) Math.round(result * 100) / 100) + "m";
                } else if (num > 9999) {
                    double result = (double) num / 1000;
                    return "" + ((double) Math.round(result * 100) / 100) + "k";
                } else {
                    return "" + num;
                }
            }
        %>
        <%
            int RANGE_OF_DAY = 7; //range of statics when report by day
            int RANGE_OF_MONTH = 12; //range of statics when report by month
            int RANGE_OF_YEAR = 3; //range of statics when report by year

            //Set x-asis
            String[] days = new String[RANGE_OF_DAY];
            String[] months = new String[RANGE_OF_MONTH];
            String[] years = new String[RANGE_OF_YEAR];

            int[] numIncomePackLastNDay = TrackDAO.numberIncomePackLastNDay(emp.getPostID(), RANGE_OF_DAY);
            int[] numIncomePackLastNMonth = TrackDAO.numberIncomePackLastNMonth(emp.getPostID(), RANGE_OF_MONTH);
            int[] numIncomePackLastNYear = TrackDAO.numberIncomePackLastNYear(emp.getPostID(), RANGE_OF_YEAR);

            int[] numInitPackLastNDay = TrackDAO.numberInitPackLastNDay(emp.getPostID(), RANGE_OF_DAY);
            int[] numInitPackLastNMonth = TrackDAO.numberInitPackLastNMonth(emp.getPostID(), RANGE_OF_MONTH);
            int[] numInitPackLastNYear = TrackDAO.numberInitPackLastNYear(emp.getPostID(), RANGE_OF_YEAR);

            long[] numRevenueLastNDay = BillDAO.revenueLastNDays(emp.getPostID(), RANGE_OF_DAY);
            long[] numRevenueLastNMonth = BillDAO.revenueLastNMonths(emp.getPostID(), RANGE_OF_MONTH);
            long[] numRevenueLastNYear = BillDAO.revenueLastNYears(emp.getPostID(), RANGE_OF_YEAR);

            int totalStorage = TrackDAO.selectStorageByPostID(emp.getPostID()).size();
            int totalProcessByCurYear = numIncomePackLastNYear[0];

            long totalRevenueByCurYear = BillDAO.totalRevenueByCurrentYear(emp.getPostID());

            int totalInitByCurYear = numInitPackLastNYear[0];

            //Init value for x-asis
            for (int i = 0; i < RANGE_OF_DAY; i++) {
                Calendar cal = Calendar.getInstance();
                cal.add(Calendar.DAY_OF_MONTH, -i);
                days[i] = new SimpleDateFormat("EE").format(cal.getTime());
            }

            for (int i = 0; i < RANGE_OF_MONTH; i++) {
                Calendar cal = Calendar.getInstance();
                cal.add(Calendar.MONTH, -i);
                months[i] = new SimpleDateFormat("MMM").format(cal.getTime());
            }

            for (int i = 0; i < RANGE_OF_YEAR; i++) {
                Calendar cal = Calendar.getInstance();
                cal.add(Calendar.YEAR, -i);
                years[i] = new SimpleDateFormat("yyyy").format(cal.getTime());
            }

            //xaxis
            String day = toLabelString(days);
            String month = toLabelString(months);
            String year = toLabelString(years);

            //yaxist
            String numIncomePackLastNDayData = toDataString(numIncomePackLastNDay);
            String numIncomePackLastNMonthData = toDataString(numIncomePackLastNMonth);
            String numIncomePackLastNYearData = toDataString(numIncomePackLastNYear);
            String numInitPackLastNDayData = toDataString(numInitPackLastNDay);
            String numInitPackLastNMonthData = toDataString(numInitPackLastNMonth);
            String numInitPackLastNYearData = toDataString(numInitPackLastNYear);
            String numRevenueLastNDayData = toDataString(numRevenueLastNDay);
            String numRevenueLastNMonthData = toDataString(numRevenueLastNMonth);
            String numRevenueLastNYearData = toDataString(numRevenueLastNYear);
        %>
        <div class="wrapper ">
            <div class="sidebar" data-color="azure" data-background-color="white" data-image="../../img/sidebar-1.jpg">
                <div class="logo">
                    <a href="./dashboard.jsp" class="simple-text logo-normal">
                        FLY POST
                    </a>
                </div>
                <div class="sidebar-wrapper">
                    <ul class="nav">
                        <li class="nav-item  active">
                            <a class="nav-link" href="./dashboard.jsp">
                                <i class="material-icons">dashboard</i>
                                <p>Dashboard</p>
                            </a>
                        </li>
                        <li class="nav-item ">
                            <a class="nav-link" href="./newOrder.jsp">
                                <i class="material-icons">library_add</i>
                                <p>New order</p>
                            </a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link" href="./checkInOut.jsp">
                                <i class="material-icons">call_received</i>
                                <p>Check In/Out</p>
                            </a>
                        </li>
                        <li class="nav-item ">
                            <a class="nav-link" href="./storage.jsp">
                                <i class="material-icons">list</i>
                                <p>Storage</p>
                            </a>
                        </li>
                        <li class="nav-item ">
                            <a class="nav-link" href="./history.jsp">
                                <i class="material-icons">history</i>
                                <p>History</p>
                            </a>
                        </li>
                        <li class="nav-item ">
                            <a class="nav-link" href="./userProfile.jsp">
                                <i class="material-icons">person</i>
                                <p>User Profile</p>
                            </a>
                        </li>
                        <li class="nav-item ">
                            <a class="nav-link" href="./logout.jsp">
                                <i class="material-icons">power_settings_new</i>
                                <p>Logout</p>
                            </a>
                        </li>
                    </ul>
                </div>
            </div>
            <div class="main-panel">
                <!-- Navbar -->
                <nav class="navbar navbar-expand-lg navbar-transparent navbar-absolute fixed-top ">
                    <div class="container-fluid">
                        <div class="navbar-wrapper">
                            <a class="navbar-brand" href="./dashboard.jsp">Dashboard</a>
                        </div>
                        <button class="navbar-toggler" type="button" data-toggle="collapse" aria-controls="navigation-index"
                                aria-expanded="false" aria-label="Toggle navigation">
                            <span class="sr-only">Toggle navigation</span>
                            <span class="navbar-toggler-icon icon-bar"></span>
                            <span class="navbar-toggler-icon icon-bar"></span>
                            <span class="navbar-toggler-icon icon-bar"></span>
                        </button>
                        <div class="collapse navbar-collapse justify-content-end">
                            <ul class="navbar-nav">
                                <li class="nav-item">
                                    <a class="nav-link" href="./dashboard.jsp">
                                        <i class="material-icons">dashboard</i>
                                        <p class="d-lg-none d-md-block">
                                            Stats
                                        </p>
                                    </a>
                                </li>
                                <li class="nav-item dropdown">
                                    <a class="nav-link" href="" id="navbarDropdownMenuLink" data-toggle="dropdown" aria-haspopup="true"
                                       aria-expanded="false">
                                        <i class="material-icons">person</i>
                                        <p class="d-lg-none d-md-block">
                                            Account
                                        </p>
                                    </a>
                                    <div class="dropdown-menu dropdown-menu-right dropdown-with-icons" aria-labelledby="navbarDropdownMenuLink">
                                        <a class="dropdown-item" href="./userProfile.jsp">
                                            <span><i class="fas fa-sliders-h"></i></span>
                                            Setting
                                        </a>
                                        <a class="dropdown-item" href="./logout.jsp">
                                            <span><i class="fas fa-power-off"></i></span>
                                            Logout
                                        </a>
                                    </div>
                                </li>
                            </ul>
                        </div>
                    </div>
                </nav>
                <!-- End Navbar -->
                <div class="content">
                    <div class="container-fluid">
                        <!--content go here-->
                        <div class="row">
                            <div class="col-lg-3 col-md-6 col-sm-6">
                                <div class="card card-stats">
                                    <div class="card-header card-header-warning card-header-icon">
                                        <div class="card-icon">
                                            <i class="material-icons">library_add</i>
                                        </div>
                                        <p class="card-category">New order</p>
                                        <h3 class="card-title"><%= numShort(totalInitByCurYear)%></h3><!--change value new order-->
                                    </div>
                                    <div class="card-footer">
                                        <div class="stats">
                                            <i class="material-icons">update</i> Just Updated
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-lg-3 col-md-6 col-sm-6">
                                <div class="card card-stats">
                                    <div class="card-header card-header-success card-header-icon">
                                        <div class="card-icon">
                                            <i class="material-icons">monetization_on</i>
                                        </div>
                                        <p class="card-category">Revenue</p>
                                        <h3 class="card-title"><%= numShort(totalRevenueByCurYear)%></h3><!--change-->
                                    </div>
                                    <div class="card-footer">
                                        <div class="stats">
                                            <i class="material-icons">update</i> Just Updated
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-lg-3 col-md-6 col-sm-6">
                                <div class="card card-stats">
                                    <div class="card-header card-header-danger card-header-icon">
                                        <div class="card-icon">
                                            <i class="material-icons">info_outline</i>
                                        </div>
                                        <p class="card-category">Storage</p>
                                        <h3 class="card-title"><%= numShort(totalStorage)%></h3><!--chnage-->
                                    </div>
                                    <div class="card-footer">
                                        <div class="stats">
                                            <i class="material-icons">update</i> Just Updated
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-lg-3 col-md-6 col-sm-6">
                                <div class="card card-stats">
                                    <div class="card-header card-header-info card-header-icon">
                                        <div class="card-icon">
                                            <i class="material-icons">transform</i>
                                        </div>
                                        <p class="card-category">Process</p>
                                        <h3 class="card-title"><%= numShort(totalProcessByCurYear)%></h3><!--change-->
                                    </div>
                                    <div class="card-footer">
                                        <div class="stats">
                                            <i class="material-icons">update</i> Just Updated
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div>
                            <div class="card ">
                                <div class="card-header card-header-tabs card-header-info" style="padding-top: 0px !important; padding-bottom: 0px !important">
                                    <div class="nav-tabs-navigation">
                                        <div class="nav-tabs-wrapper">
                                            <span class="nav-tabs-title">Statistic Chart:</span>
                                            <ul class="nav nav-tabs" data-tabs="tabs">
                                                <li class="nav-item" onclick="chart.initDashboardPageCharts('day')">
                                                    <a class="nav-link active" href="#byDayTab" data-toggle="tab">
                                                        <i class="material-icons">bubble_chart</i> By days
                                                        <div class="ripple-container"></div>
                                                    </a>
                                                </li>
                                                <li class="nav-item" onclick="chart.initDashboardPageCharts('month')">
                                                    <a class="nav-link" href="#byMonthTab" data-toggle="tab">
                                                        <i class="material-icons">insert_chart</i> By Months
                                                        <div class="ripple-container"></div>
                                                    </a>
                                                </li>
                                                <li class="nav-item" onclick="chart.initDashboardPageCharts('year')">
                                                    <a class="nav-link" href="#byYearTab" data-toggle="tab">
                                                        <i class="material-icons">show_chart</i> By Years
                                                        <div class="ripple-container"></div>
                                                    </a>
                                                </li>
                                            </ul>
                                        </div>
                                    </div>
                                </div>
                                <div class="card-body">
                                    <div class="tab-content">
                                        <div class="tab-pane active" id="byDayTab">
                                            <div class="row">
                                                <div class="col-xl-4 col-lg-12 col-md-4">
                                                    <div class="card card-chart">
                                                        <div class="card-header card-header-success">
                                                            <div class="ct-chart" id="revenueChartday"></div>
                                                        </div>
                                                        <div class="card-body">
                                                            <h4 class="card-title">Revenue</h4>
                                                            <p class="card-category" id="desRevenue">
                                                                Last 7 days statistics</p>
                                                        </div>
                                                        <div class="card-footer">
                                                            <div class="stats">
                                                                <i class="material-icons">access_time</i> Just Updated
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="col-xl-4 col-lg-12 col-md-4">
                                                    <div class="card card-chart">
                                                        <div class="card-header card-header-warning">
                                                            <div class="ct-chart" id="initNewOrderChartday"></div>
                                                        </div>
                                                        <div class="card-body">
                                                            <h4 class="card-title">New Order</h4>
                                                            <p class="card-category" id="desNewOrder">Last 7 days statistics</p>
                                                        </div>
                                                        <div class="card-footer">
                                                            <div class="stats">
                                                                <i class="material-icons">access_time</i> Just Updated
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="col-xl-4 col-lg-12 col-md-4">
                                                    <div class="card card-chart">
                                                        <div class="card-header card-header-info">
                                                            <div class="ct-chart" id="processChartday"></div>
                                                        </div>
                                                        <div class="card-body">
                                                            <h4 class="card-title">Process</h4>
                                                            <p class="card-category" id="desProcess">Last 7 days statistics</p>
                                                        </div>
                                                        <div class="card-footer">
                                                            <div class="stats">
                                                                <i class="material-icons">access_time</i> Just Updated
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="tab-pane" id="byMonthTab">
                                            <div class="row">
                                                <div class="col-md-12">
                                                    <div class="card card-chart">
                                                        <div class="card-header card-header-success">
                                                            <div class="ct-chart" id="revenueChartmonth"></div>
                                                        </div>
                                                        <div class="card-body">
                                                            <h4 class="card-title">Revenue</h4>
                                                            <p class="card-category" id="desRevenue">
                                                                Last 12 months statistics</p>
                                                        </div>
                                                        <div class="card-footer">
                                                            <div class="stats">
                                                                <i class="material-icons">access_time</i> Just Updated
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="col-md-12">
                                                    <div class="card card-chart">
                                                        <div class="card-header card-header-warning">
                                                            <div class="ct-chart" id="initNewOrderChartmonth"></div>
                                                        </div>
                                                        <div class="card-body">
                                                            <h4 class="card-title">New Order</h4>
                                                            <p class="card-category" id="desNewOrder">Last 12 months statistics</p>
                                                        </div>
                                                        <div class="card-footer">
                                                            <div class="stats">
                                                                <i class="material-icons">access_time</i> Just Updated
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="col-md-12">
                                                    <div class="card card-chart">
                                                        <div class="card-header card-header-info">
                                                            <div class="ct-chart" id="processChartmonth"></div>
                                                        </div>
                                                        <div class="card-body">
                                                            <h4 class="card-title">Process</h4>
                                                            <p class="card-category" id="desProcess">Last 12 months statistics</p>
                                                        </div>
                                                        <div class="card-footer">
                                                            <div class="stats">
                                                                <i class="material-icons">access_time</i> Just Updated
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="tab-pane" id="byYearTab">
                                            <div class="row">
                                                <div class="col-md-4">
                                                    <div class="card card-chart">
                                                        <div class="card-header card-header-success">
                                                            <div class="ct-chart" id="revenueChartyear"></div>
                                                        </div>
                                                        <div class="card-body">
                                                            <h4 class="card-title">Revenue</h4>
                                                            <p class="card-category" id="desRevenue">
                                                                Last 3 year statistics</p>
                                                        </div>
                                                        <div class="card-footer">
                                                            <div class="stats">
                                                                <i class="material-icons">access_time</i> Just Updated
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="col-md-4">
                                                    <div class="card card-chart">
                                                        <div class="card-header card-header-warning">
                                                            <div class="ct-chart" id="initNewOrderChartyear"></div>
                                                        </div>
                                                        <div class="card-body">
                                                            <h4 class="card-title">New Order</h4>
                                                            <p class="card-category" id="desNewOrder">Last 3 year statistics</p>
                                                        </div>
                                                        <div class="card-footer">
                                                            <div class="stats">
                                                                <i class="material-icons">access_time</i> Just Updated
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="col-md-4">
                                                    <div class="card card-chart">
                                                        <div class="card-header card-header-info">
                                                            <div class="ct-chart" id="processChartyear"></div>
                                                        </div>
                                                        <div class="card-body">
                                                            <h4 class="card-title">Process</h4>
                                                            <p class="card-category" id="desProcess">Last 3 year statistics</p>
                                                        </div>
                                                        <div class="card-footer">
                                                            <div class="stats">
                                                                <i class="material-icons">access_time</i> Just Updated
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <footer class="footer">
                    <div class="container-fluid">
                        <nav class="float-left">
                            <ul>
                                <li>
                                    <a href="../">
                                        FLY POST
                                    </a>
                                </li>
                                <li>
                                    <a href="../index/about.html">
                                        About Us
                                    </a>
                                </li>
                                <li>
                                    <a href="../index/about.html">
                                        Blog
                                    </a>
                                </li>
                            </ul>
                        </nav>
                        <div class="copyright float-right">
                            &copy;
                            <script>
                                document.write(new Date().getFullYear())
                            </script>, made with <i class="material-icons">favorite</i> by
                            <a href="https://www.creative-tim.com" target="_blank">Creative Tim</a> for a better web.
                        </div>
                    </div>
                </footer>
            </div>
        </div>
        <!--   Core JS Files   -->
        <script src="../../js/dashboard/core/jquery.min.js" type="text/javascript"></script>
        <script src="../../js/dashboard/core/popper.min.js" type="text/javascript"></script>
        <script src="../../js/dashboard/core/bootstrap-material-design.min.js" type="text/javascript"></script>
        <script src="../../js/dashboard/plugins/perfect-scrollbar.jquery.min.js"></script>
        <!-- Chartist JS -->
        <script src="../../js/dashboard/plugins/chartist.min.js"></script>
        <!-- Control Center for Material Dashboard: parallax effects, scripts for the example pages etc -->
        <script src="../../js/dashboard/material-dashboard.min.js?v=2.1.0" type="text/javascript"></script>
        <script>
                                $(document).ready(function () {
                                    chart.initDashboardPageCharts('day');

                                    $(window).resize(function () {
                                        md.initSidebarsCheck(), seq = seq2 = 0, setTimeout(function () {
                                            chart.initDashboardPageCharts('day');
                                            chart.initDashboardPageCharts('month');
                                            chart.initDashboardPageCharts('year');
                                        }, 500)
                                    })
                                });
        </script>

        <script>
            function setActive(button) {
                button = '#' + button;
                $('#byDay').removeClass('active');
                $('#byMonth').removeClass('active');
                $('#allYear').removeClass('active');
                $(button).addClass('active');
            }
        </script>

        <script>
            chart = {
                initDashboardPageCharts: function (type) {
                    var labelY;
                    var revenueData;
                    var newOrderData;
                    var processData;
                    var maxRevenueData;
                    var maxNewOrderData;
                    var maxProcessData;
                    var revenueChart = "#" + "revenueChart" + type;
                    var processChart = "#" + "processChart" + type;
                    var initNewOrderChart = "#" + "initNewOrderChart" + type;
                    switch (type) {
                        case 'day':
                            labelY = [<%=day%>];
                            revenueData = [<%=numRevenueLastNDayData%>];
                            newOrderData = [<%=numInitPackLastNDayData%>];
                            processData = [<%=numIncomePackLastNDayData%>];
                            maxRevenueData = [<%=max(numRevenueLastNDay) + 50%>];
                            maxNewOrderData = [<%=max(numInitPackLastNDay) + 5%>];
                            maxProcessData =<%=max(numIncomePackLastNDay) + 5%>;
                            break;
                        case 'month':
                            labelY = [<%=month%>];
                            revenueData = [<%=numRevenueLastNMonthData%>];
                            newOrderData = [<%=numInitPackLastNMonthData%>];
                            processData = [<%=numIncomePackLastNMonthData%>];
                            maxRevenueData = [<%=max(numRevenueLastNMonth) + 50%>];
                            maxNewOrderData = [<%=max(numInitPackLastNMonth) + 5%>];
                            maxProcessData =<%=max(numIncomePackLastNMonth) + 5%>;
                            break;
                        case 'year':
                            labelY = [<%=year%>];
                            revenueData = [<%=numRevenueLastNYearData%>];
                            newOrderData = [<%=numInitPackLastNYearData%>];
                            processData = [<%=numIncomePackLastNYearData%>];
                            maxRevenueData = [<%=max(numRevenueLastNYear) + 50%>];
                            maxNewOrderData = [<%=max(numInitPackLastNYear) + 5%>];
                            maxProcessData =<%=max(numIncomePackLastNYear) + 5%>;
                            break;
                        default:
                            return;
                    }
                    if (0 != $(revenueChart).length || 0 != $(processChart).length || 0 != $(
                            initNewOrderChart).length) {
                        dataDailySalesChart = {
                            labels: labelY,
                            series: [
                                revenueData
                            ]
                        }, optionsDailySalesChart = {
                            lineSmooth: Chartist.Interpolation.cardinal({
                                tension: 0
                            }),
                            low: 0,
                            high: maxRevenueData,
                            chartPadding: {
                                top: 0,
                                right: 0,
                                bottom: 0,
                                left: 0
                            },

                            axisY: {
                                labelInterpolationFnc: function (value) {
                                    if (value > 99999999) {
                                        return (value / 1000000000) + 'b';
                                    } else if (value > 999999) {
                                        return (value / 1000000) + 'm';
                                    } else if (Number(value) > 9999) {
                                        return (value / 1000) + 'k';
                                    } else {
                                        return value;
                                    }
                                }
                            }
                        };
                        var e = new Chartist.Line(revenueChart, dataDailySalesChart, optionsDailySalesChart);
                        md.startAnimationForLineChart(e), dataCompletedTasksChart = {
                            labels: labelY,
                            series: [
                                processData
                            ]
                        }, optionsCompletedTasksChart = {
                            lineSmooth: Chartist.Interpolation.cardinal({
                                tension: 0
                            }),
                            low: 0,
                            high: maxProcessData,
                            chartPadding: {
                                top: 0,
                                right: 0,
                                bottom: 0,
                                left: 0
                            },
                            axisY: {
                                labelInterpolationFnc: function (value) {
                                    if (value > 99999999) {
                                        return (value / 1000000000) + 'b';
                                    } else if (value > 999999) {
                                        return (value / 1000000) + 'm';
                                    } else if (Number(value) > 9999) {
                                        return (value / 1000) + 'k';
                                    } else {
                                        return value;
                                    }
                                }
                            }
                        };
                        var a = new Chartist.Line(processChart, dataCompletedTasksChart, optionsCompletedTasksChart);
                        md.startAnimationForLineChart(a);
                        var i = Chartist.Bar(initNewOrderChart, {
                            labels: labelY,
                            series: [
                                newOrderData
                            ]
                        }, {
                            axisX: {
                                showGrid: !1
                            },
                            low: 0,
                            high: maxNewOrderData,
                            chartPadding: {
                                top: 0,
                                right: 5,
                                bottom: 0,
                                left: 0
                            },
                            axisY: {
                                labelInterpolationFnc: function (value) {
                                    if (value > 99999999) {
                                        return (value / 1000000000) + 'b';
                                    } else if (value > 999999) {
                                        return (value / 1000000) + 'm';
                                    } else if (Number(value) > 9999) {
                                        return (value / 1000) + 'k';
                                    } else {
                                        return value;
                                    }
                                }
                            }
                        }, [
                            ["screen and (max-width: 640px)", {
                                    seriesBarDistance: 5,
                                    axisX: {
                                        labelInterpolationFnc: function (e) {
                                            return e[0]
                                        }
                                    }
                                }]
                        ]);
                        md.startAnimationForBarChart(i)
                    }
                }
            }
        </script>
    </body>
</html>