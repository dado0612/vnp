<%@page import="controller.EmployeeDAO"%>
<%@page import="java.util.List"%>
<%@page import="javafx.util.Pair"%>
<%@page import="util.SessionUtil"%>
<%@page import="config.RoleConfig"%>
<%@page import="model.Employee"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>

<!DOCTYPE html>
<html lang="en">
    <%
        Pair<Boolean, Employee> checkSession = SessionUtil.checkSession(session, RoleConfig.MANAGER);
        if (checkSession.getKey()) {
            response.sendRedirect("../index/login.html");
            return;
        }
        Employee emp = checkSession.getValue();
    %>
    <head>
        <meta charset="utf-8" />
        <link rel="apple-touch-icon" sizes="76x76" href="../../img/apple-icon.png">
        <link rel="icon" type="image/png" href="../../img/favicon.png">
        <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />
        <title>
            Fly Post | Check In/Out
        </title>
        <meta content='width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0, shrink-to-fit=no' name='viewport' />
        <!--     Fonts and icons     -->
        <link rel="stylesheet" type="text/css" href="https://fonts.googleapis.com/css?family=Roboto:300,400,500,700|Roboto+Slab:400,700|Material+Icons" />
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/latest/css/font-awesome.min.css">
        <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.4.1/css/all.css" integrity="sha384-5sAR7xN1Nv6T6+dT2mhtzEpVJvfS3NScPQTrOxhwjIuvcA67KV2R5Jz6kr4abQsz"
              crossorigin="anonymous">
        <!-- CSS Files -->
        <link href="../../css/dashboard/material-dashboard.css?v=2.1.0" rel="stylesheet" />
        <link rel="stylesheet" href="../../css/dashboard/custom.css">
    </head>

    <body class="">
        <div class="wrapper ">
            <div class="sidebar" data-color="azure" data-background-color="white" data-image="../../img/sidebar-1.jpg">
                <div class="logo">
                    <a href="./dashboard.jsp" class="simple-text logo-normal">
                        FLY POST
                    </a>
                </div>
                <div class="sidebar-wrapper">
                    <ul class="nav">
                        <li class="nav-item">
                            <a class="nav-link" href="./dashboard.jsp">
                                <i class="material-icons">dashboard</i>
                                <p>Dashboard</p>
                            </a>
                        </li>
                        <li class="nav-item ">
                            <a class="nav-link" href="./newOrder.jsp">
                                <i class="material-icons">library_add</i>
                                <p>New order</p>
                            </a>
                        </li>
                        <li class="nav-item active">
                            <a class="nav-link" href="./checkInOut.jsp">
                                <i class="material-icons">call_received</i>
                                <p>Check In/Out</p>
                            </a>
                        </li>
                        <li class="nav-item ">
                            <a class="nav-link" href="./storage.jsp">
                                <i class="material-icons">list</i>
                                <p>Storage</p>
                            </a>
                        </li>
                        <li class="nav-item ">
                            <a class="nav-link" href="./history.jsp">
                                <i class="material-icons">history</i>
                                <p>History</p>
                            </a>
                        </li>
                        <li class="nav-item ">
                            <a class="nav-link" href="./userProfile.jsp">
                                <i class="material-icons">person</i>
                                <p>User Profile</p>
                            </a>
                        </li>
                        <li class="nav-item ">
                            <a class="nav-link" href="./logout.jsp">
                                <i class="material-icons">power_settings_new</i>
                                <p>Logout</p>
                            </a>
                        </li>
                    </ul>
                </div>
            </div>
            <div class="main-panel">
                <!-- Navbar -->
                <nav class="navbar navbar-expand-lg navbar-transparent navbar-absolute fixed-top ">
                    <div class="container-fluid">
                        <div class="navbar-wrapper">
                            <a class="navbar-brand" href="./dashboard.jsp">Dashboard</a>
                        </div>
                        <button class="navbar-toggler" type="button" data-toggle="collapse" aria-controls="navigation-index"
                                aria-expanded="false" aria-label="Toggle navigation">
                            <span class="sr-only">Toggle navigation</span>
                            <span class="navbar-toggler-icon icon-bar"></span>
                            <span class="navbar-toggler-icon icon-bar"></span>
                            <span class="navbar-toggler-icon icon-bar"></span>
                        </button>
                        <div class="collapse navbar-collapse justify-content-end">
                            <ul class="navbar-nav">
                                <li class="nav-item">
                                    <a class="nav-link" href="./dashboard.jsp">
                                        <i class="material-icons">dashboard</i>
                                        <p class="d-lg-none d-md-block">
                                            Stats
                                        </p>
                                    </a>
                                </li>
                                <li class="nav-item dropdown">
                                    <a class="nav-link" href="" id="navbarDropdownMenuLink" data-toggle="dropdown" aria-haspopup="true"
                                       aria-expanded="false">
                                        <i class="material-icons">person</i>
                                        <p class="d-lg-none d-md-block">
                                            Account
                                        </p>
                                    </a>
                                    <div class="dropdown-menu dropdown-menu-right dropdown-with-icons" aria-labelledby="navbarDropdownMenuLink">
                                        <a class="dropdown-item" href="./userProfile.jsp">
                                            <span><i class="fas fa-sliders-h"></i></span>
                                            Setting
                                        </a>
                                        <a class="dropdown-item" href="./logout.jsp">
                                            <span><i class="fas fa-power-off"></i></span>
                                            Logout
                                        </a>
                                    </div>
                                </li>
                            </ul>
                        </div>
                    </div>
                </nav>
                <!-- End Navbar -->
                <div class="content">
                    <div class="container-fluid">
                        <!--content go here-->
                        <div class="row">
                            <div class="col-md-8">
                                <div class="card">
                                    <div class="card-header card-header-info">
                                        <h4 class="card-title">Check In/Out</h4>
                                        <p class="card-category">Check which packages are arrived and sending package to other posts or shipper</p>
                                    </div>
                                    <div class="card-body">
                                        <form action="../CheckInOutServlet" method="POST" onsubmit="return checkSubmit()">
                                            <!--change action letter-->
                                            <div class="row">
                                                <div class="col-md-3">
                                                    <div class="form-group">
                                                        <label>post ID</label>
                                                        <input type="text" name="postID" class="form-control" value="<%= emp.getPostID()%>" readonly>
                                                    </div>
                                                </div>
                                                <div class="col-md-5">
                                                    <div class="form-group">
                                                        <label>Package's ID</label>
                                                        <input type="text" id="packageID" name="packageID" class="form-control" readonly>
                                                    </div>
                                                </div>
                                                <div class="col-md-4">
                                                    <div class="form-group">
                                                        <label>Status</label>
                                                        <input type="text" id="status" name="status" class="form-control" readonly>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="row">
                                                <div class="col-md-6">
                                                    <div class="form-group">
                                                        <label>Sender name</label>
                                                        <input type="text" id="senderName" class="form-control" readonly>
                                                    </div>
                                                </div>
                                                <div class="col-md-6">
                                                    <div class="form-group">
                                                        <label>Sender phone</label>
                                                        <input type="text" id="senderPhone" class="form-control" readonly>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="row">
                                                <div class="col-md-6">
                                                    <div class="form-group">
                                                        <label>Receiver name</label>
                                                        <input type="text" id="receiverName" class="form-control" readonly>
                                                    </div>
                                                </div>
                                                <div class="col-md-6">
                                                    <div class="form-group">
                                                        <label>Receiver Phone</label>
                                                        <input type="text" id="receiverPhone" class="form-control" readonly>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="row">
                                                <div class="col-md-8">
                                                    <div class="form-group">
                                                        <label>Receiver Address</label>
                                                        <input type="text" id="receiverAddress" class="form-control" readonly>
                                                    </div>
                                                </div>
                                                <div class="col-md-4">
                                                    <div class="form-group">
                                                        <label>Receiver Mail</label>
                                                        <input type="mail" id="receiverMail" class="form-control" readonly>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="row">
                                                <div class="col-md-4">
                                                    <div class="form-group">
                                                        <label for="paid">Paid</label>
                                                        <input type="text" id="isPaid" class="form-control" readonly>
                                                    </div>
                                                </div>
                                                <div class="col-md-4">
                                                    <div class="form-group">
                                                        <label for="price">Package's Value</label>
                                                        <input type="text" id="price" class="form-control" readonly>
                                                    </div>
                                                </div>
                                                <div class="col-md-4">
                                                    <div class="form-group">
                                                        <label for="fee">Fee</label>
                                                        <input type="text" id="fee" class="form-control" readonly>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="row">
                                                <div class="col-md-12">
                                                    <div class="form-group">
                                                        <div class="form-group">
                                                            <label> Description</label>
                                                            <textarea class="form-control" rows="5" name="description"></textarea>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="row">
                                                <div class="col-md-3">
                                                    <div class="form-group">
                                                        <label>Next Post ID</label>
                                                        <input type="text" class="form-control" id="nextPostID" readonly>
                                                    </div>
                                                </div>
                                                <div class="col-md-9">
                                                    <div class="form-group">
                                                        <label>Next Post Name</label>
                                                        <input type="text" class="form-control" id="nextPostName" readonly>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="row">
                                                <div class="col-md-8">
                                                    <div class="form-group">
                                                        <label>Shipper</label>
                                                        <select name="shipper" id="shipper" class="form-control" disabled>
                                                            <option value="">Select One</option>
                                                            <%
                                                                List<Employee> listShipper = EmployeeDAO.selectShipperInPost(emp.getPostID());
                                                                for (Employee i : listShipper) {
                                                                    out.print("<option value='(" + i.getEmployeeID() + ") " + i.getName() + "'>(" + i.getEmployeeID() + ") " + i.getName() + "</option>");
                                                                }
                                                            %>
                                                        </select>
                                                    </div>
                                                </div>
                                                <div class="col-md-4">
                                                    <div class="form-check">
                                                        <label class="form-check-label">
                                                            <input class="form-check-input" type="radio" id="deliverySuccess" name="deliveryStatus"
                                                                   value="success" checked disabled>
                                                            Delivery successfully
                                                            <span class="circle">
                                                                <span class="check"></span>
                                                            </span>
                                                        </label>
                                                    </div>
                                                    <div class="form-check">
                                                        <label class="form-check-label">
                                                            <input class="form-check-input" type="radio" id="deliveryFail" name="deliveryStatus" value="fail"
                                                                   disabled>
                                                            Delivery failure
                                                            <span class="circle">
                                                                <span class="check"></span>
                                                            </span>
                                                        </label>
                                                    </div>
                                                </div>
                                            </div>
                                            <button id="btnSubmit" type="submit" class="btn btn-primary pull-right" disabled="">Confirm</button>
                                            <div class="clearfix"></div>
                                        </form>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-4">
                                <div class="card card-profile">
                                    <div class="card-avatar">
                                        <img class="img" src="../../img/qr.png" />
                                    </div>
                                    <div class="card-body">
                                        <video id="scanner" style="width: 100%"></video>
                                        <h6 class="card-category text-gray">QR Scanner</h6>
                                        <button class="btn btn-primary" onclick="turnOnScanner()">Turn on camera</button>
                                        <button class="btn btn-primary" onclick="turnOffScanner()">Turn off camera</button>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <footer class="footer">
                    <div class="container-fluid">
                        <nav class="float-left">
                            <ul>
                                <li>
                                    <a href="../">
                                        FLY POST
                                    </a>
                                </li>
                                <li>
                                    <a href="../index/about.html">
                                        About Us
                                    </a>
                                </li>
                                <li>
                                    <a href="../index/about.html">
                                        Blog
                                    </a>
                                </li>
                            </ul>
                        </nav>
                        <div class="copyright float-right">
                            &copy;
                            <script>
                                document.write(new Date().getFullYear())
                            </script>, made with <i class="material-icons">favorite</i> by
                            <a href="https://www.creative-tim.com" target="_blank">Creative Tim</a> for a better web.
                        </div>
                    </div>
                </footer>
            </div>
        </div>
        <!--   Core JS Files   -->
        <script src="../../js/dashboard/core/jquery.min.js" type="text/javascript"></script>
        <script src="../../js/dashboard/core/popper.min.js" type="text/javascript"></script>
        <script src="../../js/dashboard/core/bootstrap-material-design.min.js" type="text/javascript"></script>
        <script src="../../js/dashboard/plugins/perfect-scrollbar.jquery.min.js"></script>
        <!-- Control Center for Material Dashboard: parallax effects, scripts for the example pages etc -->
        <script src="../../js/dashboard/material-dashboard.min.js?v=2.1.0" type="text/javascript"></script>
        <!--qr scanner-->
        <script src="../../js/dashboard/plugins/instascan.min.js"></script>

        <script>
                                $(document).ready(function () {
                                    turnOnScanner();
                                });
        </script>

        <script>
            // INIT AJAX
            var request;
            if (window.XMLHttpRequest) {
                request = new XMLHttpRequest();
            } else if (window.ActiveXObject) {
                request = new ActiveXObject("Microsoft.XMLHTTP");
            }
            let scanner = new Instascan.Scanner({video: document.getElementById('scanner')});
            scanner.addListener('scan', function (content) {
                console.log(content);
                var r = scanner.scan();

                if (content != '') {
                    document.getElementById("packageID").value = content;
                    var url = "../CheckPackageServlet?packageID=" + content;
                    try {
                        request.onreadystatechange = getInfo; // getInfor is below function
                        request.open("GET", url, true);
                        request.send();
                    } catch (e) {
                        alert("Unable to connect to server");
                    }
                }
            });

            function getInfo() {
                if (request.readyState == 4) {
                    var val = request.responseText; // request.responseText = out.print in servlet
                    document.getElementById("senderName").value = '';
                    document.getElementById("senderPhone").value = '';
                    document.getElementById("receiverName").value = '';
                    document.getElementById("receiverPhone").value = '';
                    document.getElementById("receiverAddress").value = '';
                    document.getElementById("receiverMail").value = '';
                    document.getElementById("isPaid").value = '';
                    document.getElementById("price").value = '';
                    document.getElementById("fee").value = '';
                    document.getElementById("nextPostID").value = '';
                    document.getElementById("nextPostID").disabled = true;
                    document.getElementById("nextPostName").value = '';
                    document.getElementById("nextPostName").disabled = true;
                    document.getElementById("shipper").value = '';
                    document.getElementById("shipper").disabled = true;
                    document.getElementById("shipper").required = false;
                    document.getElementById("deliverySuccess").disabled = true;
                    document.getElementById("deliveryFail").disabled = true;
                    if (val == '') {
                        document.getElementById("status").value = 'Finished';
                        document.getElementById("btnSubmit").disabled = true;
                    } else if (val == 'error') {
                        document.getElementById("status").value = 'Error QR';
                        document.getElementById("btnSubmit").disabled = true;
                    } else {
                        document.getElementById("btnSubmit").disabled = false;
                        var jsonCheck = JSON.parse(val);
                        var status = jsonCheck.status;
                        document.getElementById("senderName").value = jsonCheck.senderName;
                        document.getElementById("senderPhone").value = jsonCheck.senderPhone;
                        document.getElementById("receiverName").value = jsonCheck.receiverName;
                        document.getElementById("receiverPhone").value = jsonCheck.receiverPhone;
                        document.getElementById("receiverAddress").value = jsonCheck.receiverAddress;
                        document.getElementById("receiverMail").value = jsonCheck.receiverMail;
                        document.getElementById("isPaid").value = jsonCheck.isPaid;
                        document.getElementById("price").value = jsonCheck.price;
                        document.getElementById("fee").value = jsonCheck.fee;
                        if (status == '2') {
                            document.getElementById("status").value = 'Check In';
                        } else if (status == '3') {
                            document.getElementById("status").value = 'Check Out';
                            document.getElementById("nextPostID").value = jsonCheck.nextPostID;
                            document.getElementById("nextPostID").disabled = false;
                            document.getElementById("nextPostName").value = jsonCheck.nextPostName;
                            document.getElementById("nextPostName").disabled = false;
                        } else if (status == '4') {
                            document.getElementById("status").value = 'Shipper';
                            document.getElementById("shipper").disabled = false;
                            document.getElementById("shipper").required = true;
                        } else if (status == '0') {
                            document.getElementById("status").value = 'Confirm';
                            document.getElementById("deliverySuccess").disabled = false;
                            document.getElementById("deliveryFail").disabled = false;
                        }
                    }
                }
            }

            function turnOffScanner() {
                scanner.stop();
            }

            function turnOnScanner() {
                Instascan.Camera.getCameras().then(function (cameras) {
                    if (cameras.length > 0) {
                        scanner.start(cameras[0]);
                    } else {
                        console.error('No cameras found.');
                    }
                }).catch(function (e) {
                    console.error(e);
                });
            }

            function checkSubmit() {
                if (document.getElementById('status').value == 'Shipper'
                        && document.getElementById('shipper').value == '') {
                    alert('You must choose a shipper!');
                    return false;
                }
            }
        </script>
    </body>
</html>
