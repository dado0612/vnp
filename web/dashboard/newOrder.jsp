<%@page import="config.RoleConfig"%>
<%@page import="util.SessionUtil"%>
<%@page import="model.Employee"%>
<%@page import="javafx.util.Pair"%>
<%@page import="java.util.List"%>
<%@page import="model.Location"%>
<%@page import="controller.LocationDAO"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>

<!DOCTYPE html>
<html lang="en">
    <%
        Pair<Boolean, Employee> checkSession = SessionUtil.checkSession(session, RoleConfig.MANAGER);
        if (checkSession.getKey()) {
            response.sendRedirect("../index/login.html");
            return;
        }
        Employee emp = checkSession.getValue();
    %>
    <head>
        <meta charset="utf-8" />
        <link rel="apple-touch-icon" sizes="76x76" href="../../img/apple-icon.png">
        <link rel="icon" type="image/png" href="../../img/favicon.png">
        <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />
        <title>
            Fly Post | New Order
        </title>
        <meta content='width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0, shrink-to-fit=no' name='viewport' />
        <!--     Fonts and icons     -->
        <link rel="stylesheet" type="text/css" href="https://fonts.googleapis.com/css?family=Roboto:300,400,500,700|Roboto+Slab:400,700|Material+Icons" />
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/latest/css/font-awesome.min.css">
        <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.4.1/css/all.css" integrity="sha384-5sAR7xN1Nv6T6+dT2mhtzEpVJvfS3NScPQTrOxhwjIuvcA67KV2R5Jz6kr4abQsz"
              crossorigin="anonymous">
        <!-- CSS Files -->
        <link href="../../css/dashboard/material-dashboard.css?v=2.1.0" rel="stylesheet" />
        <link rel="stylesheet" href="../../css/dashboard/custom.css">
    </head>

    <body class="">
        <div class="wrapper ">
            <div class="sidebar" data-color="azure" data-background-color="white" data-image="../img/sidebar-1.jpg">
                <div class="logo">
                    <a href="./dashboard.jsp" class="simple-text logo-normal">
                        FLY POST
                    </a>
                </div>
                <div class="sidebar-wrapper">
                    <ul class="nav">
                        <li class="nav-item">
                            <a class="nav-link" href="./dashboard.jsp">
                                <i class="material-icons">dashboard</i>
                                <p>Dashboard</p>
                            </a>
                        </li>
                        <li class="nav-item active">
                            <a class="nav-link" href="./newOrder.jsp">
                                <i class="material-icons">library_add</i>
                                <p>New order</p>
                            </a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link" href="./checkInOut.jsp">
                                <i class="material-icons">call_received</i>
                                <p>Check In/Out</p>
                            </a>
                        </li>
                        <li class="nav-item ">
                            <a class="nav-link" href="./storage.jsp">
                                <i class="material-icons">list</i>
                                <p>Storage</p>
                            </a>
                        </li>
                        <li class="nav-item ">
                            <a class="nav-link" href="./history.jsp">
                                <i class="material-icons">history</i>
                                <p>History</p>
                            </a>
                        </li>
                        <li class="nav-item ">
                            <a class="nav-link" href="./userProfile.jsp">
                                <i class="material-icons">person</i>
                                <p>User Profile</p>
                            </a>
                        </li>
                        <li class="nav-item ">
                            <a class="nav-link" href="./logout.jsp">
                                <i class="material-icons">power_settings_new</i>
                                <p>Logout</p>
                            </a>
                        </li>
                    </ul>
                </div>
            </div>
            <div class="main-panel">
                <!-- Navbar -->
                <nav class="navbar navbar-expand-lg navbar-transparent navbar-absolute fixed-top ">
                    <div class="container-fluid">
                        <div class="navbar-wrapper">
                            <a class="navbar-brand" href="./dashboard.jsp">Dashboard</a>
                        </div>
                        <button class="navbar-toggler" type="button" data-toggle="collapse" aria-controls="navigation-index"
                                aria-expanded="false" aria-label="Toggle navigation">
                            <span class="sr-only">Toggle navigation</span>
                            <span class="navbar-toggler-icon icon-bar"></span>
                            <span class="navbar-toggler-icon icon-bar"></span>
                            <span class="navbar-toggler-icon icon-bar"></span>
                        </button>
                        <div class="collapse navbar-collapse justify-content-end">
                            <ul class="navbar-nav">
                                <li class="nav-item">
                                    <a class="nav-link" href="./dashboard.jsp">
                                        <i class="material-icons">dashboard</i>
                                        <p class="d-lg-none d-md-block">
                                            Stats
                                        </p>
                                    </a>
                                </li>
                                <li class="nav-item dropdown">
                                    <a class="nav-link" href="" id="navbarDropdownMenuLink" data-toggle="dropdown" aria-haspopup="true"
                                       aria-expanded="false">
                                        <i class="material-icons">person</i>
                                        <p class="d-lg-none d-md-block">
                                            Account
                                        </p>
                                    </a>
                                    <div class="dropdown-menu dropdown-menu-right dropdown-with-icons" aria-labelledby="navbarDropdownMenuLink">
                                        <a class="dropdown-item" href="./userProfile.jsp">
                                            <span><i class="fas fa-sliders-h"></i></span>
                                            Setting
                                        </a>
                                        <a class="dropdown-item" href="./logout.jsp">
                                            <span><i class="fas fa-power-off"></i></span>
                                            Logout
                                        </a>
                                    </div>
                                </li>
                            </ul>
                        </div>
                    </div>
                </nav>
                <!-- End Navbar -->
                <div class="content">
                    <div class="container-fluid">
                        <!-- your content here -->
                        <div class="row justify-content-center">
                            <div class="col-md-10">
                                <div class="card">
                                    <div class="card-header card-header-info">
                                        <h4 class="card-title">New Order</h4>
                                        <p class="card-category">Create new order</p>
                                    </div>
                                    <div class="card-body">
                                        <form target="_blank" action="../NewOrderServlet" method="POST" onsubmit="return checkSubmit()"> <!--change action letter-->
                                            <div class="row">
                                                <div class="col-md-3">
                                                    <div class="form-group">
                                                        <label class="bmd-label-floating">Post ID</label>
                                                        <input type="text" id="postID" name="postID" class="form-control" value="<%= emp.getPostID()%>" readonly="" >
                                                    </div>
                                                </div>
                                                <div class="col-md-5">
                                                    <div class="form-group">
                                                        <label class="bmd-label-floating">Sender name</label>
                                                        <input type="text" name="senderName" class="form-control" required>
                                                    </div>
                                                </div>
                                                <div class="col-md-4">
                                                    <div class="form-group">
                                                        <label class="bmd-label-floating">Sender phone</label>
                                                        <input type="text" name="senderPhone" class="form-control" required>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="row">
                                                <div class="col-md-6">
                                                    <div class="form-group">
                                                        <label class="bmd-label-floating">Receiver name</label>
                                                        <input type="text" name="receiverName" class="form-control" required>
                                                    </div>
                                                </div>
                                                <div class="col-md-6">
                                                    <div class="form-group">
                                                        <label class="bmd-label-floating">Receiver Phone</label>
                                                        <input type="text" name="receiverPhone" class="form-control" required>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="row">
                                                <div class="col-md-8">
                                                    <div class="form-group">
                                                        <label class="bmd-label-floating">Receiver Address</label>
                                                        <input type="text" name="receiverAddress" class="form-control" required>
                                                    </div>
                                                </div>
                                                <div class="col-md-4">
                                                    <div class="form-group">
                                                        <label class="bmd-label-floating">Receiver Mail</label>
                                                        <input type="mail" name="receiverMail" class="form-control" required>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="row">
                                                <div class="col-md-4">
                                                    <div class="form-group">
                                                        <label for="province">Province</label>
                                                        <select name="receiverProvince" id="provinceID" class="form-control" onchange="connectLocationServlet('province')" onblur="connectFeeServlet(whatIsChecking())" required="">
                                                            <option value="" id="">Select One</option>
                                                            <%
                                                                List<Location> listProvince = LocationDAO.selectAllProvince();
                                                                for (Location l : listProvince) {
                                                                    out.print("<option value='" + l.getLocationID() + "' id=''>" + l.getLocationName() + "</option>");
                                                                }
                                                            %>
                                                        </select>
                                                    </div>
                                                </div>
                                                <div class="col-md-4">
                                                    <div class="form-group">
                                                        <label for="district">District</label>
                                                        <select name="receiverDistrict" id="districtID" class="form-control" onchange="connectLocationServlet('district')" required="">
                                                            <option value="" id="">Select One</option>
                                                        </select>
                                                    </div>
                                                </div>
                                                <div class="col-md-4">
                                                    <div class="form-group">
                                                        <label for="commune">Commune</label>
                                                        <select name="receiverLocation" id="locationID" class="form-control" required="">
                                                            <option value="" id="">Select One</option>
                                                        </select>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="row">
                                                <div class="col-md-4 col-sm-3">
                                                    <div class="form-check">
                                                        <label class="form-check-label">
                                                            <input class="form-check-input" type="radio" name="calculateByWhat" id="calculateByWhat" value="1" checked onclick="calculateBy(this.value)">
                                                            By weight (gram)
                                                            <span class="circle">
                                                                <span class="check"></span>
                                                            </span>
                                                        </label>
                                                    </div>
                                                    <div class="form-check">
                                                        <label class="form-check-label">
                                                            <input class="form-check-input" type="radio" name="calculateByWhat" id="calculateByWhat" value="0" onclick="calculateBy(this.value)">
                                                            By dimension (cm)
                                                            <span class="circle">
                                                                <span class="check"></span>
                                                            </span>
                                                        </label>
                                                    </div>
                                                </div>
                                                <div class="col-md-2">
                                                    <div class="form-group">
                                                        <label for="bmd-label-floating">Weight</label>
                                                        <input type="number" name="weight" id="weight" class="form-control" min="1" required onblur="connectFeeServlet(whatIsChecking())">
                                                    </div>
                                                </div>
                                                <div class="col-md-2 col-sm-3">
                                                    <div class="form-group">
                                                        <label for="bmd-label-floating">Height</label>
                                                        <input type="number" name="height" id="height" class="form-control" min="1" required disabled onblur="connectFeeServlet(whatIsChecking())">
                                                    </div>
                                                </div>
                                                <div class="col-md-2 col-sm-3">
                                                    <div class="form-group">
                                                        <label for="bmd-label-floating">Width</label>
                                                        <input type="number" name="width" id="width" class="form-control" min="1" required disabled onblur="connectFeeServlet(whatIsChecking())">
                                                    </div>
                                                </div>
                                                <div class="col-md-2 col-sm-3">
                                                    <div class="form-group">
                                                        <label for="bmd-label-floating">Depth</label>
                                                        <input type="number" name="depth" id="depth" class="form-control" min="1" required disabled onblur="connectFeeServlet(whatIsChecking())">
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="row">
                                                <div class="col-md-4">
                                                    <div class="form-group">
                                                        <label for="bmd-label-floating">Package's Value (VND)</label>
                                                        <input type="text" name="price" id="price" class="form-control" required>
                                                    </div>
                                                </div>
                                                <div class="col-md-4">
                                                    <div class="form-group">
                                                        <label for="bmd-label-floating">Fee (VND)</label>
                                                        <input type="text" name="fee" id="fee" class="form-control" value="0" readonly="">
                                                    </div>
                                                </div>
                                                <div class="col-md-4">
                                                    <div class="form-check">
                                                        <label class="form-check-label">
                                                            <input class="form-check-input" type="radio" name="isPaid" value="true" checked>
                                                            Paid by sender
                                                            <span class="circle">
                                                                <span class="check"></span>
                                                            </span>
                                                        </label>
                                                    </div>
                                                    <div class="form-check">
                                                        <label class="form-check-label">
                                                            <input class="form-check-input" type="radio" name="isPaid" value="false">
                                                            Paid by receiver
                                                            <span class="circle">
                                                                <span class="check"></span>
                                                            </span>
                                                        </label>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="row">
                                                <div class="col-md-12">
                                                    <div class="form-group">
                                                        <label>Description</label>
                                                        <div class="form-group">
                                                            <label class="bmd-label-floating"> Leave some message to the receiver</label>
                                                            <textarea class="form-control" rows="5" name="description"></textarea>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <button type="submit" class="btn btn-primary pull-right">Order</button>
                                            <div class="clearfix"></div>
                                        </form>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <footer class="footer">
                    <div class="container-fluid">
                        <nav class="float-left">
                            <ul>
                                <li>
                                    <a href="../">
                                        FLY POST
                                    </a>
                                </li>
                                <li>
                                    <a href="../index/about.html">
                                        About Us
                                    </a>
                                </li>
                                <li>
                                    <a href="../index/about.html">
                                        Blog
                                    </a>
                                </li>
                            </ul>
                        </nav>
                        <div class="copyright float-right">
                            &copy;
                            <script>
                                document.write(new Date().getFullYear())
                            </script>, made with <i class="material-icons">favorite</i> by
                            <a href="https://www.creative-tim.com" target="_blank">Creative Tim</a> for a better web.
                        </div>
                        <!-- your footer here -->
                    </div>
                </footer>
            </div>
        </div>
        <!--   Core JS Files   -->
        <script src="../../js/dashboard/core/jquery.min.js" type="text/javascript"></script>
        <script src="../../js/dashboard/core/popper.min.js" type="text/javascript"></script>
        <script src="../../js/dashboard/core/bootstrap-material-design.min.js" type="text/javascript"></script>
        <script src="../../js/dashboard/plugins/perfect-scrollbar.jquery.min.js"></script>
        <!-- Control Center for Material Dashboard: parallax effects, scripts for the example pages etc -->
        <script src="../../js/dashboard/material-dashboard.min.js?v=2.1.0" type="text/javascript"></script>
        <script>
                                function calculateBy(value) {
                                    document.getElementById("fee").value = 0;
                                    if (value == '0') {
                                        document.getElementById("height").removeAttribute("disabled");
                                        document.getElementById("width").removeAttribute("disabled");
                                        document.getElementById("depth").removeAttribute("disabled");
                                        document.getElementById("weight").disabled = true;
                                        document.getElementById("weight").value = '';
                                    } else {
                                        document.getElementById("height").disabled = true;
                                        document.getElementById("width").disabled = true;
                                        document.getElementById("depth").disabled = true;
                                        document.getElementById("height").value = '';
                                        document.getElementById("width").value = '';
                                        document.getElementById("depth").value = '';
                                        document.getElementById("weight").removeAttribute("disabled");
                                    }
                                }

                                function checkValid(id) {
                                    if (Number(document.getElementById(id).value) < 1) {
                                        document.getElementById(id).value = 1;
                                    }
                                }
        </script>
        <script>
            // INIT AJAX
            var request;
            if (window.XMLHttpRequest) {
                request = new XMLHttpRequest();
            } else if (window.ActiveXObject) {
                request = new ActiveXObject("Microsoft.XMLHTTP");
            }

            function connectLocationServlet(x) {
                var provinceID = document.getElementById("provinceID").value;
                var districtID = document.getElementById("districtID").value;
                var url;
                document.getElementById('locationID').innerText = null;
                var selectLocation = document.getElementById("locationID");
                var option = document.createElement("option");
                option.value = "";
                option.text = "Select One";
                selectLocation.add(option);
                if (x == 'province') {
                    document.getElementById('districtID').innerText = null;
                    var selectDistrict = document.getElementById("districtID");
                    var option = document.createElement("option");
                    option.value = "";
                    option.text = "Select One";
                    selectDistrict.add(option);
                    if (provinceID == "") {
                        return;
                    } else {
                        url = "../LocationServlet?provinceID=" + provinceID;
                    }
                } else {
                    if (districtID == "") {
                        return;
                    } else {
                        url = "../LocationServlet?provinceID=" + provinceID + "&districtID=" + districtID;
                    }
                }
                try {
                    request.onreadystatechange = getInfoLocation; // getInfor is below function
                    request.open("GET", url, true);
                    request.send();
                } catch (e) {
                    alert("Unable to connect to server");
                }
            }

            function getInfoLocation() {
                if (request.readyState == 4) {
                    var val = request.responseText; // request.responseText = out.print in servlet
                    var locations = JSON.parse(val);
                    var selectLocation;
                    if (document.getElementById("districtID").value == "") {
                        selectLocation = document.getElementById("districtID");
                    } else {
                        selectLocation = document.getElementById("locationID");
                    }
                    //var option = document.createElement("option");
                    for (var i = 0; i < locations.location.length; i++) {
                        var locationID = locations.location[i].locationID;
                        var locationName = locations.location[i].locationName;
                        var option = document.createElement("option");
                        option.text = locationName;
                        option.value = locationID;
                        selectLocation.add(option);
                    }
                }
            }

            function checkValidNumber(element) {
                var number = element.value;
                if (!Number.isInteger(Number(number)) || Number(number) < 1) {
                    element.value = '';
                    document.getElementById('fee').value = 0;
                    return false;
                }
                return true
            }

            function checkSubmit() {
                setTimeout(function () {
                    location.reload();
                }, 1000);
                return true;
            }

            function whatIsChecking() {
                var calculateByWhat = document.getElementsByName("calculateByWhat");
                for (var i = 0; i < calculateByWhat.length; i++) {
                    if (calculateByWhat[i].checked == true) {
                        return calculateByWhat[i].value;
                    }
                }
            }

            function getUrlForWeight() {
                var weight = document.getElementById("weight").value;
                var receiverLocationID = document.getElementById("provinceID").value;
                var postID = document.getElementById("postID").value;
                return "../FeeServlet?weight=" + weight + "&senderLocationID=" + postID + "&receiverLocationID=" + receiverLocationID;
            }

            function getUrlForDimention() {
                var width = document.getElementById("width").value;
                var height = document.getElementById("height").value;
                var depth = document.getElementById("depth").value;
                var receiverLocationID = document.getElementById("provinceID").value;
                var postID = document.getElementById("postID").value;
                return "../FeeServlet?width=" + width + "&height=" + height + "&depth=" + depth + "&senderLocationID=" + postID + "&receiverLocationID=" + receiverLocationID;
            }

            function connectFeeServlet(whatIsChecking) {
                var calculateByWhat = document.getElementsByName("calculateByWhat");
                var value;
                for (var i = 0; i < calculateByWhat.length; i++) {
                    if (calculateByWhat[i].checked == true) {
                        value = calculateByWhat[i].value;
                    }
                }
                if (value == '1') {
                    if (!checkValidNumber(document.getElementById("weight"))) {
                        return;
                    }
                } else {
                    if (!checkValidNumber(document.getElementById("width")) ||
                            !checkValidNumber(document.getElementById("height")) ||
                            !checkValidNumber(document.getElementById("depth"))) {
                        return;
                    }
                }

                if (document.getElementById("provinceID").value == "") {
                    document.getElementById('fee').value = 0;
                    return;
                }

                var url = "default";
                if (whatIsChecking == '1') {
                    url = getUrlForWeight();
                } else {
                    url = getUrlForDimention();
                }

                document.getElementById('fee').value = url;
                try {
                    request.onreadystatechange = getInfoFee; // getInfor is below function
                    request.open("GET", url, true);
                    request.send();
                } catch (e) {
                    alert("Unable to connect to server");
                }
            }

            function getInfoFee() {
                if (request.readyState == 4) {
                    var val = request.responseText; // request.responseText = out.print in servlet
                    document.getElementById("fee").value = val;
                }
            }
        </script>
    </body>
</html>