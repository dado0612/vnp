<%@page import="controller.LocationDAO"%>
<%@page import="controller.BillCacheDAO"%>
<%@page import="model.BillCache"%>
<%@page import="controller.PackageDAO"%>
<%@page import="javafx.util.Pair"%>
<%@page import="util.SessionUtil"%>
<%@page import="config.RoleConfig"%>
<%@page import="model.Employee"%>
<%@page import="model.Package"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>

<!DOCTYPE html>
<html lang="en">
    <%
        Pair<Boolean, Employee> checkSession = SessionUtil.checkSession(session, RoleConfig.MANAGER);
        if (checkSession.getKey()) {
            response.sendRedirect("../index/login.html");
            return;
        }
        long packageID;
        String weight, height, width, depth, description;
        weight = request.getParameter("weight");
        height = request.getParameter("height");
        width = request.getParameter("width");
        depth = request.getParameter("depth");
        description = request.getParameter("description");
        try {
            packageID = Long.parseLong(request.getParameter("packageID"));
        } catch (NumberFormatException e) {
            response.sendRedirect("../index/error.html");
            return;
        }
        Package pac = PackageDAO.selectByPackageID(packageID);
        BillCache bil = BillCacheDAO.selectByPackageID(packageID);
        String receiverLocation = LocationDAO.getFullNameCommune(pac.getReceiverLocation());
    %>
    <head>
        <meta charset="utf-8" />
        <link rel="apple-touch-icon" sizes="76x76" href="../../img/apple-icon.png">
        <link rel="icon" type="image/png" href="../../img/favicon.png">
        <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />
        <title>
            Fly Post | Bill
        </title>
        <meta content='width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0, shrink-to-fit=no' name='viewport' />
        <!--     Fonts and icons     -->
        <link rel="stylesheet" type="text/css" href="https://fonts.googleapis.com/css?family=Roboto:300,400,500,700|Roboto+Slab:400,700|Material+Icons" />
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/latest/css/font-awesome.min.css">
        <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.4.1/css/all.css" integrity="sha384-5sAR7xN1Nv6T6+dT2mhtzEpVJvfS3NScPQTrOxhwjIuvcA67KV2R5Jz6kr4abQsz"
              crossorigin="anonymous">
        <!-- CSS Files -->
        <link href="../../css/dashboard/material-dashboard.css?v=2.1.0" rel="stylesheet" />
        <link rel="stylesheet" href="../../css/dashboard/custom.css">
    </head>

    <body class="">
        <div class="wrapper ">
            <div class="container">
                <div class="content" style="margin-top: 50px">
                    <div class="container-fluid">
                        <!--content go here-->
                        <div class="row">
                            <div class="col">
                                <h2>Fly Post</h2>
                                <p>Fast - Save - Simple</p>
                            </div>
                            <div class="col" style="text-align: right">
                                <h3>Bill Code: <span id="billCacheID">FLY<%= bil.getBillCacheID()%></span></h3>
                            </div>
                        </div>
                        <hr>
                        <div class="row">
                            <div class="col-md-6">
                                <!--sender info-->
                                <div class="text-center">
                                    <h4>Sender information</h4>
                                </div>
                                <div class="row">
                                    <div class="col-md-6">
                                        <h5>Name: <span id="senderName"><%= pac.getSenderName()%></span></h5>
                                    </div>
                                    <div class="col-md-6">
                                        <h5>Phone: <span id="senderPhone"><%= pac.getSenderPhone()%></span></h5>
                                    </div>
                                </div>
                                <div rows="5">
                                    <label for="description">Description</label>
                                    <textarea class="form-control" id="description" rows="5" readonly>
                                        <%= description%>
                                    </textarea>
                                </div>
                                <div class="row" style="margin-top: 20px">
                                    <div class="text-center col">
                                        <h4>Package information</h4>
                                    </div>
                                </div>
                                <div class="row">
                                    <h5>Package's properties</h5>
                                </div>
                                <div class="row">
                                    <div class="col-md-3">
                                        <h5>Weight (g): <span id="weight"><%= weight%></span></h5>
                                    </div>
                                    <div class="col-md-3">
                                        <h5>Height (cm): <span id="height"><%= height%></span></h5>
                                    </div>
                                    <div class="col-md-3">
                                        <h5>Width (cm): <span id="width"><%= width%></span></h5>
                                    </div>
                                    <div class="col-md-3">
                                        <h5>Depth (cm): <span id="depth"><%= depth%></span></h5>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-md-4">
                                        <h5>Paid: <span id="status"><%= bil.isStatus() ? "Yes" : "No"%></span></h5>
                                        <h5>Post ID: <span id="postID"><%= pac.getPostID()%></span></h5>
                                    </div>
                                    <div class="col-md-8">
                                        <div class="row">
                                            <div class="col">
                                                <h5>Package's value (VND):</h5>
                                            </div>
                                            <div class="col text-right mr-sm-3">
                                                <h5 id="price"><%= pac.getPrice()%></h5>
                                            </div>
                                            <hr>
                                        </div>
                                        <div class="row">
                                            <div class="col">
                                                <h5>Fee (VND):</h5>
                                            </div>
                                            <div class="col text-right mr-sm-3">
                                                <h5 id="fee"><%= bil.getFee()%></h5>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div></div>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <!--receiver info-->
                                <div class="text-center">
                                    <h4>Receiver information</h4>
                                </div>
                                <div class="row">
                                    <div class="col-md-6">
                                        <h5>Name: <span id="receiverName"><%= pac.getReceiverName()%></span></h5>
                                    </div>
                                    <div class="col-md-6">
                                        <h5>Phone: <span id="receiverPhone"><%= pac.getReceiverPhone()%></span></h5>
                                    </div>
                                </div>
                                <div>
                                    <h5>Email: <span id="receiverMail"><%= pac.getReceiverMail()%></span></h5>
                                </div>
                                <div>
                                    <h5>Address: <span id="locationName"><%= pac.getReceiverAddress() + ", " + receiverLocation%></span></h5>
                                </div>
                                <div style="margin-top: 75px">
                                    <hr/>
                                </div>
                                <div class="row" style="margin-top: 50px; margin-left: 20px">
                                    <div style="width: 180px;">
                                        <div style="max-width: 150px; max-height: 150px" id="qrcode"></div>
                                    </div>
                                    <div class="col">
                                        <h6>Check this code at <span id="domain"></span> to track & trace the delivery progress.</h6>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!--   Core JS Files   -->
        <script src="../../js/dashboard/core/jquery.min.js" type="text/javascript"></script>
        <script src="../../js/dashboard/core/popper.min.js" type="text/javascript"></script>
        <script src="../../js/dashboard/core/bootstrap-material-design.min.js" type="text/javascript"></script>
        <script src="../../js/dashboard/plugins/perfect-scrollbar.jquery.min.js"></script>
        <!-- Control Center for Material Dashboard: parallax effects, scripts for the example pages etc -->
        <script src="../../js/dashboard/material-dashboard.min.js?v=2.1.0" type="text/javascript"></script>
        <!--qrcode.js-->
        <script src="../../js/dashboard/plugins/qrcode.js"></script>

        <script>
            var QR = new QRCode(document.getElementById("qrcode"));

            function makeCode() {
                var elText = '<%= packageID%>';//change value package's ID
                QR._htOption.width = 150;
                QR._htOption.height = 150;
                QR.makeCode(elText);

            }

            $(document).ready(function () {
                document.getElementById("domain").innerHTML = document.domain;
                makeCode();
            });
        </script>
    </body>
</html>
